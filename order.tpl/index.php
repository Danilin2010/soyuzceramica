<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
$APPLICATION->SetAdditionalCSS('/order.tpl/style.css', true);
$APPLICATION->AddHeadScript('/order.tpl/script.js', true);
?>

<div class="cart-page" >
  <table class="styled-table">
    <tr class="row table-head">
      <td class="product"></td>
      <td>скидка</td>
      <td>цена</td>
      <td>количество</td>
      <td>сумма</td>
      <td></td>
    </tr>
    <tr class="row">
      <td class="product"><a href="product.html"><img src="http://soyuzceramica.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/a89/ea34fe804273cc5f134a0e055a554511.jpg?151366937537669" alt=""></a><a href="product.html"><span>AR144/6230 Фантазия 25*5,4 керамический бордюр AR144\6230</span></a></td>
      <td class="discount-td"><span class='hidden'>скидка </span>5%</td>
      <td class="price"><span class='hidden'>цена </span> 5 870<span> руб.</span>
        <div class="old_price" id="old_price_4350">
          <span class="price_value">569,94</span>
          <span class="price_currency"> руб.</span>
        </div>
      </td>
      <td class="quantity-td">
        <div class="quantity">
          <div class="q-btn decrement">-</div>
          <input type="number" value="5" min="1" class="q-result">
          <div class="q-btn increment">+</div>
        </div>
      </td>
      <td class="summ"><span class='hidden'>сумма </span> 29 350<span> руб.</span></td>
      <td class="btns">
        <div class="add-to-fav"></div>
        <div class="remove"></div>
      </td>
    </tr>
    <tr class="row">
      <td class="product"><a href="product.html"><img src="http://soyuzceramica.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/a89/ea34fe804273cc5f134a0e055a554511.jpg?151366937537669" alt=""></a><a href="product.html"><span>AR144/6230 Фантазия 25*5,4 керамический бордюр AR144\6230</span></a></td>
      <td class="discount-td"><span class='hidden'>скидка </span>5%</td>
      <td class="price"><span class='hidden'>цена </span> 5 870<span> руб.</span>
        <div class="old_price" id="old_price_4350">
          <span class="price_value">569,94</span>
          <span class="price_currency"> руб.</span>
        </div>
      </td>
      <td class="quantity-td with-counter-units">
        <div class="counter-units-wrapp" data-id="4351" data-price="1273.456" data-count-in-pack="4" data-meter-in-pack="1.4400" date-ratio="1" data-use-float-quantity-js="true">
                            <div class="counter_block unit-meter" data-offers="N" data-item="4351">
                                                    <input type="text" class="text js-calc-pack" id="QUANTITY_INPUT_4351" name="QUANTITY_INPUT_4351" value="1.44" autocomplete="off">
                              <label>м2</label>
                            </div>
                            <br>
                            <div class="counter_block unit-pack" data-offers="N" data-item="4351">
                                                    <input type="text" class="text js-calc-pack" id="QUANTITY_INPUT_4351_pack" name="QUANTITY_INPUT_4351_pack" value="1" autocomplete="off">
                              <label>уп</label>
                            </div>
                            <br>
                            <div class="counter_block unit-thing" data-offers="N" data-item="4351">
                              <input type="text" class="text js-calc-pack" id="QUANTITY_INPUT_4351_thing" name="QUANTITY_INPUT_4351_thing" value="4" autocomplete="off" onkeyup="updateQuantity('QUANTITY_INPUT_4351', '4351', 1, true)">
                              <label>шт</label>
                            </div>
                          </div>
      </td>
      <td class="summ"><span class='hidden'>сумма </span> 29 350<span> руб.</span></td>
      <td class="btns">
        <div class="add-to-fav"></div>
        <div class="remove"></div>
      </td>
    </tr>
    <tr class="row">
      <td class="product"><a href="product.html"><img src="http://soyuzceramica.ru.opt-images.1c-bitrix-cdn.ru/upload/iblock/a89/ea34fe804273cc5f134a0e055a554511.jpg?151366937537669" alt=""></a><a href="product.html"><span>AR144/6230 Фантазия 25*5,4 керамический бордюр AR144\6230</span></a></td>
      <td class="discount-td"><span class='hidden'>скидка </span>5%</td>
      <td class="price"><span class='hidden'>цена </span> 5 870<span> руб.</span>
      </td>
      <td class="quantity-td">
        <div class="quantity">
          <div class="q-btn decrement">-</div>
          <input type="number" value="5" min="1" class="q-result">
          <div class="q-btn increment">+</div>
        </div>
      </td>
      <td class="summ"><span class='hidden'>сумма </span> 29 350<span> руб.</span></td>
      <td class="btns">
        <div class="add-to-fav"></div>
        <div class="remove"></div>
      </td>
    </tr>
  </table>
  <div class="total-row"><span>Итого:</span><span class="total-summ">99 790 руб.</span></div>
  <div id="order-form-wrap" class="order-form-wrap">
    <form class="order-form">
      <div class="fields-block">
        <h2 class="page-sub-title">Информация о покупателе</h2>
        <ul class="checkbox-list">
          <li>
            <input id="user-fiz" type="radio" name="user-type" checked>
            <label for="user-fiz">Физ. лицо</label>
          </li>
          <li>
            <input id="user-diller" type="radio" name="user-type">
            <label for="user-diller">Диллер</label>
          </li>
        </ul>
        <div class="input-row">
          <label for="client-name">Ф.И.О. *</label>
          <input type="text" id="client-name" class="field-style">
        </div>
        <div class="input-row">
          <label for="client-phone">Телефон *</label>
          <input type="tel" id="client-phone" class="js-mask-phone field-style">
        </div>
        <div class="input-row">
          <label for="client-mail">E-mail *</label>
          <input type="email" id="client-mail" class="field-style">
        </div>
        <div class="input-row wish-date">
          <label for="client-date">Желаемая дата отгрузки</label>
          <input type="text" id="client-date" class="field-style">
          <div class="calendar-picker"><img src="img/calendar.svg" alt="Желаемая дата отгрузки"></div>
        </div>
        <div class="input-row">
          <label for="client-address">Адрес доставки</label>
          <textarea id="client-address" class="field-style"></textarea>
        </div>
        <div class="input-row">
          <label for="client-comment">Комментарий к заказу</label>
          <textarea id="client-comment" class="field-style"></textarea>
        </div>
      </div>
      <div class="checkboxes-block">
        <h2 class="page-sub-title">Оплата заказа</h2>
        <div class="columns-block">
          <div class="column">
            <ul class="checkbox-list">
              <li>
                <input id="pay-credit" type="radio" name="pay-method" checked>
                <label for="pay-credit">Безналичная оплата</label>
              </li>
            </ul>
            <div class="payments explain-text"><img src="img/visa.svg" alt="Visa" class="visa"><img src="img/mastercard.svg" alt="MasterCard" class="mastercard"><img src="img/sberbankonline.svg" alt="SberbankOnline" class="sberbankonline"><img src="img/yandexmoney.svg" alt="YandexMoney" class="yandexmoney"><span class="etc">...</span></div>
          </div>
          <!-- Дополнит. способ оплаты -->
          <!-- <div class="column">
            <ul class="checkbox-list">
              <li>
                <input id="pay-cash" type="radio" name="pay-method">
                <label for="pay-cash">Наличными</label>
              </li>
            </ul>
            <div class="explain-text">Оплата наличными курьеру при получении заказа.</div>
          </div> -->
          <div class="column">
            <ul class="checkbox-list">
              <li>
                <input id="pay-cash" type="radio" name="pay-method">
                <label for="pay-cash">Наличными</label>
              </li>
            </ul>
            <div class="explain-text">Оплата наличными курьеру при получении заказа.</div>
          </div>
        </div>
        <h2 class="page-sub-title">Доставка</h2>
        <div class="columns-block">
          <div class="column">
            <ul class="checkbox-list">
              <li>
                <input id="curier-delivery" type="radio" name="delivery-method" checked>
                <label for="curier-delivery">Курьером</label>
              </li>
            </ul>
            <div class="explain-text">В&nbsp;удообное для вас время, после согласования заказа с&nbsp;нашим оператором.</div>
          </div>
          <div class="column">
            <ul class="checkbox-list">
              <li>
                <input id="self-delivery" type="radio" name="delivery-method">
                <label for="self-delivery">Самовывоз</label>
              </li>
            </ul>
            <div class="explain-text"><span>Москва, м. Новокосино, Носовихинское шоссе, владение 4. Торгово-выставочный центр &laquo;Никольский парк&raquo;, 4&nbsp;линия, 9&nbsp;павильон. </span><a href="contacts.html#map-1" target="_blank" class="blue underline">Смотреть на карте</a></div>
          </div>
        </div>
      </div>
      <hr>
      <ul class="checkbox-list user-agree">
        <li>
          <input id="user-agree" type="checkbox" name="user-agree" checked>
          <label for="user-agree"><span>Согласен с </span><a href="http://soyuzceramica.ru/rule/" target="_blank" class="underline">политикой конфиденциальности</a><span> и обработкой персональных данных</span></label>
        </li>
      </ul>
    </form>
  </div>
  <div class="cart-final-btn"><a class="btn btn-md button js-show-order-form">оформить заказ</a></div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
