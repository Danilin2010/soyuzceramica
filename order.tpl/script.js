$(function(){
  $('body').on('click', '.js-show-order-form', function(){
    $(this).removeClass('js-show-order-form').addClass('js-send-order').text('заказать');
    $('#order-form-wrap').slideDown(200, function() {
      $('#client-name').focus();
    });
  });
});
