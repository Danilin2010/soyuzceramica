﻿<?
set_time_limit(0);
ini_set("memory_limit", "1000M");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


function get_section($ID){
	CModule::IncludeModule('iblock');
	
	$arFilter = Array('IBLOCK_ID'=>16, 'ID'=>$ID);
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
	$names=array();
	while($ar_result = $db_list->GetNext()){
		$names[]=str_replace("&quot;", "",htmlspecialchars_decode($ar_result['NAME']));
  	}
	return implode(", ",$names);
}
function get_colection($ID){
	CModule::IncludeModule('iblock');
	
	$arFilter = Array('IBLOCK_ID'=>16, 'ID'=>$ID);
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
	$names=array();
	while($ar_result = $db_list->GetNext()){
		$names[]=get_section($ar_result['IBLOCK_SECTION_ID']);
  	}
	return implode(", ",$names);
}

function get_tupes($tupes_name,$id){
	
		
		
		if(count($tupes_name)>1){
			$rr=array();
			for($i=0;$i<count($tupes_name);$i++){
				$sort=Array('ID'=>"ASC");
				$add_filter=array('ID'=>$tupes_name[$i]);
				$arr=GetMaterials($id,1,$sort,$add_filter);
				if(count($arr['a'])>0 && is_array($arr['a'][0])){
					$rr[]=$arr['a'][0]['NAME'];
				}
				
			}
			return implode(",",$rr);
		}else{
			$sort=Array('ID'=>"ASC");
			$add_filter=array('ID'=>$tupes_name);
			$arr=GetMaterials($id,1,$sort,$add_filter);
			if(count($arr['a'])>0 && is_array($arr['a'][0])){
				return $arr['a'][0]['NAME'];
			}
		}
		
		
		}
		
$sort=Array('sort'=>"ASC");
$add_filter=array('ACTIVE'=>'Y','SECTION_ACTIVE'=>'Y');
$arr=GetMaterials(16,5000,$sort,$add_filter);	
$text='';
foreach($arr['a'] as $key =>$val){
	$arPrice = CPrice::GetBasePrice($val['ID']);
	$id = $val['ID'];
	$section=get_section($val['IBLOCK_SECTION_ID']);
	$colection=get_colection($val['IBLOCK_SECTION_ID']);
	$cod_ruus=$val['pr']['num']['VALUE'];
	$cod_kerama=$val['pr']['id_fake']['VALUE'];
	$name= htmlspecialchars_decode(str_replace("&quot;", "",$val['NAME']));
	$cvet=get_tupes($val['pr']['color']['VALUE'],14);
	$naznachenie=get_tupes($val['pr']['naznachenie']['VALUE'],11);
	$poverh=get_tupes($val['pr']['poverh']['VALUE'],13);
	$picture=get_tupes($val['pr']['picture']['VALUE'],15);
	$tupe=get_tupes($val['pr']['mat_true']['VALUE'],20);
	$ed=$val['pr']['ed']['VALUE'];
	$razmer=$val['pr']['shir']['VALUE']."x".$val['pr']['visota']['VALUE'];
	$kol_kor=$val['pr']['col_kor']['VALUE'];
	$kolm_kor=$val['pr']['m_kor']['VALUE'];
	$ves_kor=$val['pr']['ves_kor']['VALUE'];
	$pom=get_tupes($val['pr']['pom']['VALUE'],19);
	$mat=get_tupes($val['pr']['tupe']['VALUE'],18);
                                                                            
$text.=$id.';'.$section.';'.$colection.';'.$cod_ruus.';'.$cod_kerama.';'.$name.';'.$arPrice['PRICE'].';'.$cvet.';'.$naznachenie.';'.$poverh.';'.$picture.';'.$tupe.';'.$ed.';'.$razmer.';'.$kol_kor.';'.$kolm_kor.';'.$ves_kor.';'.$pom.';'.$mat.';'."\n";
                                                                                                                                                                       
}
//echo $text;
file_put_contents($_SERVER['DOCUMENT_ROOT']."/1_export.csv", iconv("utf-8","windows-1251",$text));

echo 'Выгружено '.count($arr['a']).' елементов<br>';
echo "<a href='/1_export.csv'>Загрузить експорт данных</a>";
?>