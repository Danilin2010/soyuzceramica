<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Сменить пароль");

global $USER;
if ( strlen($_REQUEST["USER_CHECKWORD"]) > 0){
	$rsUser = CUser::GetByLogin($_REQUEST["USER_LOGIN"]);
	if($arUser = $rsUser->Fetch())
	{
		if ($arUser["CHECKWORD"] === $_REQUEST["USER_CHECKWORD"]){
			$USER->Authorize(intval($arUser["ID"]));
		}
	}
}

	if(!$USER->isAuthorized()){LocalRedirect(SITE_DIR.'auth');} else {
?>

<?$APPLICATION->IncludeComponent("bitrix:main.profile", "change_password", array(
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"SET_TITLE" => "N",
	"USER_PROPERTY" => array(
	),
	"SEND_INFO" => "N",
	"CHECK_RIGHTS" => "N",
	"USER_PROPERTY_NAME" => "",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>