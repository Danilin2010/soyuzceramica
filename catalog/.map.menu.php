<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;

$aMenuLinksExt=$APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
    "IBLOCK_ID" => "50",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "360000",
    "DEPTH_LEVEL" => "4",
),
    false,
    Array('HIDE_ICONS' => 'N')
);
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);