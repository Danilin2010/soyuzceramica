<?
$arMenuAddedSEO = Array(
    Array(
      'NAME' => 'Для ванной',
      'SECTION_PAGE_URL' => '/catalog/vanna/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_03.jpg'),
      'CHILD' => Array(
          Array(
            'NAME' => 'Мозаика',
            'SECTION_PAGE_URL' => '/catalog/vanna/mozaika/',
            'SELECTED' => '',
            'IMAGES' => Array('src' => '/catalog/.images/keramma_18.jpg'),
          ),  
          Array(
            'NAME' => 'Кабанчик',
            'SECTION_PAGE_URL' => '/catalog/vanna/kabanchik/',
            'SELECTED' => '',
            'IMAGES' => Array('src' => '/catalog/.images/keramma_19.jpg'),
          ),  
      ),
    ),
    Array(
      'NAME' => 'Для пола',
      'SECTION_PAGE_URL' => '/catalog/pol/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_05.jpg'),
    ),        
    Array(
      'NAME' => 'Уличная плитка',
      'SECTION_PAGE_URL' => '/catalog/ylichnaya/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_07.jpg'),
    ),
    Array(
      'NAME' => 'Для кухни',
      'SECTION_PAGE_URL' => '/catalog/kuhnya/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_09.jpg'),
      'CHILD' => Array(
          Array(
            'NAME' => 'Для кухни на пол',
            'SECTION_PAGE_URL' => '/catalog/kuhnya/pol/',
            'SELECTED' => '',
            'IMAGES' => Array('src' => '/catalog/.images/keramma_09.jpg'),
          ),  
          Array(
            'NAME' => 'Мозаика',
            'SECTION_PAGE_URL' => '/catalog/kuhnya/mozaika/',
            'SELECTED' => '',
            'IMAGES' => Array('src' => '/catalog/.images/keramma_18.jpg'),
          ),  
          Array(
            'NAME' => 'Кабанчик',
            'SECTION_PAGE_URL' => '/catalog/kuhnya/kaabanchik/',
            'SELECTED' => '',
            'IMAGES' => Array('src' => '/catalog/.images/keramma_19.jpg'),
          ),  
      ),

    ),
    Array(
      'NAME' => 'Для ступеней лестницы',
      'SECTION_PAGE_URL' => '/catalog/stupenei/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_11.jpg'),
    ),
    Array(
      'NAME' => 'Мозаика',
      'SECTION_PAGE_URL' => '/catalog/mozaika/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_18.jpg'),
    ),
    Array(
      'NAME' => 'Кабанчик',
      'SECTION_PAGE_URL' => '/catalog/kabanchik/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_19.jpg'),
    ),
    Array(
      'NAME' => 'Моноколор',
      'SECTION_PAGE_URL' => '/catalog/monokolor/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_20.jpg'),
    ),
    Array(
      'NAME' => 'Мелкоформатная',
      'SECTION_PAGE_URL' => '/catalog/melkoformat/',
      'SELECTED' => '',
      'IMAGES' => Array('src' => '/catalog/.images/keramma_21.jpg'),
    ),
    //  Array(
    //    'NAME' => 'Панели',
    //    'SECTION_PAGE_URL' => '/catalog/paneli/',
    //    'SELECTED' => '',
    //  ),

);