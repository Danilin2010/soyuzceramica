<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Керамическая плитка для ванной | Союз Керамика");
$APPLICATION->SetTitle("Керамическая плитка для ванной комнаты: как выбрать");
$APPLICATION->SetPageProperty("description", "«Союз Керамика» - официальный дистрибьютор KERAMA MARAZZI. Большой выбор керамической плитки и керамогранита. ".$APPLICATION->GetTitle()." +7(495)5069698");

GLOBAL $USER;
if ($USER->IsAdmin()){
    //echo '<pre>'; print_r(get_defined_vars()); echo '</pre>';
}
?>
<ul class="subsections-seo">
  <li>
    <span class="image"><a href="/catalog/vanna/mozaika/"><img src="/catalog/.images/keramma_18.jpg" alt="Плитка мозаика для ванной"></a></span>
    <a class="section" href="/catalog/vanna/mozaika/"><span>Плитка мозаика для ванной</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/vanna/kabanchik/"><img src="/catalog/.images/keramma_19.jpg" alt="Плитка кабанчик ванна"></a></span>
    <a class="section" href="/catalog/vanna/kabanchik/"><span>Плитка кабанчик ванна</span></a>
    <div class="clearfix"></div>
  </li>
  <!--  <li>
    <span class="image"><a href="/catalog/vanna/paneli/"><img src="/catalog/.images/keramma_03.jpg" alt="Панели под плитку для ванной"></a></span>
    <a class="section" href="/catalog/vanna/paneli/"><span>Панели под плитку для ванной</span></a>
    <div class="clearfix"></div>
  </li>-->
  <div class="clearfix"></div>
</ul>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.top.php', Array(), Array('MODE' => 'html'));?>
</div>
<div class="catalog">
  <div class="ajax_load block">
    <? 
      global $arrFilter; 
      //$arrFilter = Array('PROPERTY_pom_VALUE' => Array(124, 87));
    ?>
    <?$APPLICATION->IncludeFile('/catalog/inc.catalog.section.php', Array(), Array('MODE' => 'php'));?>
    <div class="clearfix"></div>
  </div>
</div>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.bottom.php', Array(), Array('MODE' => 'html'));?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>