<?$APPLICATION->SetTitle("Каталог дилера");?>
<?$APPLICATION->SetPageProperty("title", "Каталог дилера");?>
<?
if($_REQUEST['arFilter'])
{
  foreach($_REQUEST['arFilter'] as $param => $values)
  {
    //$values = iconv('windows-1251', 'UTF-8', $values);
    $arFilter[$param] = explode('-and-', $values);
  }
}
?>
<div class="catalog-section dillers-block">
  <?$APPLICATION->IncludeComponent("bitrix:catalog.filter", "diler", Array(
    "FILTER_PARAMS" => $arFilter,
      "CACHE_GROUPS" => "Y",	// Учитывать права доступа
      "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
      "CACHE_TYPE" => "A",	// Тип кеширования
      "FIELD_CODE" => array(	// Поля
        0 => "",
        1 => "",
      ),
      "FILTER_NAME" => "arrFilter",	// Имя выходящего массива для фильтрации
      "IBLOCK_ID" => "50",	// Инфоблок
      "IBLOCK_TYPE" => "aspro_optimus_catalog",	// Тип инфоблока
      "LIST_HEIGHT" => "5",	// Высота списков множественного выбора
      "NUMBER_WIDTH" => "5",	// Ширина полей ввода для числовых интервалов
      "PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
      "PRICE_CODE" => array(	// Тип цены
        0 => $PRICE_CODE,
      ),
      "PRICE_ID" => $arPriceGroups[$PRICE_CODE]['ID'],
      "PROPERTY_CODE" => array(	// Свойства
        0 => "tupe",
        1 => "naznachenie",
        2 => "poverh",
        3 => "razm",
        4 => "picture",
        5 => "color",
      ),
      "SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
      "TEXT_WIDTH" => "20",	// Ширина однострочных текстовых полей ввода
      "COMPONENT_TEMPLATE" => ".default",
      "OFFERS_FIELD_CODE" => array(	// Поля предложений
        0 => "",
        1 => "",
      ),
      "OFFERS_PROPERTY_CODE" => array(	// Свойства предложений
        0 => "",
        1 => "",
      ),
      "COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
      "COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
    ),
    false
  );?>

  <?
  
//$article = 'SG216500R';
//$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CODE_ANALOG", "PROPERTY_ANALOG");
//$rsAnalogs = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "PROPERTY_CML2_ARTICLE" => $article), false, false, $arSelect);
//if($rsAnalogs->SelectedRowsCount() > 0)
//{
//  $arAnalog = $rsAnalogs->Fetch();
//  if($arAnalog['PROPERTY_ANALOG_VALUE'] == 'Y')
//  {
//    $rsProduct = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "!PROPERTY_ANALOG" => 'Y', "PROPERTY_CODE_ANALOG" => $arAnalog['PROPERTY_CODE_ANALOG_VALUE']), false, false, $arSelect);
//    if($rsProduct->SelectedRowsCount() > 0)
//    {
//      $arProduct = $rsProduct->Fetch();
//      dbgz($arProduct['PROPERTY_CML2_ARTICLE_VALUE']);
//    }
//  }
//}

  
  
  global $arrFilter;
  foreach($arFilter as $param => $values)
  {
    if(in_array($param, Array('bxajaxid', 'AJAX_CALL')))
      continue;
    
    if(in_array($param, Array('sections')))
    {
      $arrFilter['SECTION_ID'] = explode(',', $values[0]);
    }
    elseif(in_array($param, Array('name')))
    {
      
      // Если вдруг введён артикул аналога, то надо заменить его на артикул товара
      $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CODE_ANALOG", "PROPERTY_ANALOG");
      $rsAnalogs = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "PROPERTY_CML2_ARTICLE" => trim($values[0]), "PROPERTY_ANALOG_VALUE" => "Y"), false, false, $arSelect);
      if($rsAnalogs->SelectedRowsCount() > 0)
      {
        $arAnalog = $rsAnalogs->Fetch();
        if($arAnalog['PROPERTY_ANALOG_VALUE'] == 'Y')
        {
          $rsProduct = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_CATALOG_ID, "!PROPERTY_ANALOG_VALUE" => 'Y', "PROPERTY_CODE_ANALOG" => $arAnalog['PROPERTY_CODE_ANALOG_VALUE']), false, false, $arSelect);
          if($rsProduct->SelectedRowsCount() > 0)
          {
            while($arProduct = $rsProduct->Fetch())
            {
              $values[0] = $arProduct['PROPERTY_CML2_ARTICLE_VALUE'];
            }
          }
        }
      }
      
      $arrFilter[] = Array(
        'LOGIC' => 'OR',
        'NAME' => '%'. $values[0] .'%',
        'PROPERTY_CML2_ARTICLE' => '%'. $values[0] .'%',
      );
    }
    elseif(in_array($param, Array('pricef', 'pricet')))
    {
      if($param == 'pricef')
        $arrFilter[] = Array('>=CATALOG_PRICE_'. $arPriceGroups[$PRICE_CODE]['ID'] => $values[0]);
      
      if($param == 'pricet')
        $arrFilter[] = Array('<=CATALOG_PRICE_'. $arPriceGroups[$PRICE_CODE]['ID'] => $values[0]);
    }
    elseif(in_array($param, Array('naznachenie')))
    {
      //dbg($values);
      
      $arrFilter[] = Array('LOGIC' => 'OR', Array('PROPERTY_'. $param => $values));

      //$arValues = Array('PROPERTY_'. strtoupper($param) => $values);
    }
    else
    {
      $arValues = Array('PROPERTY_'. strtoupper($param) => $values);
      $arrFilter[] = $arValues;
    }

  }
  
  //dbg($arrFilter); 
  $GLOBALS["arrFilter"]["!PROPERTY_ANALOG_VALUE"] = "Y";
  ?>
  <div id="diler-products">
    <?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section", 
    "catalog_diler", 
    array(
      "ACTION_VARIABLE" => "action",
      "ADD_PICT_PROP" => "-",
      "ADD_PROPERTIES_TO_BASKET" => "Y",
      "ADD_SECTIONS_CHAIN" => "N",
      "ADD_TO_BASKET_ACTION" => "ADD",
      "AJAX_MODE" => "Y",
      "AJAX_OPTION_ADDITIONAL" => "",
      "AJAX_OPTION_HISTORY" => "N",
      "AJAX_OPTION_JUMP" => "N",
      "AJAX_OPTION_STYLE" => "Y",
      "BACKGROUND_IMAGE" => "-",
      "BASKET_URL" => "/basket/",
      "BROWSER_TITLE" => "-",
      "CACHE_FILTER" => "N",
      "CACHE_GROUPS" => "Y",
      "CACHE_TIME" => "36000000",
      "CACHE_TYPE" => "N",
      "COMPATIBLE_MODE" => "Y",
      "COMPOSITE_FRAME_MODE" => "A",
      "COMPOSITE_FRAME_TYPE" => "AUTO",
      "CONVERT_CURRENCY" => "N",
      "CUSTOM_FILTER" => "",
      "DETAIL_URL" => "",
      "DISABLE_INIT_JS_IN_COMPONENT" => "N",
      "DISPLAY_BOTTOM_PAGER" => "Y",
      "DISPLAY_COMPARE" => "N",
      "DISPLAY_TOP_PAGER" => "N",
      "ELEMENT_SORT_FIELD" => "sort",
      "ELEMENT_SORT_FIELD2" => "id",
      "ELEMENT_SORT_ORDER" => "asc",
      "ELEMENT_SORT_ORDER2" => "desc",
      "ENLARGE_PRODUCT" => "STRICT",
      "FILTER_NAME" => "arrFilter",
      "HIDE_NOT_AVAILABLE" => "N",
      "HIDE_NOT_AVAILABLE_OFFERS" => "N",
      "IBLOCK_ID" => "50",
      "IBLOCK_TYPE" => "aspro_optimus_catalog",
      "INCLUDE_SUBSECTIONS" => "Y",
      "LABEL_PROP" => "",
      "LAZY_LOAD" => "N",
      "LINE_ELEMENT_COUNT" => "3",
      "LOAD_ON_SCROLL" => "N",
      "MESSAGE_404" => "",
      "MESS_BTN_ADD_TO_BASKET" => "В корзину",
      "MESS_BTN_BUY" => "Купить",
      "MESS_BTN_DETAIL" => "Подробнее",
      "MESS_BTN_SUBSCRIBE" => "Подписаться",
      "MESS_NOT_AVAILABLE" => "Нет в наличии",
      "META_DESCRIPTION" => "-",
      "META_KEYWORDS" => "-",
      "OFFERS_CART_PROPERTIES" => array(
      ),
      "OFFERS_FIELD_CODE" => array(
        0 => "",
        1 => "",
      ),
      "OFFERS_LIMIT" => "5",
      "OFFERS_PROPERTY_CODE" => array(
        0 => "",
        1 => "",
      ),
      "OFFERS_SORT_FIELD" => "sort",
      "OFFERS_SORT_FIELD2" => "id",
      "OFFERS_SORT_ORDER" => "asc",
      "OFFERS_SORT_ORDER2" => "desc",
      "PAGER_BASE_LINK_ENABLE" => "N",
      "PAGER_DESC_NUMBERING" => "N",
      "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
      "PAGER_SHOW_ALL" => "N",
      "PAGER_SHOW_ALWAYS" => "N",
      "PAGER_TEMPLATE" => ".default",
      "PAGER_TITLE" => "Товары",
      "PAGE_ELEMENT_COUNT" => "50",
      "PARTIAL_PRODUCT_PROPERTIES" => "N",
      "PRICE_CODE" => array(
        0 => $PRICE_CODE,
      ),
      "PRICE_ID" => $arPriceGroups[$PRICE_CODE]['ID'],
      "PRICE_VAT_INCLUDE" => "Y",
      "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
      "PRODUCT_DISPLAY_MODE" => "N",
      "PRODUCT_ID_VARIABLE" => "id",
      "PRODUCT_PROPERTIES" => array(
      ),
      "PRODUCT_PROPS_VARIABLE" => "prop",
      "PRODUCT_QUANTITY_VARIABLE" => "quantity",
      "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
      "PRODUCT_SUBSCRIPTION" => "Y",
      "PROPERTY_CODE" => array(
        0 => "",
        1 => "",
      ),
      "PROPERTY_CODE_MOBILE" => "",
      "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
      "RCM_TYPE" => "personal",
      "BY_LINK" => "Y", // Если необходимо вывести элементы без привязки к SECTIONs, достаточно указать скрытый параметр - "BY_LINK" со значением Y. 
      "SECTION_CODE" => "",
      "SECTION_ID" => "",
      "SECTION_ID_VARIABLE" => "SECTION_ID",
      "SECTION_URL" => "",
      "SECTION_USER_FIELDS" => array(
        0 => "",
        1 => "",
      ),
      "SEF_MODE" => "N",
      "SET_BROWSER_TITLE" => "Y",
      "SET_LAST_MODIFIED" => "N",
      "SET_META_DESCRIPTION" => "Y",
      "SET_META_KEYWORDS" => "Y",
      "SET_STATUS_404" => "N",
      "SET_TITLE" => "Y",
      "SHOW_404" => "N",
      "SHOW_ALL_WO_SECTION" => "N",
      "SHOW_CLOSE_POPUP" => "N",
      "SHOW_DISCOUNT_PERCENT" => "N",
      "SHOW_FROM_SECTION" => "N",
      "SHOW_MAX_QUANTITY" => "N",
      "SHOW_OLD_PRICE" => "N",
      "SHOW_PRICE_COUNT" => "1",
      "SHOW_SLIDER" => "N",
      "SLIDER_INTERVAL" => "3000",
      "SLIDER_PROGRESS" => "N",
      "TEMPLATE_THEME" => "blue",
      "USE_ENHANCED_ECOMMERCE" => "N",
      "USE_MAIN_ELEMENT_SECTION" => "N",
      "USE_PRICE_COUNT" => "N",
      "USE_PRODUCT_QUANTITY" => "N",
      "COMPONENT_TEMPLATE" => "catalog_diler"
    ),
    false
  );?>
  </div>
  <div class="overlay-block-diler"></div>
</div>