<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Плитка для кухни | Союз Керамика");
$APPLICATION->SetPageProperty("description", "«Союз Керамика» - официальный дистрибьютор KERAMA MARAZZI. Большой выбор керамической плитки и керамогранита. Кухонная плитка. Правила выбора. +7(495)5069698");
$APPLICATION->SetTitle("Кухонная плитка. Правила выбора");
?>
<ul class="subsections-seo">
  <li>
    <span class="image"><a href="/catalog/kuhnya/pol/"><img src="/catalog/.images/keramma_09.jpg" alt="Плитка для кухни на пол"></a></span>
    <a class="section" href="/catalog/kuhnya/pol/"><span>Плитка для кухни на пол</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/kuhnya/mozaika/"><img src="/catalog/.images/keramma_18.jpg" alt="Плитка мозаика для кухни"></a></span>
    <a class="section" href="/catalog/kuhnya/mozaika/"><span>Плитка мозаика для кухни</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/kuhnya/kabanchik/"><img src="/catalog/.images/keramma_19.jpg" alt="Плитка кабанчик на кухне"></a></span>
    <a class="section" href="/catalog/kuhnya/kabanchik/"><span>Плитка кабанчик на кухне</span></a>
    <div class="clearfix"></div>
  </li>
<!--  <li>
    <span class="image"><a href="/catalog/kuhnya/paneli/"><img src="/catalog/.images/keramma_03.jpg" alt="Панель плитка на кухню"></a></span>
    <a class="section" href="/catalog/kuhnya/paneli/"><span>Панель плитка на кухню</span></a>
    <div class="clearfix"></div>
  </li>-->
  <div class="clearfix"></div>
</ul>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.top.php', Array(), Array('MODE' => 'html'));?>
</div>
<div class="catalog">
  <div class="ajax_load block">
    <? 
      global $arrFilter; 
      //$arrFilter = Array('SECTION_ID' => 4124);
    ?>
    <?$APPLICATION->IncludeFile('/catalog/inc.catalog.section.php', Array(), Array('MODE' => 'php'));?>
    <div class="clearfix"></div>
  </div>
</div>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.bottom.php', Array(), Array('MODE' => 'html'));?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>