<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Плитка «Кабанчик» для кухни | Союз Керамика");
$APPLICATION->SetPageProperty("description", "«Союз Керамика» - официальный дистрибьютор KERAMA MARAZZI. Большой выбор керамической плитки и керамогранита. «Кабанчик» – отличный вариант плитки для кухни. +7(495)5069698");
$APPLICATION->SetTitle("«Кабанчик» – отличный вариант плитки для кухни");
?>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.top.php', Array(), Array('MODE' => 'html'));?>
</div>
<div class="catalog">
  <div class="ajax_load block">
    <? 
      global $arrFilter; 
      //$arrFilter = Array('SECTION_ID' => 4124);
    ?>
    <?$APPLICATION->IncludeFile('/catalog/inc.catalog.section.php', Array(), Array('MODE' => 'php'));?>
    <div class="clearfix"></div>
  </div>
</div>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.bottom.php', Array(), Array('MODE' => 'html'));?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>