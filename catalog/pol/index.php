<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Керамическая плитка для пола | Союз Керамика");
$APPLICATION->SetPageProperty("description", "«Союз Керамика» - официальный дистрибьютор KERAMA MARAZZI. Большой выбор керамической плитки и керамогранита. Покупаем керамическую плитку для пола. +7(495)5069698");
$APPLICATION->SetTitle("Покупаем керамическую плитку для пола");
?>

<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.top.php', Array(), Array('MODE' => 'html'));?>
</div>
<div class="catalog">
  <div class="ajax_load block">
    <? 
      global $arrFilter; 
      //$arrFilter = Array('PROPERTY_pom_VALUE' => 124);
    ?>
    <?$APPLICATION->IncludeFile('/catalog/inc.catalog.section.php', Array(), Array('MODE' => 'php'));?>
    <div class="clearfix"></div>
  </div>
</div>
<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.bottom.php', Array(), Array('MODE' => 'html'));?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>