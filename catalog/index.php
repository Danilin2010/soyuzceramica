<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Керамическая плитка Kerama Marazzi | Союз Керамика");
$APPLICATION->SetPageProperty("description", "«Союз Керамика» - официальный дистрибьютор KERAMA MARAZZI.  Большой выбор керамической плитки и керамогранита. Керамическая плитка Kerama Marazzi: богатство выбора в интернет-магазине soyuzceramica.ru. +7(495)5069698");
$APPLICATION->SetTitle("Керамическая плитка Kerama Marazzi: богатство выбора в интернет-магазине soyuzceramica.ru");
?>
<?global $USER;
$PRICE_CODE   = "BASE";
$USERGroups   = $USER->GetUserGroupArray();
$dbResultList = CCatalogGroup::GetListEx(array($by => $order), array(), false, false);
while ($arGroup = $dbResultList->Fetch()) {
  $arPriceGroups[$arGroup['NAME']] = $arGroup;
	if ($arGroup["NAME"] !== "BASE" && $arGroup["NAME"] !== "base") {
		$rsGroups = CCatalogGroup::GetGroupsList(array("CATALOG_GROUP_ID" => intval($arGroup["ID"])));
		while ($arUGroup = $rsGroups->Fetch()) {
			if (in_array($arUGroup["GROUP_ID"], $USERGroups)) {
				$PRICE_CODE = $arGroup["NAME"];
				break;
			}
		}
	}
}?>

<?if(IS_DILER):?>
  <?include 'inc.catalog.diler.php';?>
<?else:?>
  <?include 'inc.catalog.main.php';?>
<?endif?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>