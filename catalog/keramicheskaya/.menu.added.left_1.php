<?
$arMenuAddedSEO = Array(
	Array(
		'NAME' => 'По назначению и типу',
    'SECTION_PAGE_URL' => '/catalog/keramicheskaya/',
		'SELECTED' => '',
    'UF_SPLIT' => 'Y',
    'CHILD' => Array(
        Array(
          'NAME' => 'Для ванной',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/vanna/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_03.jpg'),
          'CHILD' => Array(
              Array(
                'NAME' => 'Мозаика',
                'SECTION_PAGE_URL' => '/catalog/keramicheskaya/vanna/mozaika/',
                'SELECTED' => '',
              ),  
              Array(
                'NAME' => 'Кабанчик',
                'SECTION_PAGE_URL' => '/catalog/keramicheskaya/vanna/kabanchik/',
                'SELECTED' => '',
              ),  
          ),
        ),
        Array(
          'NAME' => 'Для пола',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/pol/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_05.jpg'),
        ),        
        Array(
          'NAME' => 'Уличная плитка',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/ylichnaya/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_07.jpg'),
        ),
        Array(
          'NAME' => 'Для кухни',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/kuhnya/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_09.jpg'),
          'CHILD' => Array(
              Array(
                'NAME' => 'Для кухни на пол',
                'SECTION_PAGE_URL' => '/catalog/keramicheskaya/kuhnya/pol/',
                'SELECTED' => '',
              ),  
              Array(
                'NAME' => 'Мозаика',
                'SECTION_PAGE_URL' => '/catalog/keramicheskaya/kuhnya/mozaika/',
                'SELECTED' => '',
              ),  
              Array(
                'NAME' => 'Кабанчик',
                'SECTION_PAGE_URL' => '/catalog/keramicheskaya/kuhnya/kaabanchik/',
                'SELECTED' => '',
              ),  
          ),

        ),
        Array(
          'NAME' => 'Для ступеней лестницы',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/stupenei/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_11.jpg'),
        ),
        Array(
          'NAME' => 'Мозаика',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/mozaika/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_18.jpg'),
        ),
        Array(
          'NAME' => 'Кабанчик',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/kabanchik/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_19.jpg'),
        ),
        Array(
          'NAME' => 'Моноколор',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/monokolor/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_20.jpg'),
        ),
        Array(
          'NAME' => 'Мелкоформатная',
          'SECTION_PAGE_URL' => '/catalog/keramicheskaya/melkoformat/',
          'SELECTED' => '',
          'IMAGES' => Array('src' => '/catalog/keramicheskaya/.images/keramma_21.jpg'),
        ),
        //  Array(
        //    'NAME' => 'Панели',
        //    'SECTION_PAGE_URL' => '/catalog/keramicheskaya/paneli/',
        //    'SELECTED' => '',
        //  ),
    ),
  ),
);