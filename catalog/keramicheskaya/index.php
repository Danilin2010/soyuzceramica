<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Керамическая плитка"); 
?>
<div class="section_block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.top.php', Array(), Array('MODE' => 'html'));?>
</div>

<ul class="subsections-seo">
  <li>
    <span class="image"><a href="/catalog/dreams-of-paris/"><img src="/catalog/keramicheskaya/.images/keramma_43.jpg" alt="Мечты о Париже"></a></span>
    <a class="section" href="/catalog/dreams-of-paris/"><span>Мечты о Париже</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/dve-venetsii/"><img src="/catalog/keramicheskaya/.images/keramma_45.jpg" alt="Две Венеции"></a></span>
    <a class="section" href="/catalog/dve-venetsii/"><span>Две Венеции</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/neapolitanskaya-kollektsiya/"><img src="/catalog/keramicheskaya/.images/keramma_47.jpg" alt="Неаполитанская коллекция"></a></span>
    <a class="section" href="/catalog/neapolitanskaya-kollektsiya/"><span>Неаполитанская коллекция</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/angliyskaya-kollektsiya/"><img src="/catalog/keramicheskaya/.images/keramma_49.jpg" alt="Английская коллекция"></a></span>
    <a class="section" href="/catalog/angliyskaya-kollektsiya/"><span>Английская коллекция</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/indiyskaya-kollektsiya/"><img src="/catalog/keramicheskaya/.images/keramma_50.jpg" alt="Индийская коллекция"></a></span>
    <a class="section" href="/catalog/indiyskaya-kollektsiya/"><span>Индийская коллекция</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/italyanskaya-kollektsiya/"><img src="/catalog/keramicheskaya/.images/keramma_56.jpg" alt="Итальянская коллекция"></a></span>
    <a class="section" href="/catalog/italyanskaya-kollektsiya/"><span>Итальянская коллекция</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/palitra/"><img src="/catalog/keramicheskaya/.images/keramma_57.jpg" alt="Палитра"></a></span>
    <a class="section" href="/catalog/palitra/"><span>Палитра</span></a>
    <div class="clearfix"></div>
  </li>
  <li>
    <span class="image"><a href="/catalog/keramicheskiy-granit/"><img src="/catalog/keramicheskaya/.images/keramma_58.jpg" alt="Керамический гранит"></a></span>
    <a class="section" href="/catalog/keramicheskiy-granit/"><span>Керамический гранит</span></a>
    <div class="clearfix"></div>
  </li>
  <div class="clearfix"></div>
</ul>

  

<h2>По назначению</h2>
<?$APPLICATION->IncludeFile(SITE_DIR .'/catalog/keramicheskaya/subsections.php', Array(), Array('MODE' => 'php'));?>
  
<h2>По типу</h2>
<?$APPLICATION->IncludeFile(SITE_DIR .'/catalog/keramicheskaya/subsections.type.php', Array(), Array('MODE' => 'php'));?>


<div class="section_block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.bottom.php', Array(), Array('MODE' => 'html'));?>
</div>

  <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>