<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if ($_POST['USER_ID']) {

        $rsUser = CUser::GetByID(intval($_POST['USER_ID']));
        $arUser = $rsUser->Fetch();
        $user_fields = array(
            "NAME" => $arUser["NAME"],
            "LAST_NAME" => $arUser["LAST_NAME"],
            "LOGIN" => $arUser["LOGIN"],
            "MAIL" => $arUser["EMAIL"],
            "PHONE" => $arUser["PERSONAL_PHONE"],
            "USER_ID" => $arUser["ID"]
        );
        if ($arUser["LAST_NAME"]) {
            $arName[] = $arUser["LAST_NAME"];
        }
        if ($arUser["NAME"]) {
            $arName[] = $arUser["NAME"];
        }
        if ($arUser["SECOND_NAME"]) {
            $arName[] = $arUser["SECOND_NAME"];
        }
        $arUser["NAME"] = implode(' ', $arName);
        $arAliases = array(
            "NAME" => "ФИО",
            "EMAIL" => "E-mail",
            "PERSONAL_PHONE" => "Телефон",
            "PERSONAL_MOBILE" => "Дополнительный телефон",
            "UF_WEB_SITES" => "Сайты",
            "UF_POINT_OF_SALE" => "Точки продаж",
            "UF_DEP_CODE" => "Код подразделения",
            "WORK_COMPANY" => "Наименование организации",
            "UF_COMPANY_INN" => "ИНН",
            "UF_COMPANY_KPP" => "КПП",
            "UF_COMPANY_OGRN" => "ОГРН",
            "UF_COMPANY_ADDRESS" => "Юридический адрес",
            "UF_BANK_DETAILS" => "Банковские реквизиты",
            "UF_POST_ADDRESS" => "Почтовый адрес"
        );

        $c = 0;
        $message = "";
        if (!empty($_POST['NEW_USER'])) {
            $message .= "Пользователь заполнил все необходимые поля чтобы стать дилером.<br/>Проверьте его данные:<br><br>";
        } else {
            $message .= "Дилером были изменены следующие данные:<br><br>";
        }
        $message .= "<table style='border-collapse:collapse;border-spacing:0;'>";
        foreach ($_POST as $key => $val) {
            if (trim($arUser[$key]) !== trim($_POST[$key]) && $key !== "USER_ID" && $key !== "NEW_USER") {
//                $user_fields[$key] = $_POST[$key];
                $message .= "<tr>";
                $message .= "<td style='padding:3px 10px;text-align:right;border:1px solid #aaa;'>" . $arAliases[$key] . "</td><td style='padding:3px 10px;border:1px solid #aaa;'>" . $_POST[$key] . "</td>";
                $message .= "</tr>";
                ++$c;
            }
        }
        $message .= "</table>";
        if ($arUser["UF_SEND_DATA"] == false) {
            $user = new CUser;
            $fields = Array(
                "UF_SEND_DATA" => 1
            );
            $user->Update(intval($_POST['USER_ID']), $fields);
            $error = $user->LAST_ERROR;
        }
        if (!empty($user_fields)) {
            $arMail = array(
                "URL" => "/bitrix/admin/user_edit.php?lang=ru&ID=" . $_POST['USER_ID'],
                "USER_NAME" => $arUser["LOGIN"],
                "MESSAGE" => $message,
                "MAIL" => "solodov.mn@gmail.com"
            );
            
            $arMail = array_merge($arMail, $user_fields);

            if ($c > 0) {
                CEvent::Send('DILLER_EDIT', 's1', $arMail, 67);
                echo "ok";
            } else {
                echo "Изменений не было внесено";
            }
        }
    }
}
?>