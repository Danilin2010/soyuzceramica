<div class="topOneAdress clearFix">
	<h3>Единая справочная и головной офис:</h3>
	<p class="workTime">
 <span class="tel">+7 495 506-96-98</span>
	</p>
 <br>
	<p class="workTime">
 <span class="time">
		Время работы: с 10:00 до 20:00 c понедельника по субботу, в воскресение с 10:00 до 18:00</span>
	</p>
 <br>
	<p class="adress">
		 г. Москва, ул. Дмитрия Ульянова, д. 42с1 <a href="https://yandex.ru/maps/?um=constructor:aVHA7QdGxzJMwZ8LZAuNhgjiLZRJainD&source=constructorLink"> Открыть в Яндекс.Картах </a>
	</p>
<br>
<br>
<p class="workTime">
	По всем вопросам Вы можете обращаться на наш электронный ящик <a href="mailto:manager@soyuzceramica.ru">manager@soyuzceramica.ru</a>
	</p>
</div>
<div class="topOneAdress clearFix">
	<h3>
	Склад и пункт самовывоза:</h3>
	<p class="workTime">
 <span class="tel">
		+7 495 739-07-37</span>,
	</p>
 <br>
	<p class="workTime">
 <span class="time">
		Время работы: с 11:00 до 17:00 с понедельника по субботу</span>
	</p>
 <br>
	<p class="adress">
		 г. Москва, ЮАО, район Царицыно, ул. Промышленная, 11Ас47 <a href="https://yandex.ru/maps/?um=constructor%3A2d7ba74e179a42e3f923f1d634161bdbfa84189ac25083a12986289df2c0d960&source=constructorLink"> Открыть в Яндекс.Картах <br>
		</a>
	</p>
	<p class="adress">
(<a href="http://soyuzceramica.ru/map/index.php" target="_blank">схема проезда</a>)
	</p>
</div>
 <br>