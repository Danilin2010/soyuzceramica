<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
//Для ошибок
$result = array();
/*var_dump($_GET);
die;*/
if ( $action = $_GET["action"] ):

	switch ($action) {
		case "update" :
			if ( ! ( $prod_id = $_GET["prod_id"] ) || ! ( $quant = $_GET["quant"] ) ) :
				$result['code'] = 1;
				$result['mess'] = "Ошибка сервера. Попробуйте позже.";
			else:
				if ( ! CSaleBasket::Update( $prod_id, array( "QUANTITY" => $quant) ) ) :
					$result['code'] = 1;
					$result['mess'] = "Ошибка сервера. Попробуйте позже.";
				else :
					$result['code'] = 0;
					$result['mess'] = "Корзина обновлена.";
				endif;
			endif;
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
			break;
		case "delete" :
			if ( ! ( $prod_id = $_GET["prod_id"] ) ) :
				$result['code'] = 1;
				$result['mess'] = "Ошибка сервера. Попробуйте позже.";
			else:
				if ( ! CSaleBasket::Delete( $prod_id ) ) :
					$result['code'] = 1;
					$result['mess'] = "Ошибка сервера. Попробуйте позже.";
				else :
					$result['code'] = 0;
					$result['mess'] = "Товар удален из корзины.";
				endif;
			endif;
			
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
			break;
		default :
			return false;
			break;
	}
endif;
?>