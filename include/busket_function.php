<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CSaleOrder::Add($arFields);
if ( !( $action = $_GET["action"] ) ):
	switch $action {
		case "update" :
			if ( !( $prod_id = $_GET["prod_id"] ) )
				return false;
			if ( !( $quant = $_GET["quant"] ) )
				return false;
			CSaleBasket::Update($prod_id, array( "QUANTITY" => $quant));
			break;
		default :
			return false;
			break;
	}
endif;
	
?>