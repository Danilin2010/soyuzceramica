<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><div class="contacts_map">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"map",
	Array(
		"COMPONENT_TEMPLATE" => "map",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONTROLS" => array(0=>"SCALELINE",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.66207259712683;s:10:\"yandex_lon\";d:37.60252319158199;s:12:\"yandex_scale\";i:11;s:10:\"PLACEMARKS\";a:2:{i:0;a:3:{s:3:\"LON\";d:37.589001880951;s:3:\"LAT\";d:55.681281216412;s:4:\"TEXT\";s:335:\"<b>ООО \"Союз Керамика\"</b><br> Офис</b><br>###RN###м. Академическая###RN###улица Дмитрия Ульянова, 42с1<br>###RN###оптовый отдел: +7 495 506-96-98;###RN###единая справочная: +7 495 506-96-98.<br>###RN###www.soyzceramica.ru###RN###manager@soyzceramica.ru\";}i:1;a:3:{s:3:\"LON\";d:37.633080457672;s:3:\"LAT\";d:55.631073283556;s:4:\"TEXT\";s:238:\"<b>ООО \"Союз Керамика\"</b><br> Склад и пункт самовывоза</b><br>###RN###Промышленная улица, 11Ас47###RN###Время работы: Пн-Пт с 11:00 до 19:00###RN###+7 495 739-07-37\";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_DBLCLICK_ZOOM",1=>"ENABLE_RIGHT_MAGNIFIER",2=>"ENABLE_DRAGGING",)
	)
);?>
</div>
<div class="wrapper_inner" itemscope="" itemtype="http://schema.org/Organization">
	<div class="contacts_left">
		<h4>Офис</h4>
		<div class="store_description">
			<div class="store_property">
				<div class="title">
					 Адрес
				</div>
				<div class="value" itemprop="address">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/address.php", Array(), Array("MODE" => "html", "NAME" => "Адрес"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					 Телефон
				</div>
				<div class="value" itemprop="telephone">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("MODE" => "html", "NAME" => "Телефон"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					 Email
				</div>
				<div class="value" itemprop="email">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/email.php", Array(), Array("MODE" => "html", "NAME" => "Email"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					 Режим работы
				</div>
				<div class="value">
					 <?$APPLICATION->IncludeFile(SITE_DIR."include/schedule.php", Array(), Array("MODE" => "html", "NAME" => "Время работы"));?>
				</div>
			</div>
		</div>
 <br>
		<div class="clear">
		</div>
		<h4>Склад</h4>
		<div class="store_description">
			<div class="store_property">
				<div class="title">
					 Адрес
				</div>
				<div class="value">
					 115516, Москва, Промышленная ул, 11Ас47
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					 Телефон
				</div>
				<div class="value">
 <a href="tel:+74957390737" rel="nofollow" class="phone-number">+7 (495) 739-07-37</a>
				</div>
			</div>
			<div class="store_property">
				<div class="title">
					 Режим работы
				</div>
				<div class="value">
					 пн&nbsp;– сб с 11:00 до&nbsp;19:00
				</div>
			</div>
		</div>
	</div>
	<div class="contacts_right">
		<blockquote itemprop="description">
			 <?$APPLICATION->IncludeFile(SITE_DIR."include/contacts_text.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("CONTACTS_TEXT")));?>
		</blockquote>
		 <?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("form-feedback-block");?> <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"inline",
	Array(
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "?send=ok",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
		"WEB_FORM_ID" => "3"
	)
);?> <?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("form-feedback-block", "");?>
	</div>
</div>
<div class="clearboth">
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>