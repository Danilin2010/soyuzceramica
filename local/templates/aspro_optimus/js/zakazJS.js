var formChanged = false;
$(document).ready(function(){
	$("input[name=phone]").inputmask("+9(999)999-99-99");
	
	$("input[name=phone]").mouseout(function(){
		var mask = $(this).attr("placeholder");
		if ($(this).val() == mask)
			$(this).clear();
		return;
	});
	
	$('.steps').each(function(){
		$(this).hide();
	});
	
	$('.steps.active').show();
	
	$('#costumer_form input').change(function(){formChanged = true});

	$('.stepMap a').click(function(e){
		if ($(this).parent().hasClass('disabled') || ( $(this).parent().is(":last-child") && formChanged )){
			return false;
			formChanged = false;
		}
		e.preventDefault();
		var $step = $(this).data('step');
		tabSelect( $step );
	});
	
	$('.next').click(function(){
		nextStep($(this));
	});
	
	$('.closeBtn a').click(function(){
		removeItem(jQuery,$(this).attr("ids"));
	});
	
	$(document).delegate('.zakaz .calculator .countMeters', 'change', function () {
		if ($(this).val() != NaN) {
			convertToPieces($(this));
		}
	});

	$(document).delegate('.zakaz .calculator .countItems', 'change', function () {
		if ($(this).val() != NaN) {
			convertToMetters($(this));
		}
		updateBasket($, $(this));
	});
	
	$(document).delegate('.zakaz .shippingType li', 'click', function () {
		if ($(this).data('shippingtype') == 2){
			$('input[name="costumer_address"]').val('г. Москва, ЮАО, район Царицыно, ул. Промышленная, 11А').attr('disabled',true);
		} else {
			$('input[name="costumer_address"]').val('').attr('disabled',false);
		}
		changeShippingTypeVisual($(this));
	});
	
	$('#costumer_form').submit(function(e){
		var data = $(this).serializeArray();
		createConfirmForm(data);
		confirmFormList();
		nextStep($(this));
		e.preventDefault();
	});
	
	$('#do_order').submit(function(e){
		formPlaceholder();
		var shippType = $('.shippingType .active').data('shippingType');
		$(this).append('<input type="hidden" value='+shippType+' name="deliver" />');
		var url = $(this).attr("action");
		var data = $(this).serialize();
		$.get(url,data).done(function(data){
			var obj = $.parseJSON(data);
			window.location = "?order_done=Y&order_id="+obj.mess;
		});	
		e.preventDefault();
	});
	
	sumPriceStep0();
	calcWeight();
});

function formPlaceholder(){
	$('.mainSide').css('position','relative').append('<div class="form-placeholder"><div class="form-preloader"></div></div>');
	$('.mainSide').append('<style>.form-placeholder{position:absolute;width:100%;height:100%;top:0;left:0;background:#fff;z-index:100;opacity:0.3;display:flex;align-items:center;justify-content:center;}.form-preloader{width:200px;height:200px;background:url(/img/preload.gif);background-size:100%;}</style>');
}
function confirmFormList(){
	var html = "";
	var calc = "";
	$("#do_order #order_body_list").empty()
	$("#do_order #order_body_list").prepend("<h2 class='second'>Состав заказа</h2>");
	html = "<style>tr{border-bottom: 10px solid transparent;}td{vertical-align:middle;text-align:center;}td:first-child{text-align:left;}</style><table style='width:100%;margin-bottom:20px;'>";
	//html += "<tr><th>Изображение</th><th>Название</th><th>Вес</th><th>Количество</th><th>Сумма</th></tr>";
	$('.myItem').each(function(){
		
		var boxName = $(this).find('.box.name').html();
		var calcM = $(this).find('.box.calculator').find('.countMeters').val();
		var calcI = $(this).find('.box.calculator').find('.countItems').val();
		var summ = $(this).find('.box.sum:not(.col)').html();
		var weight = $(this).find('.box.weight').html();
		var img = $(this).find('.imgWrap.box').html();
		
		//if (calcM !== undefined){
		//	calc = calcM+"м2 = "+calcI+"шт.";
		//} else {
			calc = calcI+"уп.";
		//}
		
		html += "<tr><td>"+img+"</td><td>"+boxName+"</td><td>"+weight+"</td><td>"+calc+"</td><td>"+summ+"</td></tr>";
		
		/*$('#order_body_list').append("<td>"+boxName+"</td>");
		$('#order_body_list').append("<td>"+calcM+"м2 = "+calcI+"шт.</td>");
		$('#order_body_list').append("<td>"+summ+"</td>");*/
		
	});
	html += "</table>";
	$('#order_body_list').append(html);		
	var sum = $("#summer").html();
	$('#order_body_list').append(sum);
}
function createConfirmForm(data){
	$("#do_order #order_body_costumer").empty();
	$.each(data, function(i, data){
		var fieldName = "";
		switch (data.name){
			case "costumer_address" :
				fieldName = "Адрес";
				break;
			case "phone" :
				fieldName = "Телефон";
				break;
			case "costumer_name" :
				fieldName = "Контактное лицо";
				break;
			case "email" : 
				fieldName = "E-mail";
				break;
			case "comment" : 
				fieldName = "Коментарий к заказу";
				break;
			default:
				break;
		}
		
		var value = data.value || "Не указанно.";
		
		$("#do_order #order_body_costumer").append("<div class='row'><b>"+fieldName+"</b><p>"+value+"</p></div>");
		
		$("#do_order #order_body_costumer").append("<input type='hidden' name='"+data.name+"' value='"+data.value+"' />");
	});
	var price = $('.calculatedResult').data('calculatedresult');
	var shipp = $('.shippingType .active').text();
	$("#do_order #order_body_costumer").append("<input type='hidden' name='price' value='"+price+"' />");
	$("#do_order #order_body_costumer").prepend("<div class='row'><b>Тип доставки:</b>"+shipp+"</div>");
	$("#do_order #order_body_costumer").prepend("<h2 class='second' style='margin-top:20px;'>Данные доставки</h2>");
	$("#do_order #order_body_costumer").prepend("<style>.row{margin-top:10px;}.second{color:#2e2e2e;}</style>");
	formChanged = false;
}

function updateBasket($, $quant){
	var prod_id = $quant.data("product_id");
	var quant = $quant.val();
	if (parseInt(quant) <= 0 || quant == ""){
		return false;
	}
	$.get("/include/busket_func.php",{action:"update",prod_id:prod_id,quant:quant}).done(function(data){
		var obj = $.parseJSON(data);
		showMsg(obj);
	});
}

function removeItem($, prod_id){
	$.get("/include/busket_func.php",{action:"delete",prod_id:prod_id,quant:0}).done(function(data){
		var obj = $.parseJSON(data);
		showMsg(obj);
	});
	location.reload();
}
function showMsg(obj){
	if (parseInt(obj.code) == 1){
		$('.msg-box').removeClass("success");
		$('.msg-box').addClass("err");
	}
	if (parseInt(obj.code) == 0){
		$('.msg-box').removeClass("err");
		$('.msg-box').addClass("success");
	}
	if ($('.msg-box').css("display") != "block")
		$('.msg-box').html(obj.mess).fadeIn(200).delay(2000).fadeOut(200);
}
function nextStep($obj){
	var $step = $obj.data('step');
	$step++;
	tabSelect($step);
}

function tabSelect($step){
	$step = parseInt($step);
	$('.steps').each(function(){
		$(this).hide();
	});
	$('#step'+$step).show();
	
	$('.stepMap li').each(function(){
		$(this).removeClass('active');
		var $aStep = $(this).find('a').data('step');
		
		if ($aStep == $step) {
			$(this).addClass('active');
			$(this).removeClass('disabled');
		}
	});
}

function changeShippingTypeVisual (eventObj) {
	var liButtons = $('.zakaz .shippingType li');
	for (var i = 0; i < liButtons.length; i++) {
		$(liButtons[i]).removeClass('active');
	}
	$(eventObj).addClass('active');
	toggleShipingTypeBlocks(eventObj);
}
function toggleShipingTypeBlocks (selectedType) {
	var typeBlocks = $('.zakaz .shippingBox'),
		selectedTypeData = $(selectedType).attr('data-shippingType');
	for (var i = 0; i < typeBlocks.length; i++) {
		var boxData = $(typeBlocks[i]).attr('data-shippingType');
		if (boxData != selectedTypeData) {
			$(typeBlocks[i]).removeClass('active');
		} else {
			$(typeBlocks[i]).addClass('active');
		}
	}
}

function convertToPieces (eventObj) {
	var meters = $(eventObj).val(),
	piecesInput = $(eventObj).parent().find('.countItems'),
	mmm = $(eventObj).parent().parent().find('.priceForOneMeter').attr('col_m'),
	hhh = $(eventObj).parent().parent().find('.priceForOneMeter').attr('col_h'),
	inputs=$(eventObj).parent().parent().find('.lastItemcol'),
	priceForOneMeter = $(eventObj).closest('.myItem').find('.priceForOneMeter').attr('data-priceforonemeter'),
	priceResult = $(eventObj).closest('.myItem').find('.sum .lastItemPrice');
	inputs.text(meters);
	$.get("/include/get_coll.php", {h: hhh,m: mmm,col: meters}).done(function(data) {
		var obj = $.parseJSON(data);
		$(piecesInput).val(obj.col_h);
		eventObj.val(obj.col_m);
		changeOnePriceOnInput(obj.col_h, priceForOneMeter, priceResult);
		sumPriceStep0();
	});
}

function convertToMetters (eventObj) {
	var pieces = $(eventObj).val(),
		mettersInput = $(eventObj).parent().find('.countMeters'),
		inputs=$(eventObj).parent().parent().find('.lastItemcol'),
		mmm = $(eventObj).parent().parent().find('.priceForOneMeter').attr('col_m'),
		hhh = $(eventObj).parent().parent().find('.priceForOneMeter').attr('col_h'),
		priceForOneMeter = $(eventObj).closest('.myItem').find('.priceForOneMeter').attr('data-priceforonemeter'),
		priceResult = $(eventObj).closest('.myItem').find('.sum .lastItemPrice');
	//alert(mettersInput.val().length);
	if(!mettersInput.val() && mettersInput.val()!=""){
		//alert('wwws');
		changeOnePriceOnInput(pieces, priceForOneMeter, priceResult);
		sumPriceStep0();
		inputs.text(pieces);
		//alert('ss');
	}else{
	//alert(hhh+' 2 '+mmm+' 3 '+pieces);
	$.get("/include/get_metr.php", {h: hhh,m: mmm,col: pieces}).done(function(data) {
		var obj = $.parseJSON(data);
		$(mettersInput).val(obj.col_m);
		eventObj.val(obj.col_h);
		//alert(data)
		changeOnePriceOnInput(obj.col_h, priceForOneMeter, priceResult);
		sumPriceStep0();
		inputs.text(obj.col_h);
	});
	}
}

function sumPriceStep0 () {
	var lastItemPrice = $('.zakaz .steps.active .lastItemPrice'),
		elemetForResult = $('.zakaz .calculatedSum .calculatedResult'),
		sum = 0;
	for (var i = 0; i < lastItemPrice.length; i++) {
		//var data = parseInt($(lastItemPrice[i]).attr('data-valueprice'));
		var data = $(lastItemPrice[i]).attr('data-valueprice')*1;
		sum = (sum*1)+(data*1);
	}
	$(elemetForResult).attr('data-calculatedresult', sum.toFixed(2))
	$(elemetForResult).html($(elemetForResult).attr('data-calculatedresult').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
	$('.itemsPrice').html($(elemetForResult).attr('data-calculatedresult').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
calcWeight();
}

function changeOnePriceOnInput (meters, priceForOneMeter, priceResult) {
	var sum = (meters*1) * (priceForOneMeter*1);
	$(priceResult).attr('data-valueprice', sum.toFixed(2));
	$(priceResult).html($(priceResult).attr('data-valueprice').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
}

function calcWeight(){
	var sum = 0;
	$('.myItem').each(function(){
		var weight = parseFloat($(this).find('.weight').text());
		console.log(weight);
		var quant = parseInt($(this).find('.countItems').val());
		var wq = quant * weight;
		sum += wq;
	});
	$('.calculatedWeight').html(Math.round(sum).toFixed(2));
}