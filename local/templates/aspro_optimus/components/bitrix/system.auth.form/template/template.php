<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>

<?if($arResult["FORM_TYPE"] == "login"):?>

  <div class="subscribe-form" id="subscribe-form">
    <div class="wrap_bg">
      <div class="top_block box-sizing">
        <div class="text">
          <div class="title">Вход для дилеров</div>
  <?
  if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
  {
    ?><div class="more"><? ShowMessage($arResult['ERROR_MESSAGE']) ?></div><?
  }
  ?>
        </div>
      </div>


  <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="/auth/<?=$arResult["AUTH_URL"]?>">
  <?if($arResult["BACKURL"] <> ''):?>
    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
  <?endif?>
  <?foreach ($arResult["POST"] as $key => $value):?>
    <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
  <?endforeach?>
    <input type="hidden" name="AUTH_FORM" value="Y" />
    <input type="hidden" name="TYPE" value="AUTH" />
    <table width="100%">
      <tr>
        <td colspan="2" class="email_wrap">
          <input type="text" name="USER_LOGIN" class="email_input" maxlength="50" value="" size="17" placeholder="<?=GetMessage("AUTH_LOGIN")?>" />
        <script>
          BX.ready(function() {
            var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
            if (loginCookie)
            {
              var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
              var loginInput = form.elements["USER_LOGIN"];
              loginInput.value = loginCookie;
            }
          });
        </script>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="email_wrap">
          <div style="height:5px"></div>
          <input type="password" class="email_input" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off" placeholder="<?=GetMessage("AUTH_PASSWORD")?>" />
  <?if($arResult["SECURE_AUTH"]):?>
          <span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
            <div class="bx-auth-secure-icon"></div>
          </span>
          <noscript>
          <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
            <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
          </span>
          </noscript>
  <script type="text/javascript">
  document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
  </script>
  <?endif?>
        </td>
      </tr>
  <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
      <tr style="display: none;">
        <td valign="top"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" checked="checked" /></td>
        <td width="100%"><label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label></td>
      </tr>
  <?endif?>
  <?if ($arResult["CAPTCHA_CODE"]):?>
      <tr>
        <td colspan="2">
        <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
        <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
        <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
        <input type="text" name="captcha_word" maxlength="50" value="" /></td>
      </tr>
  <?endif?>
      <tr>
        <td colspan="2">
          <!-- input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" / -->
          <button type="submit" class="button vbig_btn wides" name="Login" style="margin: 7px 0 6px;width: 100%;padding-top: 6px;padding-bottom: 7px;"><span><?=GetMessage("AUTH_LOGIN_BUTTON")?></span></button>
        </td>
      </tr>
  <?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
      <tr>
        <td colspan="2"><noindex><a href="/auth/registration<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a></noindex><br /></td>
      </tr>
  <?endif?>

      <tr>
        <td colspan="2"><noindex><a href="/auth/forgot-password<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex></td>
      </tr>
  <?if($arResult["AUTH_SERVICES"]):?>
      <tr>
        <td colspan="2">
          <div class="bx-auth-lbl"><?=GetMessage("socserv_as_user_form")?></div>
  <?
  $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", 
    array(
      "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
      "SUFFIX"=>"form",
    ), 
    $component, 
    array("HIDE_ICONS"=>"Y")
  );
  ?>
        </td>
      </tr>
  <?endif?>
    </table>
  </form>


    </div>
  </div>

<?endif?>