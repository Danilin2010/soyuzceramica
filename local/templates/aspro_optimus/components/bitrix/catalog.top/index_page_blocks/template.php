<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<? $this->setFrameMode( true ); ?>
<?
$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
$arNotify = unserialize($notifyOption);
?>
<?if($arResult["ITEMS"]):?>
  <?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
    $totalCount = COptimus::GetTotalCount($arItem);
    $arQuantityData = COptimus::GetQuantityArray($totalCount);
    $arItem["FRONT_CATALOG"] = "Y";
    
    $strMeasure='';
    if($arItem["OFFERS"]){
      $strMeasure=$arItem["MIN_PRICE"]["CATALOG_MEASURE_NAME"];
    }else{
      if (($arParams["SHOW_MEASURE"]=="Y")&&($arItem["CATALOG_MEASURE"])){
        $arMeasure = CCatalogMeasure::getList(array(), array("ID"=>$arItem["CATALOG_MEASURE"]), false, false, array())->GetNext();
        $strMeasure=$arMeasure["SYMBOL_RUS"];
      }
    }
    ?>
    <div class="item_block col-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
      <div class="catalog_item_wrapp item">
        <div class="catalog_item item_wrap">
          <div class="image_wrapper_block">
            <div class="stickers">
              <?if($arParams['STICKER'] == 'STOCK'):?>
                <div class="sticker_sale_text">Акция</div>
              <?elseif($arParams['STICKER'] == 'NEW'):?>
                <div class="sticker_hit">Новинка</div>
              <?elseif($arParams['STICKER'] == 'HIT'):?>
                <div class="sticker_stock">Хит</div>
              <?elseif($arParams['STICKER'] == 'EXCLUSIVE'):?>
                <div class="sticker_recommend">Эксклюзив</div>
              <?endif;?>
            </div>
            <div class="like_icons">
              <?if($arParams["DISPLAY_WISH_BUTTONS"] != "N" || $arParams["DISPLAY_COMPARE"] == "Y"):?>
                <?if($arItem["CAN_BUY"] && empty($arItem["OFFERS"]) && $arParams["DISPLAY_WISH_BUTTONS"] != "N"):?>
                  <div class="wish_item_button">
                  <span title="<?=GetMessage('CATALOG_WISH')?>" class="wish_item to" data-item="<?=$arItem["ID"]?>"><i></i></span>
                  <span title="<?=GetMessage('CATALOG_WISH_OUT')?>" class="wish_item in added" style="display: none;" data-item="<?=$arItem["ID"]?>"><i></i></span>
                  </div>
                <?endif;?>
                <?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
                  <div class="compare_item_button">
                  <span title="<?=GetMessage('CATALOG_COMPARE')?>" class="compare_item to" data-iblock="<?=$arParams["IBLOCK_ID"]?>" data-item="<?=$arItem["ID"]?>" ><i></i></span>
                  <span title="<?=GetMessage('CATALOG_COMPARE_OUT')?>" class="compare_item in added" style="display: none;" data-iblock="<?=$arParams["IBLOCK_ID"]?>" data-item="<?=$arItem["ID"]?>"><i></i></span>
                  </div>
                <?endif;?>
              <?endif;?>
            </div>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="thumb">
            <?if(!empty($arItem["PREVIEW_PICTURE"])):?>
              <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=($arItem["PREVIEW_PICTURE"]["ALT"]?$arItem["PREVIEW_PICTURE"]["ALT"]:$arItem["NAME"]);?>" title="<?=($arItem["PREVIEW_PICTURE"]["TITLE"]?$arItem["PREVIEW_PICTURE"]["TITLE"]:$arItem["NAME"]);?>" />
            <?elseif(!empty($arItem["DETAIL_PICTURE"])):?>
              <?$img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array("width" => 170, "height" => 170), BX_RESIZE_IMAGE_PROPORTIONAL, true );?>
              <img src="<?=$img["src"]?>" alt="<?=($arItem["PREVIEW_PICTURE"]["ALT"]?$arItem["PREVIEW_PICTURE"]["ALT"]:$arItem["NAME"]);?>" title="<?=($arItem["PREVIEW_PICTURE"]["TITLE"]?$arItem["PREVIEW_PICTURE"]["TITLE"]:$arItem["NAME"]);?>" />
            <?else:?>
              <img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="<?=($arItem["PREVIEW_PICTURE"]["ALT"]?$arItem["PREVIEW_PICTURE"]["ALT"]:$arItem["NAME"]);?>" title="<?=($arItem["PREVIEW_PICTURE"]["TITLE"]?$arItem["PREVIEW_PICTURE"]["TITLE"]:$arItem["NAME"]);?>" />
            <?endif;?>
            </a>
          </div>
          <div class="item_info TYPE_1" >
            <div class="item-title" >
              <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?=$arItem["NAME"]?> <?=$arItem["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["VALUE"]?></span></a>
              <span><?=$arItem["NAME"]?></span>
            </div>
            <?if($arParams["SHOW_RATING"] == "Y"):?>
              <div class="rating">
                <?$APPLICATION->IncludeComponent(
                  "bitrix:iblock.vote",
                  "element_rating_front",
                  Array(
                  "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                  "IBLOCK_ID" => $arItem["IBLOCK_ID"],
                  "ELEMENT_ID" =>$arItem["ID"],
                  "MAX_VOTE" => 5,
                  "VOTE_NAMES" => array(),
                  "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                  "CACHE_TIME" => $arParams["CACHE_TIME"],
                  "DISPLAY_AS_RATING" => 'vote_avg'
                  ),
                  $component, array("HIDE_ICONS" =>"Y")
                );?>
              </div>
            <?endif;?>
            
            <div class="item-stock">
              <span class="icon stock"></span>
              <span class="value">В наличии</span>
            </div>
            
            <div class="cost prices clearfix" >
              <?if($arItem["OFFERS"]):?>
                <?$minPrice = false;
                if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
                $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
                
                if($minPrice["VALUE"]>$minPrice["DISCOUNT_VALUE"] && $arParams["SHOW_OLD_PRICE"]=="Y"){?>
                  <div class="price"><?=GetMessage("CATALOG_FROM");?> <?=$minPrice["PRINT_DISCOUNT_VALUE"];?><?if (($arParams["SHOW_MEASURE"]=="Y") && $strMeasure):?><span class="price_measure">/<?=$strMeasure?></span><?endif;?></div>
                  <div class="price discount">
                  <span><?=$minPrice["PRINT_VALUE"];?></span>
                  </div>
                <?}else{?>
                  <div class="price"><?=GetMessage("CATALOG_FROM");?> <?=$minPrice['PRINT_DISCOUNT_VALUE'];?><?if (($arParams["SHOW_MEASURE"]=="Y") && $strMeasure):?><span class="price_measure">/<?=$strMeasure?></span><?endif;?></div>
                <?}?>
              <?elseif($arItem["PRICES"]):?>
                <?
                $arCountPricesCanAccess = 0;
                foreach($arItem["PRICES"] as $key => $arPrice){
                  if($arPrice["CAN_ACCESS"]){
                    ++$arCountPricesCanAccess;
                  }
                }?>
                <?foreach($arItem["PRICES"] as $key => $arPrice):?>
                  <?if($arPrice["CAN_ACCESS"]):
                    $percent=0;?>
                    <?$price = CPrice::GetByID($arPrice["ID"]);?>
                    <?if($arCountPricesCanAccess > 1):?>
                      <div class="price_name"><?=$price["CATALOG_GROUP_NAME"];?></div>
                    <?endif;?>
                    <?if($arPrice["VALUE"] > $arPrice["DISCOUNT_VALUE"] && $arParams["SHOW_OLD_PRICE"]=="Y"):?>
                      <div class="price"><?=$arPrice["PRINT_DISCOUNT_VALUE"];?><?if (($arParams["SHOW_MEASURE"]=="Y") && $strMeasure):?><span class="price_measure">/<?=$strMeasure?></span><?endif;?></div>
                      <div class="price discount">
                      <span><?=$arPrice["PRINT_VALUE"];?></span>
                      </div>
                    <?else:?>
                      <div class="price"><?=$arPrice["PRINT_VALUE"];?><?if (($arParams["SHOW_MEASURE"]=="Y") && $strMeasure):?><span class="price_measure">/<?=$strMeasure?></span><?endif;?></div>
                    <?endif;?>
                  <?endif;?>
                <?endforeach;?>
              <?endif;?>
            </div>
            <div class="hover_block1 footer_button">
              <div class="counter_wrapp ">
                <?$arAddToBasketData = COptimus::GetAddToBasketArray($arItem, $totalCount, $arParams["DEFAULT_COUNT"], $arParams["BASKET_URL"], true);?>
                <div class="buttons_block clearfix">
                  <?=$arAddToBasketData["HTML"]?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?endforeach;?>
<?endif;?>