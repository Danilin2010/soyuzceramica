<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="module-form-block-wr lk-page border_block">

	<? ShowError($arResult["strProfileError"]); ?>
	<? if ($arResult['DATA_SAVED'] == 'Y') { 
    ?><? ShowNote(GetMessage('PROFILE_DATA_SAVED')); ?><br /><?
	}
	$disabled = '';
	if ($arResult["arUser"]["UF_SEND_DATA"] == true) {
		$disabled = ' disabled="disabled"';
	}
	?>
    <script>
		$(document).ready(function()
		{
			$(".form-block-wr form").validate({rules: {EMAIL: {email: true}}});
		})
    </script>  
    <div class="form-block-wr">
        <form method="post" name="form1" id="main-form" class="main" action="<?= $arResult["FORM_TARGET"] ?>?" enctype="multipart/form-data">
			<?= $arResult["BX_SESSION_CHECK"] ?>
            <input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
            <input type="hidden" name="lang" value="<?= LANG ?>" />
            <input type="hidden" name="ID" value=<?= $arResult["ID"] ?> />
            <div class="form-control">

                <input type="hidden" name="NEW_USER" value="<? if ($arResult["arUser"]["UF_SEND_DATA"] == false): ?>new<? endif; ?>" />

                <div class="wrap_md">
                    <div class="iblock label_block">
                        <label><?= GetMessage("PERSONAL_NAME") ?><span class="star">*</span></label>
						<?
						$arName = array();
						if (!$arResult["strProfileError"]) {
							if ($arResult["arUser"]["LAST_NAME"]) {
								$arName[] = $arResult["arUser"]["LAST_NAME"];
							}
							if ($arResult["arUser"]["NAME"]) {
								$arName[] = $arResult["arUser"]["NAME"];
							}
							if ($arResult["arUser"]["SECOND_NAME"]) {
								$arName[] = $arResult["arUser"]["SECOND_NAME"];
							}
						}
						else
							$arName[] = htmlspecialcharsbx($_POST["NAME"]);
						?>
                        <input required type="text" name="NAME" maxlength="50" value="<?= implode(' ', $arName); ?>" />
                    </div>
                    <div class="iblock text_block">
						<?= GetMessage("PERSONAL_NAME_DESCRIPTION") ?>
                    </div>
                </div>
            </div>
            <div class="form-control">
                <div class="wrap_md">
                    <div class="iblock label_block">
                        <label><?= GetMessage("PERSONAL_EMAIL") ?><span class="star">*</span></label>
                        <input required type="text" name="EMAIL" maxlength="50" placeholder="name@company.ru" value="<? echo $arResult["arUser"]["EMAIL"] ?>" />
                    </div>
                    <div class="iblock text_block">
						<?= GetMessage("PERSONAL_EMAIL_DESCRIPTION") ?>
                    </div>
                </div>
            </div>
            <div class="form-control">
                <div class="wrap_md">
                    <div class="iblock label_block">
                        <label><?= GetMessage("PERSONAL_PHONE") ?><span class="star">*</span></label>
                        <input required type="text" name="PERSONAL_PHONE" class="phone" maxlength="255" value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>" />
                    </div>
                    <div class="iblock text_block">
						<?= GetMessage("PERSONAL_PHONE_DESCRIPTION") ?>
                    </div>
                </div>
            </div>
			<? if (intval($arResult["arUser"]["UF_DEP_CODE"]) > 0): ?>                        
				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("PERSONAL_MOBILE") ?></label>
							<input type="text" name="PERSONAL_MOBILE" maxlength="50" value="<? echo $arResult["arUser"]["PERSONAL_MOBILE"] ?>" />
						</div>
						<div class="iblock text_block">
							<?= GetMessage("PERSONAL_MOBILE_DESCRIPTION") ?>
						</div>
					</div>
				</div>


				<div class="form-control">
					<div class="wrap_md web-sites">
						<? if (!empty($arResult["arUser"]["UF_WEB_SITES"])): ?>
							<? foreach ($arResult["arUser"]["UF_WEB_SITES"] as $key => $web_sites): ?>
								<div class="iblock label_block">
									<? if ($key === 0): ?><label><?= GetMessage("UF_WEB_SITES") ?></label><? endif; ?>
									<input type="text" name="UF_WEB_SITES[]" maxlength="50" value="<? echo $web_sites ?>" />
								</div>
							<? endforeach; ?>
						<? else: ?>
							<div class="iblock label_block">
								<label><?= GetMessage("UF_WEB_SITES") ?></label>
								<input type="text" name="UF_WEB_SITES[]" maxlength="50" value="" />
							</div>
						<? endif; ?>
						<div class="iblock text_block">
							<?= GetMessage("PERSONAL_WEB_SITES_DESCRIPTION") ?>
						</div>
					</div>
					<div class="add-new-field-block">
						<a href="" class="add-new-field" id="add-sites"><?= GetMessage("ADD_LABEL") ?></a>
					</div>
				</div>                        

				<div class="form-control">
					<div class="wrap_md point-of-sale">
						<? if (!empty($arResult["arUser"]["UF_POINT_OF_SALE"])): ?>
							<? foreach ($arResult["arUser"]["UF_POINT_OF_SALE"] as $key => $point_of_sale): ?>
								<div class="iblock label_block">
									<? if ($key === 0): ?><label><?= GetMessage("UF_POINT_OF_SALE") ?></label><? endif; ?>
									<input type="text" name="UF_POINT_OF_SALE[]" maxlength="50" value="<? echo $point_of_sale ?>" />
								</div>
							<? endforeach; ?>
						<? else: ?>
							<div class="iblock label_block">
								<label><?= GetMessage("UF_POINT_OF_SALE") ?></label>
								<input type="text" name="UF_POINT_OF_SALE[]" maxlength="50" value="" />
							</div>
						<? endif; ?>
						<div class="iblock text_block">
							<?= GetMessage("PERSONAL_POINT_OF_SALE_DESCRIPTION") ?>
						</div>
					</div>
					<div class="add-new-field-block">
						<a href="" class="add-new-field" id="add-point-of-sale"><?= GetMessage("ADD_LABEL") ?></a>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_DEP_CODE") ?><span class="star">*</span></label>
							<input required type="text" maxlength="50" value="<? echo $arResult["arUser"]["UF_DEP_CODE"] ?>" disabled="disabled" />
						</div>
					</div>
				</div>
				<br />
				<h4><?= GetMessage("COMPANY_INFO") ?></h4>


				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("WORK_COMPANY") ?><span class="star">*</span></label>
							<input required type="text" name="WORK_COMPANY" maxlength="50" value="<? echo $arResult["arUser"]["WORK_COMPANY"] ?>"<? echo $disabled; ?>/>
						</div>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_COMPANY_INN") ?><span class="star">*</span></label>
							<input required type="text" name="UF_COMPANY_INN" maxlength="50" value="<? echo $arResult["arUser"]["UF_COMPANY_INN"] ?>"<? echo $disabled; ?> />
						</div>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_COMPANY_KPP") ?></label>
							<input type="text" name="UF_COMPANY_KPP" maxlength="50" value="<? echo $arResult["arUser"]["UF_COMPANY_KPP"] ?>" />
						</div>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_COMPANY_OGRN") ?><span class="star">*</span></label>
							<input required type="text" name="UF_COMPANY_OGRN" maxlength="50" value="<? echo $arResult["arUser"]["UF_COMPANY_OGRN"] ?>"<? echo $disabled; ?> />
						</div>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_BANK_DETAILS") ?></label>
							<textarea name="UF_BANK_DETAILS"><? echo $arResult["arUser"]["UF_BANK_DETAILS"] ?></textarea>
						</div>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_COMPANY_ADDRESS") ?><span class="star">*</span></label>
							<textarea required name="UF_COMPANY_ADDRESS"<? echo $disabled; ?> ><? echo $arResult["arUser"]["UF_COMPANY_ADDRESS"] ?></textarea>
						</div>
					</div>
				</div>

				<div class="form-control">
					<div class="wrap_md">
						<div class="iblock label_block">
							<label><?= GetMessage("UF_POST_ADDRESS") ?></label>
							<textarea name="UF_POST_ADDRESS"><? echo $arResult["arUser"]["UF_POST_ADDRESS"] ?></textarea>
						</div>
					</div>
				</div>
			<? endif; ?>
            <input type="hidden" name="save" value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE_TITLE") : GetMessage("MAIN_ADD_TITLE")) ?>" />
            <div class="but-r">
                <button class="button" type="button" name="save-btn" value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE_TITLE") : GetMessage("MAIN_ADD_TITLE")) ?>"><span><?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE_TITLE") : GetMessage("MAIN_ADD_TITLE")) ?></span></button>
				<? /* <div class="prompt">
				  <span class="star">*</span> &nbsp;&mdash;&nbsp; <?=GetMessage("REQUIRED_FIELDS")?>
				  </div>
				  <div class="clearboth"></div>
				  <?/*<a class="cancel"><?=GetMessage('MAIN_RESET');?></a> */ ?>
            </div>

        </form>
		<?
		if ($arResult["SOCSERV_ENABLED"]) {
			$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", "main", array("SUFFIX" => "form", "SHOW_PROFILES" => "Y", "ALLOW_DELETE" => "Y"), false);
		}
		?>
    </div>
</div>
<font class="result notetext"></font>
<script>
	$('#add-sites').on('click', function() {
		$('.web-sites').append('<div class="iblock label_block"> <input type="text" name="UF_WEB_SITES[]" maxlength="50" value="<? echo $arResult["arUser"]["PERSONAL_MOBILE"] ?>" /></div>');
		return false;
	});
	$('#add-point-of-sale').on('click', function() {
		$('.point-of-sale').append('<div class="iblock label_block"> <input type="text" name="UF_POINT_OF_SALE[]" maxlength="50" value="<? echo $arResult["arUser"]["PERSONAL_MOBILE"] ?>" /></div>');
		return false;
	});
<? if (intval($arResult["arUser"]["UF_DEP_CODE"]) > 0): ?>
		$(function() {


			$(document).on("click", "button[name=save-btn]", function() {
				var flag = 1;
				$('input:required').each(function() {
					$(this).css({"background": "#f7f7f7"});
					if ($(this).val() == "") {
						$(this).css({"background": "#ffcccc"});
						flag = 0;
					}
				});
				$('textarea:required').each(function() {
					$(this).css({"background": "#f7f7f7"});
					if ($(this).val() == "") {
						$(this).css({"background": "#ffcccc"});
						flag = 0;
					}
				});
				if (flag == 1) {
					$.post('/include/ajax/diler_edit_send.php',
							{
								NAME: $("input[name=NAME]").val(),
								PERSONAL_PHONE: $("input[name=PERSONAL_PHONE]").val(),
								EMAIL: $("input[name=EMAIL]").val(),
								PERSONAL_MOBILE: $("input[name=PERSONAL_MOBILE]").val(),
								UF_WEB_SITES: $("input[name=UF_WEB_SITES]").val(),
								UF_POINT_OF_SALE: $("input[name=UF_POINT_OF_SALE]").val(),
								UF_DEP_CODE: $("input[name=UF_DEP_CODE]").val(),
								WORK_COMPANY: $("input[name=WORK_COMPANY]").val(),
								UF_COMPANY_INN: $("input[name=UF_COMPANY_INN]").val(),
								UF_COMPANY_KPP: $("input[name=UF_COMPANY_KPP]").val(),
								UF_COMPANY_OGRN: $("input[name=UF_COMPANY_OGRN]").val(),
								UF_COMPANY_ADDRESS: $("textarea[name=UF_COMPANY_ADDRESS]").val(),
								UF_BANK_DETAILS: $("textarea[name=UF_BANK_DETAILS]").val(),
								UF_POST_ADDRESS: $("textarea[name=UF_POST_ADDRESS]").val(),
								NEW_USER: $("input[name=NEW_USER]").val(),
								USER_ID: <? echo $arResult["arUser"]["ID"] ?>
							},
					function(data) {
						if (data == "ok") {
							$("#main-form").submit();
						} else {
							$(".result").html(data);
						}
					});
				} else {
					//                   alert("Заполнены не все поля");
				}
			});

		});
<? endif; ?>
</script>