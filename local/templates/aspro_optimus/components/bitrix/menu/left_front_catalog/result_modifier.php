<?
$catalog_id=\Bitrix\Main\Config\Option::get("aspro.optimus", "CATALOG_IBLOCK_ID", COptimusCache::$arIBlocks[SITE_ID]['aspro_optimus_catalog']['aspro_optimus_catalog'][0]);
$arSections = COptimusCache::CIBlockSection_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => COptimusCache::GetIBlockCacheTag($catalog_id), 'GROUP' => array('ID'))), array('IBLOCK_ID' => $catalog_id, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y', '<DEPTH_LEVEL' =>\Bitrix\Main\Config\Option::get("aspro.optimus", "MAX_DEPTH_MENU", 2)), false, array("ID", "NAME", "PICTURE", "LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL", "SECTION_PAGE_URL", "IBLOCK_SECTION_ID"));
if($arSections){
	$arResult = array();
	$cur_page = $GLOBALS['APPLICATION']->GetCurPage(true);
	$cur_page_no_index = $GLOBALS['APPLICATION']->GetCurPage(false);

	foreach($arSections as $ID => $arSection){
		$arSections[$ID]['SELECTED'] = CMenu::IsItemSelected($arSection['SECTION_PAGE_URL'], $cur_page, $cur_page_no_index);
		if($arSection['PICTURE']){
			$img=CFile::ResizeImageGet($arSection['PICTURE'], Array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arSections[$ID]['IMAGES']=$img;
		}
		if($arSection['IBLOCK_SECTION_ID']){
			if(!isset($arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'])){
				$arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'] = array();
			}
			$arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'][] = &$arSections[$arSection['ID']];
		}

		if($arSection['DEPTH_LEVEL'] == 1){
			$arResult[] = &$arSections[$arSection['ID']];
		}
	}
}


// $arMenuAddedSEO - Добавляем принудительно в меню каталога seo-разделы 
include_once $_SERVER['DOCUMENT_ROOT'] .'/catalog/.menu.added.left.php';

foreach($arMenuAddedSEO as $index => $arSection)
{
  if(stristr($APPLICATION->GetCurDir(), $arSection['SECTION_PAGE_URL']))
    $arMenuAddedSEO[$index]['SELECTED'] = 'Y';
  
  if(count($arSection['CHILD']) > 0)
  {
    foreach($arSection['CHILD'] as $key => $arSubSection)
    {
      if(stristr($APPLICATION->GetCurDir(), $arSubSection['SECTION_PAGE_URL']))
        $arMenuAddedSEO[$index]['CHILD'][$key]['SELECTED'] = 'Y';
    }
  }
}

$arResult = array_merge($arResult, $arMenuAddedSEO);

//array_splice($arResult, 0, 0, $arMenuAddedSEO); 


//if($_GET['dbg'] == 'Y')
//  dbg($arMenuAddedSEO);