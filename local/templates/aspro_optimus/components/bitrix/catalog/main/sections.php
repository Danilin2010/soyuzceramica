<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>

<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.top.php', Array(), Array('MODE' => 'html'));?>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"sections_list",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		"SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
		"SECTIONS_LIST_PREVIEW_PROPERTY" => $arParams["SECTIONS_LIST_PREVIEW_PROPERTY"],
		"SECTIONS_LIST_PREVIEW_DESCRIPTION" => $arParams["SECTIONS_LIST_PREVIEW_DESCRIPTION"],
		"SHOW_SECTION_LIST_PICTURES" => $arParams["SHOW_SECTION_LIST_PICTURES"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],		
	),
	$component
);?>


<br><br>
<h2>По назначению</h2>
<div class="catalog_section_list rows_block items">	
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/vanna/" class="thumb"><img src="/catalog/.images/keramma_03.jpg" alt="Для ванной" title="Для ванной"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name"><a href="/catalog/vanna/"><span>Для ванной</span></a></li>
              <li class="sect"><a href="/catalog/vanna/mozaika/" class="dark_link">Мозаика</a></li>
              <li class="sect"><a href="/catalog/vanna/kabanchik/" class="dark_link">Кабанчик</a></li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/pol/" class="thumb"><img src="/catalog/.images/keramma_05.jpg" alt="Для пола" title="Для пола"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name">
                <a href="/catalog/pol/"><span>Для пола</span></a> 
              </li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/ylichnaya/" class="thumb"><img src="/catalog/.images/keramma_07.jpg" alt="Уличная плитка" title="Уличная плитка"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name">
                <a href="/catalog/ylichnaya/"><span>Уличная плитка</span></a> 
              </li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/kuhnya/" class="thumb"><img src="/catalog/.images/keramma_09.jpg" alt="Для кухни" title="Для кухни"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name"><a href="/catalog/kuhnya/"><span>Для кухни</span></a></li>
              <li class="sect"><a href="/catalog/kuhnya/pol/" class="dark_link">Для кухни на пол</a></li>
              <li class="sect"><a href="/catalog/kuhnya/mozaika/" class="dark_link">Мозаика</a></li>
              <li class="sect"><a href="/catalog/kuhnya/kaabanchik/" class="dark_link">Кабанчик</a></li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/stupenei/" class="thumb"><img src="/catalog/.images/keramma_11.jpg" alt="Для ступеней лестницы" title="Для ступеней лестницы"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name"><a href="/catalog/stupenei/"><span>Для ступеней лестницы</span></a></li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<br><br>
<h2>По типу</h2>

<div class="catalog_section_list rows_block items">	
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/mozaika/" class="thumb"><img src="/catalog/.images/keramma_18.jpg" alt="Мозаика" title="Мозаика"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name"><a href="/catalog/mozaika/"><span>Мозаика</span></a></li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/kabanchik/" class="thumb"><img src="/catalog/.images/keramma_19.jpg" alt="Кабанчик" title="Кабанчик"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name">
                <a href="/catalog/kabanchik/"><span>Кабанчик</span></a> 
              </li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/monokolor/" class="thumb"><img src="/catalog/.images/keramma_20.jpg" alt="Моноколор" title="Моноколор"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name"><a href="/catalog/monokolor/"><span>Моноколор</span></a></li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="item_block col-2">
    <div class="section_item item">
      <table class="section_item_inner">	
        <tbody><tr>
          <td class="image">
            <a href="/catalog/melkoformat/" class="thumb"><img src="/catalog/.images/keramma_21.jpg" alt="Мелкоформатная" title="Мелкоформатная"></a>
          </td>
          <td class="section_info">
            <ul>
              <li class="name">
                <a href="/catalog/melkoformat/"><span>Мелкоформатная</span></a> 
              </li>
            </ul>
          </td></tr><tr><td class="desc" colspan="2"><span class="desc_wrapp"></span></td></tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<br><br>

<div class="seo-description-block">
  <?$APPLICATION->IncludeFile($APPLICATION->GetCurDir() .'text.bottom.php', Array(), Array('MODE' => 'html'));?>
</div>
