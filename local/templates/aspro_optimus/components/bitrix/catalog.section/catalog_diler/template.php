<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->AddHeadScript('script.js');

//$this->setFrameMode(true);
$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name);

$arUser = CUser::GetByID(intval($USER->GetID()))->Fetch();

$arMeasure = getMeasuresArray();

//CCatalogMeasureRatio
//dbg($arUser);
//dbg($_REQUEST);
//dbg($arResult['NAV_RESULT']->nSelectedCount); // ['nSelectedCount'] // 4135
//dbg($arResult['ITEMS']);

// Если не нажата клавиша Показать ещё..
if($_GET['MODE'] != 'page')
{
  ?>
  <input type="hidden" id="param-items" data-ajax-id="<?=$bxajaxid?>" data-show-more="<?=$arResult["NAV_RESULT"]->NavNum?>" data-max-page="<?=$arResult["NAV_RESULT"]->nEndPage?>" />
  <input type="hidden" id="param-items-count" value="<?=$arResult['NAV_RESULT']->nSelectedCount?>" />

  <div class="responcive-table">
  <?
}
?>
    <table>
      <thead>
        <tr>
          <th>Артикул</th>
          <th>Картинка</th>
          <th class="name">Наименование</th>
          <?if(in_array(13, $arUser["UF_DISPLAY_COUNT"])):?>
            <th>Наличие <br>на складе</th>
          <?endif?>
          <?if(in_array(15, $arUser["UF_DISPLAY_COUNT"])):?>
            <th>Наличие <br>на заводе</th>
          <?endif?>
          <?if(in_array(16, $arUser["UF_DISPLAY_COUNT"])):?>
            <th>План производства</th>
          <?endif?>
          <?if(in_array(14, $arUser["UF_DISPLAY_COUNT"])):?>
            <th>В пути</th>
          <?endif?>
          <th>Количество</th>
          <th>Цена</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

      <?foreach($arResult['ITEMS'] as $arItem):?>
        <?
        //dbgz($arItem['CATALOG_MEASURE']);
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

        $arPrice = Array();
        foreach($arItem['PRICES'] as $arPrice)
          $arPrice;
        
        $calcPack = false;
        if($arItem['CATALOG_MEASURE'] == 6 && !empty($arItem['PROPERTIES']['col_kor']['VALUE']) && !empty($arItem['PROPERTIES']['m_kor']['VALUE']))
          $calcPack = true;
        
        $arImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'] ? $arItem['PREVIEW_PICTURE']['ID'] : $arItem['DETAIL_PICTURE']['ID'], Array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL);
        
        $arItem["strMainID"] = $this->GetEditAreaId($arItem['ID']);
        $arItemIDs=COptimus::GetItemsIDs($arItem);

        $totalCount = 3; //COptimus::GetTotalCount($arItem);
        $arQuantityData = COptimus::GetQuantityArray($totalCount, $arItemIDs["ALL_ITEM_IDS"]);

        $strMeasure = '';
        if(!$arItem["OFFERS"] || $arParams['TYPE_SKU'] !== 'TYPE_1'){
          if($arParams["SHOW_MEASURE"] == "Y" && $arItem["CATALOG_MEASURE"]){
            $arMeasure = CCatalogMeasure::getList(array(), array("ID" => $arItem["CATALOG_MEASURE"]), false, false, array())->GetNext();
            $strMeasure = $arMeasure["SYMBOL_RUS"];
          }
          $arAddToBasketData = COptimus::GetAddToBasketArray($arItem, $totalCount, $arParams["DEFAULT_COUNT"], $arParams["BASKET_URL"], false, $arItemIDs["ALL_ITEM_IDS"], 'small', $arParams);          
        }
        elseif($arItem["OFFERS"]){
          $strMeasure = $arItem["MIN_PRICE"]["CATALOG_MEASURE_NAME"];
        }
        $arAddToBasketData['HTML'] = str_replace(Array($arAddToBasketData['TEXT'][0], $arAddToBasketData['TEXT'][1]), '', $arAddToBasketData['HTML']);
        
        $elementName = ((isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME']);

        ?>
        <tr id="<?=$arItem["strMainID"]?>" data-id="<?=$arItem['ID']?>"<?if($calcPack):?> class="counter-units-wrapp" data-price="<?=$arPrice['VALUE']?>" data-count-in-pack="<?=$arItem['PROPERTIES']['col_kor']['VALUE']?>" data-meter-in-pack="<?=$arItem['PROPERTIES']['m_kor']['VALUE']?>"<?endif?>>
          <td class="col-article">
            <span><?=$arItem['PROPERTIES']['CML2_ARTICLE']['VALUE']?></span>
            <?if(!empty($arItem['ANALOGS'])):?>
              <br><a class="analog">есть аналог</a>
            <?endif?>
          </td>
          <td class="col-image">
            <?if(!empty($arItem['DETAIL_PICTURE'])):?>
              <a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="fancy" title="<?=$elementName?>" rel="group-fancy"><div class="wrap-img"><img src="<?=$arImage['src']?>" alt=""></div></a>
            <?elseif(!empty($arImage['src'])):?>
              <img src="<?=$arImage['src']?>" alt="">
            <?endif?>
          </td>
          <td class="col-name"><?=$elementName?></td>
          
          <?if(in_array(13, $arUser["UF_DISPLAY_COUNT"])):?>
            <td class="col-count">
              <?if($arItem['COUNTS']['MAIN']['SKLAD']):?>
                <div><?=$arItem['COUNTS']['MAIN']['SKLAD']?> <?=$arMeasure[$arItem['CATALOG_MEASURE']]['SYMBOL_RUS']?></div>
                <?if($calcPack):?>
                  <? $cntValuePack = round(str_replace(',', '.', $arItem['COUNTS']['MAIN']['SKLAD'] / $arItem['PROPERTIES']['m_kor']['VALUE'])); ?>
                  <div><?=$cntValuePack?> упак</div>
                  <div><?=round($cntValuePack * $arItem['PROPERTIES']['col_kor']['VALUE'], 4)?> шт</div>
                <?endif?>
              <?else:?>
                <div>—</div>
              <?endif?>
            </td>
          <?endif?>
            
          <?if(in_array(15, $arUser["UF_DISPLAY_COUNT"])):?>
            <td class="col-count">
              <?if($arItem['COUNTS']['MAIN']['FACTORY']):?>
                <div><?=$arItem['COUNTS']['MAIN']['FACTORY']?> <?=$arMeasure[$arItem['CATALOG_MEASURE']]['SYMBOL_RUS']?></div>
                <?if($calcPack):?>
                  <? $cntValuePack = ceil(str_replace(',', '.', $arItem['COUNTS']['MAIN']['FACTORY'] / $arItem['PROPERTIES']['m_kor']['VALUE'])); ?>
                  <div><?=$cntValuePack?> упак</div>
                  <div><?=round($cntValuePack * $arItem['PROPERTIES']['col_kor']['VALUE'], 4)?> шт</div>
                <?endif?>
              <?else:?>
                <div>—</div>
              <?endif?>
            </td>
          <?endif?>
            
          <?if(in_array(16, $arUser["UF_DISPLAY_COUNT"])):?>
            <td class="col-count">
              <?if($arItem['COUNTS']['MAIN']['PLAN']):?>
                <div><?=$arItem['COUNTS']['MAIN']['PLAN']?> <?=$arMeasure[$arItem['CATALOG_MEASURE']]['SYMBOL_RUS']?></div>
                <?if($calcPack):?>
                  <? $cntValuePack = ceil(str_replace(',', '.', $arItem['COUNTS']['MAIN']['PLAN'] / $arItem['PROPERTIES']['m_kor']['VALUE'])); ?>
                  <div><?=$cntValuePack?> упак</div>
                  <div><?=round($cntValuePack * $arItem['PROPERTIES']['col_kor']['VALUE'], 4)?> шт</div>
                <?endif?>
              <?else:?>
                <div>—</div>
              <?endif?>
            </td>
          <?endif?>
            
          <?if(in_array(14, $arUser["UF_DISPLAY_COUNT"])):?>
            <td class="col-count">
              <?if($arItem['COUNTS']['MAIN']['TRANSIT']):?>
                <div><?=$arItem['COUNTS']['MAIN']['TRANSIT']?> <?=$arMeasure[$arItem['CATALOG_MEASURE']]['SYMBOL_RUS']?></div>
                <?if($calcPack):?>
                  <? $cntValuePack = ceil(str_replace(',', '.', $arItem['COUNTS']['MAIN']['TRANSIT'] / $arItem['PROPERTIES']['m_kor']['VALUE'])); ?>
                  <div><?=$cntValuePack?> упак</div>
                  <div><?=round($cntValuePack * $arItem['PROPERTIES']['col_kor']['VALUE'], 4)?> шт</div>
                <?endif?>
              <?else:?>
                <div>—</div>
              <?endif?>
            </td>
          <?endif?>
            
          <?if($calcPack):?>
            <td class="col-miasure">
              <div class="counter_block unit-meter" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arItem["ID"];?>">
                <? $tmpValueMeter = round(1 * $arItem['PROPERTIES']['m_kor']['VALUE'] * 10000, 4) / 10000; ?>
                <input title="м2" type="text" class="text js-calc-pack" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<?=$tmpValueMeter?>" autocomplete="off" />
              </div>
              <div class="counter_block unit-pack" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arItem["ID"];?>">
                <? $tmpValuePack = 1; //round(str_replace(',', '.', 1 / $arItem['PROPERTIES']['m_kor']['VALUE']), 4); ?>
                <input title="упак" type="text" class="text js-calc-pack" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>_pack" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>_pack" value="<?=$tmpValuePack?>" autocomplete="off" />
              </div>
              <div class="counter_block unit-thing" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arItem["ID"];?>">
                <input title="шт" type="text" class="text js-calc-pack" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>_thing" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>_thing" value="<?=round($tmpValuePack * $arItem['PROPERTIES']['col_kor']['VALUE'], 4)?>" autocomplete="off" />
              </div>
            </td>
          <?else:?>
            <td class="col-miasure">
              <div class="counter_block" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arItem["ID"];?>" data-price="<?=$arPrice['VALUE']?>">
                <input type="text" class="text js-unit-default" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<?=$arAddToBasketData["MIN_QUANTITY_BUY"]?>" />
              </div>
            </td>
          <?endif?>
            
          <td class="col-price">
            <div class="js-change-price-from-units">
              <div class="price_value">
                <?if($calcPack && !empty($arPrice['VALUE'])):?>
                  <?=priceFormat($arPrice['VALUE'] * $tmpValueMeter)?>
                <?else:?>
                  <?=priceFormat($arPrice['VALUE'])?>
                <?endif?>
              </div>
            </div>
          </td>
          <td class="col-buy">
            <div id="<?=$arItemIDs["ALL_ITEM_IDS"]['BASKET_ACTIONS']; ?>" class="button_block add-to-cart <?=(($arAddToBasketData["ACTION"] == "ORDER"/*&& !$arItem["CAN_BUY"]*/)  || !$arItem["CAN_BUY"] || !$arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] || $arAddToBasketData["ACTION"] == "SUBSCRIBE" ? "wide" : "");?>">
              <!--noindex-->
              <?if($calcPack):?>
                <?$arAddToBasketData["HTML"] = str_replace('data-quantity="1"', 'data-quantity="'. $tmpValueMeter .'"', $arAddToBasketData["HTML"]);?>
              <?endif?>
              <?=$arAddToBasketData["HTML"]?>
              <!--/noindex-->
            </div>
          </td>
        </tr>
        
        
        
        <?if(!empty($arItem['ANALOGS'])):?>
        
          <?foreach($arItem['ANALOGS'] as $arAnalog):?>
            <?
            $calcPack = false;
            if($arAnalog['MEASURE_ID'] == 6 && !empty($arAnalog['col_kor']) && !empty($arAnalog['m_kor']))
              $calcPack = true;
            
            $arImage = CFile::ResizeImageGet($arAnalog['PREVIEW_PICTURE']['ID'] ? $arAnalog['PREVIEW_PICTURE']['ID'] : $arAnalog['DETAIL_PICTURE']['ID'], Array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL);
            
            $arAnalog["strMainID"] = $this->GetEditAreaId($arAnalog['ID']);
            $arItemIDs = COptimus::GetItemsIDs($arAnalog);
            
            $totalCount = 3; //COptimus::GetTotalCount($arItem);
            $arQuantityData = COptimus::GetQuantityArray($totalCount, $arItemIDs["ALL_ITEM_IDS"]);
            ?>
            <tr id="<?=$arAnalog["strMainID"]?>" data-id="<?=$arAnalog['ID']?>" data-parent="<?=$arItem['ID']?>" class="is-analog counter-units-wrapp"<?if($calcPack):?> data-price="<?=$arAnalog['PRICE']['PRICE']?>" data-count-in-pack="<?=$arAnalog['col_kor']?>" data-meter-in-pack="<?=$arAnalog['m_kor']?>"<?endif?>>
              <td class="col-article">
                <span><?=$arAnalog['ARTICLE']?></span>
              </td>
              <td class="col-image">
                <?if(!empty($arAnalog['DETAIL_PICTURE'])):?>
                  <a href="<?=$arAnalog['DETAIL_PICTURE']['SRC']?>" class="fancy" title="<?=$arAnalog['NAME']?>" rel="group-fancy"><div class="wrap-img"><img src="<?=$arImage['src']?>" alt=""></div></a>
                <?elseif(!empty($arImage['src'])):?>
                  <img src="<?=$arImage['src']?>" alt="">
                <?endif?>
              </td>
              <td class="col-name"><?=$arAnalog['NAME']?></td>

              <?if(in_array(13, $arUser["UF_DISPLAY_COUNT"])):?>
                <td class="col-count">
                  <?if($arAnalog['COUNTS']['SKLAD']):?>
                    <div><?=$arAnalog['COUNTS']['SKLAD']?> <?=$arMeasure[$arAnalog['MEASURE_ID']]['SYMBOL_RUS']?></div>
                    <?if($calcPack):?>
                      <? $cntValuePack = round(str_replace(',', '.', $arAnalog['COUNTS']['SKLAD'] / $arAnalog['m_kor'])); ?>
                      <div><?=$cntValuePack?> упак</div>
                      <div><?=round($cntValuePack * $arAnalog['col_kor'], 4)?> шт</div>
                    <?endif?>
                  <?else:?>
                    <div>—</div>
                  <?endif?>
                </td>
              <?endif?>

              <?if(in_array(15, $arUser["UF_DISPLAY_COUNT"])):?>
                <td class="col-count">
                  <?if($arAnalog['COUNTS']['FACTORY']):?>
                    <div><?=$arAnalog['COUNTS']['FACTORY']?> <?=$arMeasure[$arAnalog['MEASURE_ID']]['SYMBOL_RUS']?></div>
                    <?if($calcPack):?>
                      <? $cntValuePack = ceil(str_replace(',', '.', $arAnalog['COUNTS']['FACTORY'] / $arAnalog['m_kor'])); ?>
                      <div><?=$cntValuePack?> упак</div>
                      <div><?=round($cntValuePack * $arAnalog['col_kor'], 4)?> шт</div>
                    <?endif?>
                  <?else:?>
                    <div>—</div>
                  <?endif?>
                </td>
              <?endif?>

              <?if(in_array(16, $arUser["UF_DISPLAY_COUNT"])):?>
                <td class="col-count">
                  <?if($arAnalog['COUNTS']['PLAN']):?>
                    <div><?=$arAnalog['COUNTS']['PLAN']?> <?=$arMeasure[$arAnalog['MEASURE_ID']]['SYMBOL_RUS']?></div>
                    <?if($calcPack):?>
                      <? $cntValuePack = ceil(str_replace(',', '.', $arAnalog['COUNTS']['PLAN'] / $arAnalog['m_kor'])); ?>
                      <div><?=$cntValuePack?> упак</div>
                      <div><?=round($cntValuePack * $arAnalog['col_kor'], 4)?> шт</div>
                    <?endif?>
                  <?else:?>
                    <div>—</div>
                  <?endif?>
                </td>
              <?endif?>

              <?if(in_array(14, $arUser["UF_DISPLAY_COUNT"])):?>
                <td class="col-count">
                  <?if($arAnalog['COUNTS']['TRANSIT']):?>
                    <div><?=$arAnalog['COUNTS']['TRANSIT']?> <?=$arMeasure[$arAnalog['MEASURE_ID']]['SYMBOL_RUS']?></div>
                    <?if($calcPack):?>
                      <? $cntValuePack = ceil(str_replace(',', '.', $arAnalog['COUNTS']['TRANSIT'] / $arAnalog['m_kor'])); ?>
                      <div><?=$cntValuePack?> упак</div>
                      <div><?=round($cntValuePack * $arAnalog['col_kor'], 4)?> шт</div>
                    <?endif?>
                  <?else:?>
                    <div>—</div>
                  <?endif?>
                </td>
              <?endif?>

              <?if($calcPack):?>
                <td class="col-miasure">
                  <div class="counter_block unit-meter" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arAnalog["ID"];?>">
                    <? $tmpValueMeter = round(1 * $arAnalog['m_kor'] * 10000, 4) / 10000; ?>
                    <input title="м2" type="text" class="text js-calc-pack" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<?=$tmpValueMeter?>" autocomplete="off" />
                  </div>
                  <div class="counter_block unit-pack" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arAnalog["ID"];?>">
                    <? $tmpValuePack = 1; //round(str_replace(',', '.', 1 / $arAnalog['m_kor']), 4); ?>
                    <input title="упак" type="text" class="text js-calc-pack" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>_pack" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>_pack" value="<?=$tmpValuePack?>" autocomplete="off" />
                  </div>
                  <div class="counter_block unit-thing" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arAnalog["ID"];?>">
                    <input title="шт" type="text" class="text js-calc-pack" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>_thing" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>_thing" value="<?=round($tmpValuePack * $arAnalog['col_kor'], 4)?>" autocomplete="off" />
                  </div>
                </td>
              <?else:?>
                <td class="col-miasure">
                  <div class="counter_block" data-offers="<?=($arItem["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arAnalog["ID"];?>" data-price="<?=$arAnalog['PRICE']['PRICE']?>">
                    <input type="text" class="text js-unit-default" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<?=$arAddToBasketData["MIN_QUANTITY_BUY"]?>" />
                  </div>
                </td>
              <?endif?>

              <td class="col-price">
                <div class="js-change-price-from-units">
                  <div class="price_value">
                    <?if($calcPack && !empty($arAnalog['PRICE']['PRICE'])):?>
                      <?=priceFormat($arAnalog['PRICE']['PRICE'] * $tmpValueMeter)?>
                    <?else:?>
                      <?=priceFormat($arAnalog['PRICE']['PRICE'])?>
                    <?endif?>
                  </div>
                </div>
              </td>
              <td class="col-buy">
                <div id="<?=$arItemIDs["ALL_ITEM_IDS"]['BASKET_ACTIONS']; ?>" class="button_block add-to-cart <?=(($arAddToBasketData["ACTION"] == "ORDER"/*&& !$arItem["CAN_BUY"]*/)  || !$arItem["CAN_BUY"] || !$arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] || $arAddToBasketData["ACTION"] == "SUBSCRIBE" ? "wide" : "");?>">
                  <!--noindex-->
                  <?if($calcPack):?>
                    <?$arAddToBasketData["HTML"] = str_replace('data-quantity="1"', 'data-quantity="'. $tmpValueMeter .'"', $arAddToBasketData["HTML"]);?>
                    <?$arAddToBasketData["HTML"] = str_replace('data-item="'. $arItem['ID'] .'"', 'data-item="'. $arAnalog['ID'] .'"', $arAddToBasketData["HTML"]);?>
                  <?endif?>
                  <?=$arAddToBasketData["HTML"]?>
                  <!--/noindex-->
                </div>
              </td>
            </tr>
          <?endforeach?>
        
          <?/*
          <div class="analogs-wrap">
            <a class="analog">есть аналог</a>
            <table class="analogs-table">
              <thead>
                <tr>
                  <td>Артикул</td>
                  <td>Цена</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?foreach($arItem['ANALOGS'] as $index => $item):?>
                  <tr>
                    <td><?=$item['ARTICLE']?></td>
                    <td><?=$item['PRICE']?></td>
                    <td>
                      <span class="small to-cart button transition_bg" data-item="<?=$index?>" data-float_ratio="" data-ratio="1" data-bakset_div="bx_basket_div_<?=$index?>" data-props="" data-part_props="Y" data-add_props="Y" data-empty_props="Y" data-offers="" data-iblockid="<?=$arParams["IBLOCK_ID"]?>" data-quantity=""><i></i><span>В корзину</span></span><a rel="nofollow" href="/basket/" class="small in-cart button transition_bg" data-item="<?=$index?>" style="display:none;"><i></i><span>В корзине</span></a>
                    </td>
                  </tr>
                <?endforeach?>
              </tbody>
            </table>
          </div> */?>
        <?endif?>
        
        
      <?endforeach?>
      
        
        
      </tbody>
    </table>
    
<?if($_GET['MODE'] != 'page'):?>
    
    <?if($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage):?>
      <a class="show-more-btn" data-next-page="<?=($arResult["NAV_RESULT"]->NavPageNomer + 1)?>" href="javascript:void(0)">показать ещё</a>
    <?endif?>

  </div>
  <div id="tmp-table"></div>

<?endif?>