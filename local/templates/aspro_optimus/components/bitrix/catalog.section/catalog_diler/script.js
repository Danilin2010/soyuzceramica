// ФИЛЬТР КАТАЛОГА
$(function(){

  $('.add-reviews .review-sub').click(function(){
    ajaxQuery($(this));
  });

  $('.add-reviews input[type=text]').keypress(function(){
    if(event.keyCode==13)
    {
      event.preventDefault();
      ajaxQuery($(this));
    }
  });


  if ($('#smart-filter').length) {
    $('.filter-head').on('click', function(event) {
      $this = $(this);

      if ($this.closest('.filter-container').find('.filter-droplist').hasClass('opened')) {
        $this.closest('.filter-container').find('.filter-droplist').toggleClass('opened');
        $this.closest('.filter-container').find('.filter-head').toggleClass('opened');
      } else {
        closeDropList();
        $this.closest('.filter-container').find('.filter-droplist').toggleClass('opened');
        $this.closest('.filter-container').find('.filter-head').toggleClass('opened');
      }
    });
  }

  $(document).on('click', function(event) {
    if (!$(event.target).closest('.filter-container').length) {
      closeDropList('noLoad');
    }
  });

  setCountItems();
  checkIfSelected();
  checkNubberInputs();

  $('.clear-all').on('click', function() {
    $('#smart-filter').find('[type="checkbox"]').prop("checked", false);
    checkIfSelected();
    $('.filter-container .from').val('');
    $('.filter-container .to').val('');
    $('.filter-container').find('.added-to').text('');
    $('.filter-container').find('.added-from').text('');
    closeDropList();
    callFilter();
    window.location.pathname;
    return false;
  });

  $('.filter-droplist .button').on('click', function() {
    checkNubberInputs();
    closeDropList();
    return false;
  });

  //////////////////////////////////////////////////////////////////////////////
  // Работа с разделами
  $('.sections-list li').click(function(){
    if($(this).hasClass('active'))
      return;
    sectionsCheck($(this));
  });
  
  $('.filter-section-list').live('keyup', function(){
    var needle = $(this).val();
    $(this).siblings('ul').find('li:not(.hide):not(.no-hide)').each(function(){
      if($(this).text().toUpperCase().indexOf(needle.toUpperCase()) + 1)
        $(this).removeClass('filter-hide');
      else
        $(this).addClass('filter-hide');
    });
  });
  // ФИНИШ: Работа с разделами
  //////////////////////////////////////////////////////////////////////////////
  
  $('#field-filter-name').change(function(){
    initFilter();
  });
  $("#field-filter-name").keyup(function(event){
    if(event.keyCode == 13){
      initFilter();
    }
  });
  
  $('.dillers-block').on('click', '.show-more-btn', function(){
    initFilter('page');
  });
  
  $(document).on('click', '.responcive-table .analog', function(){
    var tr = $(this).closest('tr');
    var id = tr.data('id');
    tr.addClass('is-parent').siblings('tr[data-parent='+ id +']').addClass('is-analog-show');
  })
});

/**
 * Инициализация фильтра
 */
function initFilter(mode)
{
  // Проверка авторизации
  $.post('/include/ajax/check_auth.php', function(data) {
    if(data == 'N')
      alert('Сессия истекла, вам необходимо авторизоваться на сайте.');
  });
  
  var mode = mode || false;
  var arSerialize = $('#filter-form').serializeArray();
  var arFilter = [];
  var arVars = [];
  
  //console.log('FILTER');
  //console.log(arSerialize);
  //console.log($('#field-filter-sections').val());
  //return;
  
  // Создаём массивы первого уровня
  arSerialize.forEach(function(item, i, arr) {
    if(item.value.length)
      arVars[item.name] = [];
  });
  
  // Собираем значения во второй уровень массивов
  arSerialize.forEach(function(item, i, arr) {
    if(item.value.length)
      arVars[item.name].push(item.value);
  });

  // Формируем данные
  var count = 0;
  for (key in arVars) {
    arFilter[count] = 'arFilter['+ key +']='+ implode('-and-', arVars[key]);
    count++;
  }
  
  var strFilter = '?'+ implode('&', arFilter);
  setLocation(strFilter);
  
  //console.log(strFilter);
  
  var button = $('#param-items');
  var id = button.attr('data-show-more');
  var bx_ajax_id = button.attr('data-ajax-id');
  var blockID = "#comp_"+ bx_ajax_id;
  var pageMax = parseInt(button.attr('data-max-page'));
  var pageMoreBtn = $(blockID).find('.show-more-btn');
  var page = 1;
  
  if(mode == 'page')
    page = parseInt(pageMoreBtn.attr('data-next-page'));
  
  var data = { bxajaxid: bx_ajax_id };
  data['PAGEN_'+ id] = page;
  data['MODE'] = mode;

  //console.log(data);

  animateLoadStart();

  $.ajax({
    type: "GET",
    url: window.location.href,
    data: data,
    success: function(data) {
      
      if(mode == 'page')
      {
        console.log(data);
        //$(blockID).find('.responcive-table tbody').append(data);
        
        $('#tmp-table').html(data);
        var cloneTR = $('#tmp-table table tbody tr').clone();
        $(blockID).find('.responcive-table tbody').append(cloneTR);
        $('#tmp-table').html('');
        //$(blockID).find('.responcive-table tbody script').remove();
        if(pageMax >= page)
          pageMoreBtn.attr('data-next-page', page + 1);
        else
          pageMoreBtn.hide();
      }
      else
      {
        $(blockID).html(data);
        setCountItems();
      }
      
      animateLoadStop();
    }
  });
  
}


/**
 * Работа с разделами
 */
function sectionsCheck(element)
{
  var currentId = element.data('id');
  element.addClass('active').siblings('li').removeClass('active');

  // Если есть блок секций справа
  if(element.closest('.col').next('.col').length)
  {
    var nextCol = element.closest('.col').next('.col');
    if(currentId == 'ALL')
    {
      var arSectionsCur = [];
      element.siblings('li:not(.hide):not(.no-hide)').each(function(){
        arSectionsCur.push(parseInt($(this).data('id')));
      });

      nextCol.find('ul li').each(function(){
        if(in_array(parseInt($(this).data('parent')), arSectionsCur) != -1)
          $(this).removeClass('hide active');
        else
          $(this).addClass('hide');
      });
    }
    else
    {
      nextCol.find('ul li').each(function(){
        $(this).removeClass('active filter-hide').addClass('hide');
        if($(this).data('parent') == currentId)
          $(this).removeClass('hide');
      });
    }

    // Включение/выключение блока
    if(nextCol.find('ul li:not(.hide)').length)
      nextCol.removeClass('data-empty');
    else
      nextCol.addClass('data-empty');

    nextCol.find('.filter-section-list').val('');
  }

  // Третий блок секций
  if(element.closest('.col').next('.col').next('.col').length)
  {
    var thirdCol = element.closest('.col').next('.col').next('.col');
    var arSectionsSecond = [];

    nextCol.find('ul li:not(.hide):not(.no-hide)').each(function(){
      arSectionsSecond.push(parseInt($(this).data('id')));
    });

    thirdCol.find('ul li').each(function(){
      if(in_array(parseInt($(this).data('parent')), arSectionsSecond) != -1)
        $(this).removeClass('hide active');
      else
        $(this).addClass('hide');
    });

    // Включение/выключение блока
    if(thirdCol.find('ul li:not(.hide)').length)
      thirdCol.removeClass('data-empty');
    else
      thirdCol.addClass('data-empty');

    thirdCol.find('.filter-section-list').val('');
  }
  
  if(currentId == 'ALL')
  {
    $('#field-filter-sections').val(arSectionsCur);
  }
  else
    $('#field-filter-sections').val(currentId);
  
  initFilter();
}


checkNubberInputs = function () {
  console.log('checkNubberInputs()');
  $thi$ = $('.filter-container [type="number"]');
  for (var i = $thi$.length - 1; i >= 0; i--) {
    $thisInput = $($thi$[i]);
    if ($thisInput.val().length) {
      $thisInput.closest('.filter-container').addClass('active-2');
      } else {
        $thisInput.closest('.filter-container').removeClass('active-2');
      }
  }
};

closeDropList = function (mode) {
  console.log('closeDropList('+mode+')');
  if ($('#smart-filter .filter-droplist.opened').length) {
    $('#smart-filter .filter-droplist.opened').removeClass('opened');
    $('#smart-filter .filter-head.opened').removeClass('opened');
    //if(mode != 'noLoad')
      callFilter();
  }
  checkIfSelected();
};

checkIfSelected = function () {
  console.log('checkIfSelected()');
  for (var i = $('.filter-container').length - 1; i >= 0; i--) {
    $fc = $('.filter-container')[i];
    $checkbox = $($fc).find('[type="checkbox"]:checked');
    if ($($checkbox).length > 0) {
      $($fc).addClass('active-2').find('.filter-head .added').text('— ' + $($checkbox).length);
    } else {
        if (!$($fc).find('[type="number"]').length) {
          $($fc).removeClass('active-2').find('.filter-head .added').text('');
        }
    }
  }
};

clearThis= function (el) {
  console.log('clearThis()');
  $(el).closest($('.filter-container')).find('[type="checkbox"]').prop("checked", false);
  $(el).closest($('.filter-container')).find('.from').val('');
  $(el).closest($('.filter-container')).find('.to').val('');
  // $(el).closest($('.filter-container')).find('.added-from').text('');
  // $(el).closest($('.filter-container')).find('.added-to').text('');
  checkIfSelected();
  callFilter();
};

callFilter = function () {
  console.log('callFilter()');
  checkNubberInputs();
  initFilter();
};


function animateLoadStart()
{
  waitCheck();
  $('.overlay-block-diler').stop().fadeIn('fast');
}

function animateLoadStop()
{
  waitCheck('close');
  $('.overlay-block-diler').stop().fadeOut('fast');
}

/**
 * Поиск значения в массиве
 */
function in_array(value, array) {
  if (array.indexOf) { // если метод существует
    return array.indexOf(value);
  }

  for (var i = 0; i < array.length; i++) {
    if (array[i] === value) return i;
  }

  return -1;
}

function implode(glue, pieces) {
	return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}

function parseUrlQuery() {
  var data = {};
  if(location.search) {
    var pair = (location.search.substr(1)).split('&');
    for(var i = 0; i < pair.length; i ++) {
      var param = pair[i].split('=');
      data[param[0]] = param[1];
    }
  }
  return data;
}

/**
 * Подставляем новый URL без перезагрузки страницы
 */
function setLocation(curLoc)
{
  try {
    history.pushState(null, null, curLoc);
    return;
  } catch(e) {}
  location.hash = curLoc;
}

/**
 * Обновление количества
 */
function setCountItems()
{
  $('#js-count-items').html( $('#param-items-count').val() );
}

/**
 * Меняем курсор мышки
 */
function waitCheck(mode)
{
  mode = mode || 'open';

  switch(mode)
  {
    case 'open': $('body').addClass('wait'); break;
    case 'close': $('body').removeClass('wait'); break;
    default: $('body').addClass('wait'); break;
  }
}

function isUndefined(variable)
{
  if(arguments.length == 0) throw new Error('Кого проверять?');
  return typeof variable === 'undefined';
}