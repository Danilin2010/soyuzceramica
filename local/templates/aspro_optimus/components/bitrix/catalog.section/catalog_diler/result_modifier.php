<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/*function getSubData()
{
	$dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
	mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");
  
	$arArticleShei = array();
	$arRes = array();
	$arArticShei = array();
	$query = 'CALL art_allitem("'. OSTATKI_DB_USER .'")';
	$result = mysql_query($query) or die("Query failed : " . mysql_error() ."<br>". $query ."<br>Count: ". $count);
	while($arData2 = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		if($arData2["shiequ"] && !in_array($arData2["article"], $arRes[$arData2["shiequ"]]))
			$arRes[$arData2["shiequ"]][] = $arData2["article"];
	
		if(strlen($arData2["shiequ"]) && !in_array($arData2["shiequ"], $arArticShei[$arData2["article"]]))
			$arArticShei[$arData2["article"]] = $arData2["shiequ"];
	}
	mysql_close($dbLink);

	foreach($arArticShei as $oneArticle => $sheque)
	{
		if(count($arRes[$sheque]) > 1)
		{
			foreach($arRes[$sheque] as $oneArArticle)
				if($oneArArticle != $oneArticle)
					$arArticleShei[$oneArticle][] = $oneArArticle;
		}
	}
	return $arArticleShei;
}*/

/*
function getSubData()
{
  
  $arTmp = Array();
	$res = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_ANALOG" => 'Y'), false, false, array("ID", "NAME", "IBLOCK_ID", "PROPERTY_CML2_ARTICLE"));
	while($arItem = $res->Fetch())
	{
    $arTmp[] = $arItem;
  }

}

$arAnalog = getSubData();

//if($USER->IsAdmin())
//  dbg($arResult["ITEMS"]);

$arNeedArticle = array();
foreach($arResult["ITEMS"] as $key => $arElement)
{
	if($arAnalog[$arElement['PROPERTIES']['CML2_ARTICLE']['VALUE']])
	{
		$arResult["ITEMS"][$key]['COUNTS']['SUB']['article'][] = $arAnalog[$arElement['PROPERTIES']['CML2_ARTICLE']['VALUE']];
		foreach($arAnalog[$arElement['PROPERTIES']['CML2_ARTICLE']['VALUE']] as $oneAnalog)
			$arNeedArticle[] = $oneAnalog;
	}
}


if(!empty($arNeedArticle))
{
	CModule::IncludeModule('iblock');
	CModule::IncludeModule("sale");
	$arArticleData = array();
	$arNeedArticle = array_unique($arNeedArticle);
	$res = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_CML2_ARTICLE" => $arNeedArticle), false, false, array("ID", "NAME", "PROPERTY_ELEMENT_PAGE_TITLE", "IBLOCK_ID", "PROPERTY_CML2_ARTICLE"));
	while($arNeedData = $res->Fetch())
	{
		$ipropValues = new Bitrix\Iblock\InheritedProperty\ElementValues($arNeedData["IBLOCK_ID"], $arNeedData["ID"]);
		$arFilterItemProp = $ipropValues->getValues();
		if(strlen($arFilterItemProp["ELEMENT_PAGE_TITLE"]))
			$arArticleData[$arNeedData["PROPERTY_CML2_ARTICLE_VALUE"]][$arNeedData["ID"]]["NAME"] = $arFilterItemProp["ELEMENT_PAGE_TITLE"];
		else
			$arArticleData[$arNeedData["PROPERTY_CML2_ARTICLE_VALUE"]][$arNeedData["ID"]]["NAME"] = $arNeedData["NAME"];
		$arArticleData[$arNeedData["PROPERTY_CML2_ARTICLE_VALUE"]][$arNeedData["ID"]]["ID"] = $arNeedData["ID"];
		$arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arNeedData["ID"]); 	
		$arArticleData[$arNeedData["PROPERTY_CML2_ARTICLE_VALUE"]][$arNeedData["ID"]]["MEASURE"] = $arMeasure[$arNeedData["ID"]]["MEASURE"]["SYMBOL_RUS"];
		
		$price = CPrice::GetBasePrice($arNeedData["ID"]);
		$arArticleData[$arNeedData["PROPERTY_CML2_ARTICLE_VALUE"]][$arNeedData["ID"]]["PRICE"] = $price["PRICE"];
	}
}
*/



CModule::IncludeModule('iblock');
CModule::IncludeModule("sale");


$arPrivateIDs = Array();
foreach($arResult["ITEMS"] as $indexItem => $arElement)
{
  if(!empty($arElement['PROPERTIES']['PRIVATE_ID']['VALUE']))
    $arPrivateIDs[$indexItem] = $arElement['PROPERTIES']['PRIVATE_ID']['VALUE'];
}

if(count($arPrivateIDs) > 0)
{
  // Загружаем остатки
  $dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
  mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");
  
  $query = "CALL rc_tovar.dealer_item('". OSTATKI_DB_USER ."', '^". implode('$|^', $arPrivateIDs) ."$')";
  $result = mysql_query($query) or die("Query failed : " . mysql_error() ."<br>". $query ."<br>Article: ". $arElement['PROPERTIES']['CML2_ARTICLE']['VALUE']);
  
  $arCounts = Array();
  while ($arItem = mysql_fetch_array($result, MYSQL_ASSOC)) 
  {
    $arCounts[$arItem['extcode']] = Array(
        'SKLAD' => $arItem['skl'],
        'FACTORY' => $arItem['zavod'],
        'TRANSIT' => $arItem['inway'],
        'PLAN' => '',
    );
  }
  
  mysql_close($dbLink);
  
  // Вставляем результат в глобальный массив данных
  if(count($arCounts) > 0)
  {
    foreach($arPrivateIDs as $indexItem => $privateID)
    {
      if(!empty($arCounts[ $privateID ]))
      {
        $arResult["ITEMS"][ $indexItem ]['COUNTS']['MAIN'] = $arCounts[ $privateID ];
      }
    }
  }
}



foreach($arResult["ITEMS"] as $key => $arElement)
{
  
  // Если имеется код аналога, значит собираем аналоги
  if(!empty($arElement['PROPERTIES']['CODE_ANALOG']['VALUE']))
  {
    $arAnalogues = array();
    //$arNeedArticle = array_unique($arNeedArticle);
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_ELEMENT_PAGE_TITLE", "PROPERTY_PRIVATE_ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_col_kor", "PROPERTY_m_kor");
    $res = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_CODE_ANALOG" => $arElement['PROPERTIES']['CODE_ANALOG']['VALUE'], "!ID" => $arElement['ID'], "ACTIVE" => 'Y', "GLOBAL_ACTIVE" => 'Y'), false, false, $arSelect);
    while($arAnalog = $res->Fetch())
    {
      $ID = $arAnalog["ID"];
      $ipropValues = new Bitrix\Iblock\InheritedProperty\ElementValues($arAnalog["IBLOCK_ID"], $ID);
      $arFilterItemProp = $ipropValues->getValues();
      
      //dbgz($arAnalog);
      
      if(!empty($arFilterItemProp["ELEMENT_PAGE_TITLE"]))
        $arAnalogues[$ID]["NAME"] = $arFilterItemProp["ELEMENT_PAGE_TITLE"];
      else
        $arAnalogues[$ID]["NAME"] = $arAnalog["NAME"];
      
      $arAnalogues[$ID]["ID"] = $arAnalog["ID"];
      $arAnalogues[$ID]["PREVIEW_PICTURE"] = CFile::GetByID($arAnalog['PREVIEW_PICTURE'])->GetNext();
      $arAnalogues[$ID]["DETAIL_PICTURE"] = CFile::GetByID($arAnalog['DETAIL_PICTURE'])->GetNext();
      $arAnalogues[$ID]["DETAIL_PICTURE"]['SRC'] = CFile::GetPath($arAnalog['DETAIL_PICTURE']);
      $arAnalogues[$ID]["PRIVATE_ID"] = $arAnalog["PROPERTY_PRIVATE_ID_VALUE"];
      $arAnalogues[$ID]["ARTICLE"] = $arAnalog["PROPERTY_CML2_ARTICLE_VALUE"];
      $arAnalogues[$ID]["col_kor"] = $arAnalog["PROPERTY_COL_KOR_VALUE"];
      $arAnalogues[$ID]["m_kor"] = $arAnalog["PROPERTY_M_KOR_VALUE"];
      
      $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arAnalog["ID"]);
      $arAnalogues[$ID]["MEASURE"] = $arMeasure[$arAnalog["ID"]]["MEASURE"]["SYMBOL_RUS"];
      $arAnalogues[$ID]["MEASURE_ID"] = $arMeasure[$arAnalog["ID"]]["MEASURE"]["ID"];

      $rsPrice = CPrice::GetList(Array(), Array("PRODUCT_ID" => $arAnalog["ID"], "CATALOG_GROUP_ID" => $arParams['PRICE_ID']));
      if($arPrice = $rsPrice->Fetch())
        $arAnalogues[$ID]["PRICE"] = $arPrice;

      
      // Загружаем остатки
      $dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
      mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");

      //$query = 'CALL art_oneitem("'. OSTATKI_DB_USER .'", \''. $arAnalog["PROPERTY_PRIVATE_ID_VALUE"] .'\')';
      $query = "CALL rc_tovar.dealer_item('". OSTATKI_DB_USER ."', '^". $arAnalog["PROPERTY_PRIVATE_ID_VALUE"] ."$')";
      $result = mysql_query($query) or die("Query failed : " . mysql_error() ."<br>". $query ."<br>Article: ". $arAnalog["PROPERTY_PRIVATE_ID_VALUE"]);
      $arData = mysql_fetch_array($result, MYSQL_ASSOC);
      $arAnalogues[$ID]['COUNTS'] = Array(
          'SKLAD' => $arData['skl'],
          'FACTORY' => $arData['zavod'],
          'TRANSIT' => $arData['inway'],
          'PLAN' => '',
      );

      mysql_close($dbLink);
      
    }
    
    $arResult["ITEMS"][$key]['ANALOGS'] = $arAnalogues;
  }
  
}

/* if($USER->IsAdmin())
{
  // Загружаем остатки
  $dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
  mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");

  $article = '5009';
  $query = 'CALL art_oneitem("'. OSTATKI_DB_USER .'", \''. $article .'\')';
  $result = mysql_query($query) or die("Query failed : " . mysql_error() ."<br>". $query ."<br>Count: ". $count);
  $arData = mysql_fetch_array($result, MYSQL_ASSOC);
  dbg($arData);

  mysql_close($dbLink);
} */


/*
if($USER->GetID() == 7)
{  
//mysql_close($dbLink);

  $dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
  mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");
  
  $query = 'CALL art_oneitem("'. OSTATKI_DB_USER .'", \'A2257\1146T\')';
  $result = mysql_query($query) or die("Query failed : " . mysql_error() ."<br>". $query ."<br>Count: ". $count);
  $arData = mysql_fetch_array($result, MYSQL_ASSOC);
  dbg($arData);
  
  mysql_close($dbLink);
  
//  mysql_free_result($result);
//  mysql_store_result($result);
  
//  while($result->next_result())
//    $result->store_result();
  
  
  $dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
  mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");
  
  $query = 'CALL art_oneitem("'. OSTATKI_DB_USER .'", \'A2257\1146T\')';
  $result = mysql_query($query) or die("Query failed : " . mysql_error() ."<br>". $query ."<br>Count: ". $count);
  $arData = mysql_fetch_array($result, MYSQL_ASSOC);
  dbg($arData);
  
  mysql_close($dbLink);
}
*/