function CartCallback(i,e) {
    var el=e.vars.cache.nodes[i];

    //console.log(e);

    $('#client-CITY').val(el.DISPLAY);

}

function SetOrder(add) {
    var
        templateFolder=$("[name=templateFolder]").val(),
        url=templateFolder+"/ajax.php",
        dataType='text',
        data=$("#order-form-wrap form").serializeArray(),
        objData={}
    ;
    $.each(data,function (index, value) {
        objData[value.name]=value.value;
    });
    if(add>0){
        objData["soa-action"]="saveOrderAjax";
        dataType='json';
        if(!$('#user-agree').prop("checked"))
        {
            /*$('.error_message').empty();
            var err=$('<p><font class="errortext">Необходимо согласие</font></p>');
            $('.error_message').append(err);*/


            messageShow('Необходимо согласие', 'error');


            //$('html, body').animate({ scrollTop: $('.error_message').offset().top }, 500);
            return;
        }
    }else{
        objData["soa-action"]="processOrder";
    }
    $('body').addClass('wait');
    $.ajax({
        type: "POST",
        url: url,
        dataType: dataType,
        data: objData,
        error: function(jqXHR, textStatus, errorThrown){
            $('body').removeClass('wait');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        },
        success: function(msg){
            $('body').removeClass('wait');
            if(add>0){
                if(typeof (msg.order.ERROR)!='undefined')
                {
                    /*$('.error_message').empty();
                    $.each(msg.order.ERROR,function (index, value) {
                        $.each(value,function (errindex, errvalue) {
                            var err=$('<p><font class="errortext">'+errvalue+'</font></p>');
                            $('.error_message').append(err);
                        });
                    });
                    $('html, body').animate({ scrollTop: $('.error_message').offset().top }, 500);*/
                    var text='';
                    $.each(msg.order.ERROR,function (index, value) {
                        $.each(value,function (errindex, errvalue) {
                            text=text+'<p>'+errvalue+'</p>';
                        });
                    });
                    messageShow(text, 'error');
                }
                if(typeof (msg.order.ID)!='undefined')
                {
                    window.location="/order/?ORDER_ID="+msg.order.ID;
                }
            }else{
                $("#order-form-wrap").html(msg);

            }

        }
    });
}



$(document).ready(function(){
    $("#order-form-wrap").on('change','input[type=radio]',function () {
        SetOrder();
    });

    $('body').on('click', '.js-send-order', function(){
        SetOrder(true);
    });
});
