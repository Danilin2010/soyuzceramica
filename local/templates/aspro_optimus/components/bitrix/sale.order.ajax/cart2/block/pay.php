<?php
foreach ($arResult["JS_DATA"]["PAY_SYSTEM"] as $pay)
{
    ?>
    <div class="column">
        <ul class="checkbox-list">
            <li>
                <input id="pay-ID_PAY_SYSTEM_ID_<?=$pay["PAY_SYSTEM_ID"]?>" type="radio" name="PAY_SYSTEM_ID" value="<?=$pay["ID"]?>" <?if($pay["CHECKED"]=="Y"){?>checked=""<?}?>>
                <label for="pay-ID_PAY_SYSTEM_ID_<?=$pay["PAY_SYSTEM_ID"]?>"><?=$pay["PSA_NAME"]?></label>
            </li>
        </ul>
        <div class="payments explain-text">
            <?=$pay["DESCRIPTION"]?>
        </div>
    </div>
    <?
}