<?php
foreach ($arResult["JS_DATA"]["DELIVERY"] as $delivery)
{
    ?>
    <div class="column">
        <ul class="checkbox-list">
            <li>
                <input id="curier-delivery-ID_DELIVERY_ID<?=$delivery["ID"]?>" value="<?=$delivery["ID"]?>" type="radio" name="DELIVERY_ID" <?if($delivery["CHECKED"]=="Y"){?>checked=""<?}?>>
                <label for="curier-delivery-ID_DELIVERY_ID<?=$delivery["ID"]?>"><?=$delivery["OWN_NAME"]?></label>
            </li>
        </ul>
        <div class="explain-text"><?=$delivery["DESCRIPTION"]?></div>
    </div>
    <?
}