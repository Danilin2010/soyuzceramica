<?php
global $APPLICATION;
//echo "<pre>";print_r($arResult["JS_DATA"]["ORDER_PROP"]["properties"]);
foreach ($arResult["JS_DATA"]["ORDER_PROP"]["properties"] as $properties)
{
    if($properties["IS_ZIP"]=="Y")
        continue;
    if($properties["CODE"]=="CITY")
    {
        ?><input type="hidden"
                 name="ORDER_PROP_<?=$properties["ID"]?>"
                 value="<?=$properties["VALUE"][0]?>"
                 id="client-<?=$properties["CODE"]?>"><?
        continue;
    }

    ?>
    <div class="input-row <?if($properties["TYPE"]=="DATE"){?>wish-date<?}?>">
        <label for="client-<?=$properties["CODE"]?>"><?=$properties["NAME"]?> <?if($properties["REQUIRED"]=="Y"){?>*<?}?></label>
        <?
        switch ($properties["TYPE"]) {
            /*case 0:

                break;*/
            case "LOCATION":
                $APPLICATION->IncludeComponent(
                    "bitrix:sale.location.selector.search",
                    "cart2",
                    Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "CODE" => $properties["VALUE"][0],
                        "INPUT_NAME" => "ORDER_PROP_".$properties["ID"],
                        "PROVIDE_LINK_BY" => "code",
                        "JSCONTROL_GLOBAL_ID" => "",
                        "JS_CALLBACK" => "CartCallback",
                        "FILTER_BY_SITE" => "Y",
                        "SHOW_DEFAULT_LOCATIONS" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "FILTER_SITE_ID" => "s1",
                        "INITIALIZE_BY_GLOBAL_EVENT" => "",
                        "SUPPRESS_ERRORS" => "N",
                        "CLASS_ID" => "client-".$properties["CODE"],
                    )
                );
                break;
            case "DATE":
?>

                <!--<input type="text"
                       size="<?=$properties["SIZE"]?>"
                       name="ORDER_PROP_<?=$properties["ID"]?>"
                       value="<?=$properties["VALUE"][0]?>"
                       id="client-<?=$properties["CODE"]?>"
                       class="field-style">
                <div class="calendar-picker"><img src="<?=$templateFolder?>/img/calendar.svg" alt="<?=$properties["NAME"]?>"></div>
            -->
                <?//echo CalendarDate("ORDER_PROP_".$properties["ID"], $properties["VALUE"][0], "ORDER_FORM", "15", "class=\"field-style\"")?>
                <?//=Calendar("ORDER_PROP_".$properties["ID"], "ORDER_FORM")?>

<?

                $APPLICATION->IncludeComponent('bitrix:main.calendar', 'order', Array(
                    'SHOW_INPUT' => 'Y',
                    'FORM_NAME' => 'ORDER_FORM',
                    'INPUT_NAME' => "ORDER_PROP_".$properties["ID"],
      'INPUT_VALUE' => $properties["VALUE"][0],
      'SHOW_TIME' => 'N',
      'HIDE_TIMEBAR' => 'Y',
      'INPUT_ADDITIONAL_ATTR' => 'class="field-style"'
   )
);
                ?>




            <?
                break;
            case "STRING":
            default:
                if($properties["MULTILINE"]=="Y"){
                    ?><textarea
                            cols="<?=$properties["COLS"]?>"
                            rows="<?=$properties["ROWS"]?>"
                            name="ORDER_PROP_<?=$properties["ID"]?>"
                            id="client-<?=$properties["CODE"]?>"
                            class="field-style"><?=$properties["VALUE"][0]?></textarea><?
                }else{
                    ?><input type="text"
                             size="<?=$properties["SIZE"]?>"
                             name="ORDER_PROP_<?=$properties["ID"]?>"
                             value="<?=$properties["VALUE"][0]?>"
                             id="client-<?=$properties["CODE"]?>"
                             class="field-style"><?
                }

        }
        ?>
    </div>
<?
}
?>
<div class="input-row">
    <label for="orderDescription">Комментарий к заказу</label>
    <textarea id="orderDescription" cols="4" name="ORDER_DESCRIPTION" class="field-style"><?=$arResult["JS_DATA"]["ORDER_DESCRIPTION"]?></textarea>
</div>