<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

if(IS_DILER)
{
    foreach ($arResult["JS_DATA"]["PERSON_TYPE"] as $key=>$val)
        if($key!=3)
            unset($arResult["JS_DATA"]["PERSON_TYPE"][$key]);
}else{
    foreach ($arResult["JS_DATA"]["PERSON_TYPE"] as $key=>$val)
        if($key!=1)
            unset($arResult["JS_DATA"]["PERSON_TYPE"][$key]);
}
