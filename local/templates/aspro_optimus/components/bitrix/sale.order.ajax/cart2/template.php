<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var SaleOrderAjax $component
 * @var string $templateFolder
 */

?><div class="error_message"><?
if (strlen($arResult["ERROR_MESSAGE"]) > 0)
{
    ?>
    <script>
        $(function(){
            messageShow('<?=$arResult["ERROR_MESSAGE"]?>', 'error');
        });
    </script>
    <?
    //ShowError($arResult["ERROR_MESSAGE"]);
}
?></div><?

        ?>
    <form action="<?=POST_FORM_ACTION_URI?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" class="order-form" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="<?=$arParams['ACTION_VARIABLE']?>" value="processOrder">
        <input type="hidden" name="location_type" value="code">
        <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult['BUYER_STORE']?>">
        <div class="fields-block">
            <h2 class="page-sub-title">Информация о покупателе</h2>
            <?/*if(count($arResult["JS_DATA"]["PERSON_TYPE"])==1){?>
                <input type="hidden" name="PERSON_TYPE" value="<?=$arResult["JS_DATA"]["PERSON_TYPE"][0]["ID"]?>">
                <ul class="checkbox-list">
                    <?foreach($arResult["JS_DATA"]["PERSON_TYPE"] as $PersonType){?>
                        <li>
                            <?=$PersonType["NAME"]?>
                        </li>
                    <?}?>
                </ul>
            <?}else{*/?>
                <ul class="checkbox-list">
                    <?foreach($arResult["JS_DATA"]["PERSON_TYPE"] as $PersonType){?>
                    <li>
                        <input id="user-<?=$PersonType["ID"]?>" value="<?=$PersonType["ID"]?>" type="radio" name="PERSON_TYPE" <?if($PersonType["CHECKED"]=="Y"){?>checked<?}?>>
                        <label for="user-<?=$PersonType["ID"]?>"><?=$PersonType["NAME"]?></label>
                    </li>
                    <?}?>
                </ul>
            <?//}?>
            <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/block/property.php");?>
        </div>
        <div class="checkboxes-block">
            <h2 class="page-sub-title">Оплата заказа</h2>
            <div class="columns-block">
                <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/block/pay.php");?>
            </div>
            <h2 class="page-sub-title">Доставка</h2>
            <div class="columns-block">
                <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/block/delivery.php");?>
            </div>
        </div>
        <hr>
        <ul class="checkbox-list user-agree">
            <li>
                <input id="user-agree" type="checkbox" name="save-user-agree" value="Y" checked="">
                <label for="user-agree"><span>Согласен с </span>
                    <a href="/rule/" target="_blank" class="underline">политикой конфиденциальности</a><span> и обработкой персональных данных</span>
                </label>
            </li>
        </ul>
    </form>




<?

//unset($arResult["PERSON_TYPE"]);
unset($arResult["PAY_SYSTEM"]);
//unset($arResult["ORDER_PROP"]);
unset($arResult["DELIVERY"]);
//unset($arResult["JS_DATA"]);
//unset($arResult["BASKET_ITEMS"]);
//unset($arResult["GRID"]);
//unset($arResult["ITEMS_DIMENSIONS"]);
//unset($arResult["LOCATIONS"]);
//unset($arResult["USER_VALS"]);


//echo "<pre>";print_r($arResult);echo "</pre>";
//echo "<pre>";print_r($arResult["JS_DATA"]);echo "</pre>";
//echo "<pre>";print_r($arResult["JS_DATA"]["PERSON_TYPE"]);echo "</pre>";
