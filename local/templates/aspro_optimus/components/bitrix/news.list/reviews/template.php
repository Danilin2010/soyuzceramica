<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$this->addExternalCss($templateFolder."/css/style.css");

$this->addExternalCss($templateFolder."/css/reset.css");
$this->addExternalJS($templateFolder."/js/modernizr.js");

$this->addExternalJS($templateFolder."/js/masonry.pkgd.min.js");
$this->addExternalJS($templateFolder."/js/jquery.flexslider-min.js");
$this->addExternalJS($templateFolder."/js/main.js");

?>
<div class="reviews">
  <div class="cd-testimonials-wrapper cd-container">
    <ul class="cd-testimonials">
      <?foreach($arResult["ITEMS"] as $arItem):?>
      	<?
      	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      	?>
      	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
          <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
      			<p><?echo $arItem["PREVIEW_TEXT"];?></p>
      		<?endif;?>
    			<div class="cd-author">
            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
      				<img
      					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
      					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
      					/>
        		<?endif?>
    				<ul class="cd-author-info">
    					<li><?=$arItem["PROPERTIES"]["NAME"]["VALUE"]?></li>
              <?if($arItem["PROPERTIES"]["COMPANY"]["VALUE"]):?>
    					 <li><?=$arItem["PROPERTIES"]["COMPANY"]["VALUE"]?></li>
              <?endif;?>
    				</ul>
    			</div>
      	</li>
      <?endforeach;?>
    </ul> <!-- cd-testimonials -->
  
  	<a href="#0" class="cd-see-all">Все отзывы</a>
  </div> <!-- cd-testimonials-wrapper -->
  
  <div class="cd-testimonials-all">
  	<div class="cd-testimonials-all-wrapper">
  		<ul>
        <?foreach($arResult["ITEMS"] as $arItem):?>
        	<?
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	?>
          <li class="cd-testimonials-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    				<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
        			<p><?echo $arItem["PREVIEW_TEXT"];?></p>
        		<?endif;?>
            
    				<div class="cd-author">
    					<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
        				<img
        					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
        					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
        					/>
          		<?endif?>
    					<ul class="cd-author-info">
    						<li><?=$arItem["PROPERTIES"]["NAME"]["VALUE"]?></li>
                <?if($arItem["PROPERTIES"]["COMPANY"]["VALUE"]):?>
      					 <li><?=$arItem["PROPERTIES"]["COMPANY"]["VALUE"]?></li>
                <?endif;?>
    					</ul>
    				</div> <!-- cd-author -->
    			</li>
        <?endforeach;?>
  		</ul>
  	</div>	<!-- cd-testimonials-all-wrapper -->
  
  	<a href="#0" class="close-btn">Закрыть</a>
  </div> <!-- cd-testimonials-all -->
</div>