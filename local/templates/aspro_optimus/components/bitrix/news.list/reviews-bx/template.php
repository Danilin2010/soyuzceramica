<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//$this->addExternalCss(SITE_TEMPLATE_PATH ."/css/jquery.bxslider.css");
$this->addExternalCss($templateFolder."/css/style.css");
$this->addExternalJS(SITE_TEMPLATE_PATH ."/js/jquery.bxslider.js");

?>
<script>
  $(function(){
    $('.box-opinion .items').bxSlider({
      adaptiveHeight: true,
      mode: 'fade',
      prevSelector: $('#opinion-prev'),
      nextSelector: $('#opinion-next'),
      nextText: '', 
      prevText: ''
    });
  })
</script>

<div class="box-opinion">
  <div class="wrapper">
    <div class="wrap-indent">
      <div class="caption tab-links top_block"><a class="active">Отзывы наших клиентов</a></div>
      <div class="items">
        <?
        foreach($arResult["ITEMS"] as $key => $arItem)
        {
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
          
          if($key == 0 || $key%2 == 0)
            echo '<div class="slide">';
          ?>
          <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="photo-wrapper">
              <img class="photo" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
            </div>
            <div class="info">
              <div class="name"><?=$arItem['PROPERTIES']['NAME']['VALUE']?></div>
              <!-- div class="salary">Расчёт по проекту К-198-1</div -->
            </div>
            <div class="text"><?=$arItem['PREVIEW_TEXT']?></div>
          </div>
          <?
          if($key%2 != 0 || count($arResult["ITEMS"])-1 == $key)
            echo '</div>';
        }
        ?>
      </div>
      <div id="opinion-prev" class="opinion-arrow prev"></div>
      <div id="opinion-next" class="opinion-arrow next"></div>
      <div class="clear"></div>
    </div>
  </div>
</div>