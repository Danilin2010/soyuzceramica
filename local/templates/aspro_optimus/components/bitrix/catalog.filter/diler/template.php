<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//dbg($arParams['FILTER_PARAMS']);
//dbg($arResult);
?>
<form id="filter-form">
  <input type="hidden" id="field-filter-sections" name="sections" value="<?=$arParams['FILTER_PARAMS']['sections'][0]?>" />
  <div class="collection-filter">
    <div class="col wrap-section-level-1">
      <div class="col-head">Коллекция</div>
      <input class="filter-section-list" type="text" placeholder="Введите название коллекции">
      <ul id="sections-level-1" class="sections-list" data-level="1">
        <li class="no-hide active" data-id="ALL">Все коллекции</li>
        <?foreach($arResult['DATA']['SECTIONS']['LEVEL_1'] as $arItem):?>
          <li data-id="<?=$arItem['ID']?>"><?=$arItem['NAME']?></li>
        <?endforeach?>
      </ul>
    </div>
    <div class="col wrap-section-level-2">
      <div class="col-head">Каталог</div>
      <input class="filter-section-list" type="text" placeholder="Введите название каталога">
      <ul id="sections-level-2" class="sections-list" data-level="2">
        <li class="no-hide" data-id="ALL">Все каталоги</li>
        <?foreach($arResult['DATA']['SECTIONS']['LEVEL_2'] as $arItem):?>
          <li class="" data-id="<?=$arItem['ID']?>" data-parent="<?=$arItem['PARENT']?>"><?=$arItem['NAME']?></li>
        <?endforeach?>
        <? /* <li>Фантазия <span>(181)</span></li> */ ?>
      </ul>
      <div class="overlay-list"></div>
    </div>
    <div class="col wrap-section-level-3">
      <div class="col-head">Подкаталог</div>
      <input class="filter-section-list" type="text" placeholder="Введите название подкаталога">
      <ul id="sections-level-3" class="sections-list" data-level="3">
        <?foreach($arResult['DATA']['SECTIONS']['LEVEL_3'] as $arItem):?>
          <li class="" data-id="<?=$arItem['ID']?>" data-parent="<?=$arItem['PARENT']?>"><?=$arItem['NAME']?></li>
        <?endforeach?>
      </ul>
      <div class="overlay-list"></div>
    </div>
  </div>

  <div class="input-row">
    <label for="field-filter-name">Поиск по выбранным каталогам</label>
    <input id="field-filter-name" type="text" name="name" placeholder="Название товара или артикул" value="<?=$arParams['FILTER_PARAMS']['name'][0]?>">
  </div>

  <div id="smart-filter">
    <div class="smart-form" id="smart-form">
      <div class="filter-container">
        <div class="filter-head">
          <span>Цена </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <li>
              <label>
                <span class="fixed-small">от</span>
                <input class="from min-price" type="number" name="pricef" id="field-filter-pricef" value="<?=$arParams['FILTER_PARAMS']['pricef'][0]?>" min="1">
              </label>
            </li>
            <li>
              <label>
                <span class="fixed-small">до</span>
                <input class="to max-price" type="number" name="pricet" id="field-filter-pricet" value="<?=$arParams['FILTER_PARAMS']['pricef'][0]?>" min="1">
              </label>
            </li>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>
      <div class="filter-container">
        <div class="filter-head">
          <span>Назначение <span class="added"></span></span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <?foreach($arResult['FILTERS']['naznachenie'] as $index => $arItem):?>
              <li><label for="field-naznachenie-<?=$index?>">
                  <input type="checkbox" id="field-naznachenie-<?=$index?>" value="<?=$arItem['ID']?>" name="naznachenie"<?if(in_array($arItem['ID'], $arParams['FILTER_PARAMS']['naznachenie'])):?> checked="checked"<?endif?>>
                  <span><?=$arItem['VALUE']?></span>
              </label></li>
            <?endforeach?>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>
      <div class="filter-container">
        <div class="filter-head">
          <span>Поверхность <span class="added"></span>
          </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <?foreach($arResult['FILTERS']['poverh'] as $index => $arItem):?>
              <li><label for="field-poverh-<?=$index?>">
                  <input type="checkbox" id="field-poverh-<?=$index?>" value="<?=$arItem['ID']?>" name="poverh"<?if(in_array($arItem['ID'], $arParams['FILTER_PARAMS']['poverh'])):?> checked="checked"<?endif?>>
                  <span><?=$arItem['VALUE']?></span>
              </label></li>
            <?endforeach?>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>

      <div class="filter-container">
        <div class="filter-head">
          <span>Цвет <span class="added"></span>
          </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <?foreach($arResult['FILTERS']['color'] as $index => $arItem):?>
              <li><label for="field-color-<?=$index?>">
                  <input type="checkbox" id="field-color-<?=$index?>" value="<?=$arItem['ID']?>" name="color"<?if(in_array($arItem['ID'], $arParams['FILTER_PARAMS']['color'])):?> checked="checked"<?endif?>>
                  <!-- b class="color-1"></b -->
                  <span><?=strtolower($arItem['VALUE'])?></span>
              </label></li>
            <?endforeach?>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>

      <div class="filter-container">
        <div class="filter-head">
          <span>Рисунок <span class="added"></span>
          </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <?foreach($arResult['FILTERS']['picture'] as $index => $arItem):?>
              <li><label for="field-picture-<?=$index?>">
                  <input type="checkbox" id="field-picture-<?=$index?>" value="<?=$arItem['ID']?>" name="picture"<?if(in_array($arItem['ID'], $arParams['FILTER_PARAMS']['picture'])):?> checked="checked"<?endif?>>
                  <span><?=$arItem['VALUE']?></span>
              </label></li>
            <?endforeach?>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>

      <div class="filter-container">
        <div class="filter-head">
          <span>Размер <span class="added"></span>
          </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <?$count = 0;?>
            <?foreach($arResult['FILTERS']['razm'] as $index => $value):?>
              <li><label for="field-razm-<?=$count?>">
                  <input type="checkbox" id="field-razm-<?=$count?>" value="<?=$value?>" name="razm"<?if(in_array($value, $arParams['FILTER_PARAMS']['razm'])):?> checked="checked"<?endif?>>
                  <span><?=str_replace('*', ' <span class="split-value">x</span> ', $value)?></span>
              </label></li>
              <?$count++;?>
            <?endforeach?>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>
      
      <? /*
      <div class="filter-container">
        <div class="filter-head">
          <span>Размер
          </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <li>
              <label>
                <span class="fixed-small">от</span>
                  <input class="from min-price" type="number" name="" id="" value="" min="1" ">
              </label>
            </li>
            <li>
              <label>
                <span class="fixed-small">до</span>
                <input class="to max-price" name="" id="" value="" type="number" min="1" ">
              </label>
            </li>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>
      */ ?>
      
      <div class="filter-container">
        <div class="filter-head">
          <span>Материал <span class="added"></span>
          </span>
          <span class="fa fa-angle-down"></span>
        </div>
        <div class="filter-droplist">
          <ul>
            <?foreach($arResult['FILTERS']['tupe'] as $index => $arItem):?>
              <li><label for="field-tupe-<?=$index?>">
                  <input type="checkbox" id="field-tupe-<?=$index?>" value="<?=$arItem['ID']?>" name="tupe"<?if(in_array($arItem['ID'], $arParams['FILTER_PARAMS']['tupe'])):?> checked="checked"<?endif?>>
                  <span><?=$arItem['VALUE']?></span>
              </label></li>
            <?endforeach?>
          </ul>
          <input class="button" type="submit" name="set_filter" value="Применить">
        </div>
      </div>

      <a href="#" class="clear-all"><span id="js-count-items" title="Найдено позиций"></span><span class="fa fa-close"></span>Сбросить</a>

    </div>
  </div>

</form>