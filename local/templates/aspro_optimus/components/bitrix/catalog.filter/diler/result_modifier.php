<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//$arRs = CIBlockElement::GetList(Array('PROPERTY_DURATION' => 'DESC'), Array('IBLOCK_ID' => IBLOCK_TRAVEL_ID, '!PROPERTY_DURATION' => false), false, Array('nPageSize'=>1), Array('PROPERTY_DURATION'))->Fetch();
//$arResult['DATA']['DURATION']['MAX'] = $arRs['PROPERTY_DURATION_VALUE'];
//$arRs = CIBlockElement::GetList(Array('PROPERTY_DURATION' => 'ASC'), Array('IBLOCK_ID' => IBLOCK_TRAVEL_ID, '!PROPERTY_DURATION' => false), false, Array('nPageSize'=>1), Array('PROPERTY_DURATION'))->Fetch();
//$arResult['DATA']['DURATION']['MIN'] = $arRs['PROPERTY_DURATION_VALUE'];


// Назначение
$rsEnums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID" => 50, "CODE" => "naznachenie"));
while($arFields = $rsEnums->GetNext())
  $arResult['FILTERS']['naznachenie'][] = $arFields;

// Поверхность
$rsEnums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID" => 50, "CODE" => "poverh"));
while($arFields = $rsEnums->GetNext())
  $arResult['FILTERS']['poverh'][] = $arFields;

// Цвет
$rsEnums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID" => 50, "CODE" => "color"));
while($arFields = $rsEnums->GetNext())
  $arResult['FILTERS']['color'][] = $arFields;

// Рисунок
$rsEnums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID" => 50, "CODE" => "picture"));
while($arFields = $rsEnums->GetNext())
  $arResult['FILTERS']['picture'][] = $arFields;

// Материал
$rsEnums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID" => 50, "CODE" => "tupe"));
while($arFields = $rsEnums->GetNext())
  $arResult['FILTERS']['tupe'][] = $arFields;


// Размер (строка)
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_razm");
$arFilter = Array("IBLOCK_ID" => 50, "ACTIVE" => "Y", "!PROPERTY_razm" => false);
$res = CIBlockElement::GetList(Array("PROPERTY_razm" => "ASC"), $arFilter, Array("PROPERTY_razm"), false, $arSelect);
while($item = $res->Fetch())
  $arPropRazm[] = $item['PROPERTY_RAZM_VALUE'];

foreach($arPropRazm as $value)
{
  $arVal = explode('*', $value);
  $arPropTmp[ str_replace(',', '.', $arVal[0]) ] = $value;
}
ksort($arPropTmp);
$arResult['FILTERS']['razm'] = $arPropTmp;



$arFilter = Array('IBLOCK_ID' => 50, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => Array(1, 2, 3)); 
$rsSection = CIBlockSection::GetList(Array("left_margin"=>"asc", 'NAME' => 'ASC'), $arFilter);
while($arSection = $rsSection->Fetch())
{
  $PARENT_ID = '';
  if($arSection['DEPTH_LEVEL'] > 1)
  {
    $nav = CIBlockSection::GetNavChain(50, $arSection['ID']);
    while($item = $nav->Fetch())
      $arTmpChain[] = $item;
    
    $arTmpChain = array_reverse($arTmpChain);
    $PARENT_ID = $arTmpChain[1]['ID'];
  }
  
  $arTree['LEVEL_'. $arSection['DEPTH_LEVEL'] ][] = Array(
      'ID' => $arSection['ID'],
      'NAME' => $arSection['NAME'],
      'PARENT' => $PARENT_ID,
  );
}

// Сортируем по имени
foreach($arTree as $index => $value)
  usort($arTree[$index], function($a, $b){
    if($a['NAME'] === $b['NAME']) return 0;
    return $a['NAME'] > $b['NAME'] ? 1 : -1;
  });

$arResult['DATA']['SECTIONS'] = $arTree;

