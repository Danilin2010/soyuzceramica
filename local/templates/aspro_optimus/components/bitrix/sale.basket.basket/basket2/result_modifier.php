<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;

$arResult['allSum_FORMATED']=str_replace('руб.', "",$arResult['allSum_FORMATED']);
$arResult['allSum_FORMATED']=trim($arResult['allSum_FORMATED']);


foreach ($arResult['ITEMS']["AnDelCanBuy"] as &$item){
    $item['PRICE_FORMATED']=str_replace('руб.', "",$item['PRICE_FORMATED']);
    $item['PRICE_FORMATED']=trim($item['PRICE_FORMATED']);
    $item['SUM']=str_replace('руб.', "",$item['SUM']);
    $item['SUM']=trim($item['SUM']);
    $item['SUM_FULL_PRICE_FORMATED']=str_replace('руб.', "",$item['SUM_FULL_PRICE_FORMATED']);
    $item['SUM_FULL_PRICE_FORMATED']=trim($item['SUM_FULL_PRICE_FORMATED']);
}unset($item);