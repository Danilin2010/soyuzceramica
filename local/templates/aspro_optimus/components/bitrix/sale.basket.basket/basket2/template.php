<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
CJSCore::Init(array("ajax"));
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\DiscountCouponsManager;
$documentRoot = Main\Application::getDocumentRoot();

$curPage = $APPLICATION->GetCurPage().'?'.$arParams["ACTION_VARIABLE"].'=';
$arUrls = array(
    "delete" => $curPage."delete&id=#ID#",
    "delay" => $curPage."delay&id=#ID#",
    "add" => $curPage."add&id=#ID#",
);
unset($curPage);

$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$arBasketJSParams = array(
    'SALE_DELETE' => GetMessage("SALE_DELETE"),
    'SALE_DELAY' => GetMessage("SALE_DELAY"),
    'SALE_TYPE' => GetMessage("SALE_TYPE"),
    'TEMPLATE_FOLDER' => $templateFolder,
    'DELETE_URL' => $arUrls["delete"],
    'DELAY_URL' => $arUrls["delay"],
    'ADD_URL' => $arUrls["add"],
    'EVENT_ONCHANGE_ON_START' => (!empty($arResult['EVENT_ONCHANGE_ON_START']) && $arResult['EVENT_ONCHANGE_ON_START'] === 'Y') ? 'Y' : 'N',
    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
    'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
);
?>
<script type="text/javascript">
    var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams);?>;
</script>


<div class="cart-page">
    <?if (empty($arResult['ERROR_MESSAGE'])){
        if(count($arResult['ITEMS']["AnDelCanBuy"])>0){?>
    <form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_items_list">

        <?=bitrix_sessid_post()?>
        <input type="hidden" name="site_id" value="<?=SITE_ID?>">
        <input type="hidden" name="templateFolder" value="<?=$templateFolder?>">

    <table class="styled-table" id="basket_items">

        <input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
        <input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
        <input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
        <input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />

        <tr class="row table-head">
            <td class="product"></td>
            <td><?=Loc::getMessage("SALE_SALE")?></td>
            <td><?=Loc::getMessage("SALE_PRICE")?></td>
            <td><?=Loc::getMessage("SALE_QUANTITY")?></td>
            <td><?=Loc::getMessage("SALE_SUM")?></td>
            <td></td>
        </tr>
            <?foreach ($arResult['ITEMS']["AnDelCanBuy"] as $item){
                $calcPack = false;
                if(($item['MEASURE'] == 6 || $item['MEASURE_CODE'] == 55) && !empty($item['PROPERTY_col_kor_VALUE']) && !empty($item['PROPERTY_m_kor_VALUE']))
                    $calcPack = true;
                $ratio = isset($item["MEASURE_RATIO"]) ? $item["MEASURE_RATIO"] : 0;
                $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                ?>
                <tr
                        id="<?=$item["ID"]?>"
                        data-item-name="<?=$item["NAME"]?>"
                        data-item-brand="<?=$item[$arParams['BRAND_PROPERTY']."_VALUE"]?>"
                        data-item-price="<?=$item["PRICE"]?>"
                        data-item-currency="<?=$item["CURRENCY"]?>"
                        class="row">
                    <td class="product"><a href="<?=$item["DETAIL_PAGE_URL"]?>"><img src="<?=$item["PREVIEW_PICTURE_SRC"]?>" alt=""></a><a href="<?=$item["DETAIL_PAGE_URL"]?>"><span><?=$item["NAME"]?> <?=$item['PROPERTY_CML2_ARTICLE_VALUE']?></span></a></td>
                    <td class="discount-td" id="discount_value_<?=$item["ID"]?>"><span class='hidden'><?=Loc::getMessage("SALE_SALE")?> </span><?=$item["DISCOUNT_PRICE_PERCENT_FORMATED"]?></td>
                    <td class="price"><span class='hidden'><?=Loc::getMessage("SALE_PRICE")?> </span> <?=$item["PRICE_FORMATED"]?><span> <?=Loc::getMessage("SALE_SUFFIX")?></span>
                        <?if($item["PRICE"]!=$item["FULL_PRICE"]){?>
                        <div class="old_price" id="old_price_<?=$item["ID"]?>">
                            <span class="price_value"><?=$item["SUM_FULL_PRICE_FORMATED"]?></span>
                            <span class="price_currency"> <?=Loc::getMessage("SALE_SUFFIX")?></span>
                        </div>
                        <?}?>
                    </td>
                    <?

                    if (!isset($item["MEASURE_RATIO"]))
                    {
                        $item["MEASURE_RATIO"] = 1;
                    }



                    ?>
                    <?if($calcPack){?>
                    <td class="quantity-td with-counter-units">
                        <div class="counter-units-wrapp"
                             data-id="<?=$item['ID']?>"
                             data-price="<?=$item['PRICE']?>"
                             data-count-in-pack="<?=$item['PROPERTY_col_kor_VALUE']?>"
                             data-meter-in-pack="<?=$item['PROPERTY_m_kor_VALUE']?>"
                             date-ratio="<?=$ratio?>"
                             data-use-float-quantity-js="<?=$useFloatQuantityJS?>"
                        >
                            <div class="counter_block unit-meter" data-offers="<?=($item["OFFERS"] ? "Y" : "N");?>" data-item="<?=$item["ID"];?>">
                                <? $tmpValueMeter = $item['QUANTITY']; ?>
                                <input type="text"
                                       class="text js-calc-pack"
                                       id="QUANTITY_INPUT_<?=$item["ID"]?>"
                                       name="QUANTITY_INPUT_<?=$item["ID"]?>"
                                       value="<?=$tmpValueMeter?>"
                                       autocomplete="off"
                                       onchange="updateQuantity('QUANTITY_INPUT_<?=$item["ID"]?>', '<?=$item["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)" />
                                />
                                <label><?=Loc::getMessage("SALE_UNIT_M2")?></label>
                            </div>
                            <br>
                            <div class="counter_block unit-pack" data-offers="<?=($item["OFFERS"] ? "Y" : "N");?>" data-item="<?=$item["ID"];?>">
                                <? $tmpValuePack = round(str_replace(',', '.', $item['QUANTITY'] / $item['PROPERTY_m_kor_VALUE']), 4); ?>
                                <input type="text"
                                       class="text js-calc-pack"
                                       id="QUANTITY_INPUT_<?=$item["ID"]?>_pack"
                                       name="QUANTITY_INPUT_<?=$item["ID"]?>_pack"
                                       value="<?=$tmpValuePack?>"
                                       autocomplete="off"
                                       onchange="updateQuantity('QUANTITY_INPUT_<?=$item["ID"]?>', '<?=$item["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)" />
                                />
                                <label><?=Loc::getMessage("SALE_UNIT_YUP")?></label>
                            </div>
                            <br>
                            <div class="counter_block unit-thing" data-offers="<?=($item["OFFERS"] ? "Y" : "N");?>" data-item="<?=$item["ID"];?>">
                                <input type="text"
                                       class="text js-calc-pack"
                                       id="QUANTITY_INPUT_<?=$item["ID"]?>_thing"
                                       name="QUANTITY_INPUT_<?=$item["ID"]?>_thing"
                                       value="<?=round($tmpValuePack * $item['PROPERTY_col_kor_VALUE'], 4)?>"
                                       autocomplete="off"
                                       onchange="updateQuantity('QUANTITY_INPUT_<?=$item["ID"]?>', '<?=$item["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)" />
                                <label><?=Loc::getMessage("SALE_UNIT_PC")?></label>
                            </div>
                        </div>
                        <input type="hidden" id="QUANTITY_<?=$item['ID']?>" name="QUANTITY_<?=$item['ID']?>" value="<?=$tmpValueMeter?>" />
                    </td>
                    <?}else{?>
                        <td class="quantity-td">
                            <div class="quantity">
                                <div class="q-btn decrement" onclick="setQuantity(<?=$item["ID"]?>, <?=$item["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);">-</div>
                                <input
                                        type="number"
                                        value="<?=$item["QUANTITY"]?>"
                                        min="1"
                                        size="3"
                                        class="q-result"
                                        data-id="<?=$item["ID"]?>"
                                        id="QUANTITY_INPUT_<?=$item["ID"]?>"
                                        name="QUANTITY_INPUT_<?=$item["ID"]?>"
                                        onchange="updateQuantity('QUANTITY_INPUT_<?=$item["ID"]?>', '<?=$item["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
                                >
                                <div class="q-btn increment" onclick="setQuantity(<?=$item["ID"]?>, <?=$item["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);">+</div>
                            </div>
                            <input type="hidden" id="QUANTITY_<?=$item['ID']?>" name="QUANTITY_<?=$item['ID']?>" value="<?= $item['QUANTITY']?>" />
                        </td>
                    <?}?>
                    <td class="summ" id="sum_<?=$item["ID"]?>"><span class='hidden'><?=Loc::getMessage("SALE_SUM")?> </span> <?=$item["SUM"]?><span> <?=Loc::getMessage("SALE_SUFFIX")?></span></td>
                    <td class="btns">
                        <a href="<?=str_replace("#ID#", $item["ID"], $arUrls["delay"])?>" class="add-to-fav"></a>
                        <a href="<?=str_replace("#ID#", $item["ID"], $arUrls["delete"])?>" onclick="return deleteProductRow(this)" class="remove"></a>
                    </td>
                </tr>
            <?}?>
    </table>
    <div class="total-row"><span><?=Loc::getMessage("SALE_TOTAL")?>:</span><span class="total-summ" id="allVATSum_FORMATED"><?=$arResult['allSum_FORMATED']?> <?=Loc::getMessage("SALE_SUFFIX")?></span></div>
    </form>
    <div id="order-form-wrap" class="order-form-wrap">
        <?include($documentRoot.$templateFolder."/order.php");?>
    </div>
    <div class="cart-final-btn"><a class="btn btn-md button js-show-order-form"><?=Loc::getMessage("SALE_ORDER")?></a></div>
<?
        }
    }
    else
    {
        ShowError($arResult['ERROR_MESSAGE']);
    }
?>
</div>