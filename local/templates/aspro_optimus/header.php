<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if($GET["debug"] == "y"){
  error_reporting(E_ERROR | E_PARSE);
}
IncludeTemplateLangFile(__FILE__);
global $APPLICATION, $TEMPLATE_OPTIONS, $arSite;
$arSite = CSite::GetByID(SITE_ID)->Fetch();
$htmlClass = ($_REQUEST && isset($_REQUEST['print']) ? 'print' : false);
?><!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>" xmlns="http://www.w3.org/1999/xhtml" <?=($htmlClass ? 'class="'.$htmlClass.'"' : '')?>>
<head>
  <title><?$APPLICATION->ShowTitle()?></title>
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/images/favicon/favicon-128.png" sizes="128x128" />
  <meta name="yandex-verification" content="88131c320cc7d8af" />
  <meta name="google-site-verification" content="AO03FnIXtwdR0njpZwXkSrqI4bLg0mCr2UqWLEgD4BU" />
  <meta name="author" content="arlix.ru"/>
  <meta name="application-name" content="&nbsp;"/>
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="<?=SITE_TEMPLATE_PATH?>/images/favicon/mstile-144x144.png" />
  <meta name="msapplication-square70x70logo" content="<?=SITE_TEMPLATE_PATH?>/images/favicon/mstile-70x70.png" />
  <meta name="msapplication-square150x150logo" content="<?=SITE_TEMPLATE_PATH?>/images/favicon/mstile-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="<?=SITE_TEMPLATE_PATH?>/images/favicon/mstile-310x150.png" />
  <meta name="msapplication-square310x310logo" content="<?=SITE_TEMPLATE_PATH?>/images/favicon/mstile-310x310.png" />
	<?$APPLICATION->ShowMeta("viewport");?>
	<?$APPLICATION->ShowMeta("HandheldFriendly");?>
	<?$APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes");?>
	<?$APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style");?>
	<?$APPLICATION->ShowMeta("SKYPE_TOOLBAR");?>
	<?$APPLICATION->ShowHead();?>
  <?if(IS_DILER):?>
    <?$APPLICATION->AddHeadString('<link href="/bitrix/templates/aspro_optimus/components/bitrix/catalog.section/catalog_diler/styles_addAfterTemplateStyles.css" type="text/css"  rel="stylesheet" />');?>
  <?endif?>
	<?$APPLICATION->AddHeadString('<script>BX.message('.CUtil::PhpToJSObject( $MESS, false ).')</script>', true);?>
	<?if(CModule::IncludeModule("aspro.optimus")) {COptimus::Start(SITE_ID);}?>
	<!--[if gte IE 9]><style type="text/css">.basket_button, .button30, .icon {filter: none;}</style><![endif]-->
	<link href='<?=CMain::IsHTTPS() ? 'https' : 'http'?>://fonts.googleapis.com/css?family=Ubuntu:400,500,700,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
	<body id="main" class="<?if(strstr($APPLICATION->GetCurDir(), '/catalog/')):?>page-catalog<?endif?><?if(IS_DILER):?> is-dealer<?endif?>">
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
		<?if(!CModule::IncludeModule("aspro.optimus")){?><center><?$APPLICATION->IncludeFile(SITE_DIR."include/error_include_module.php");?></center></body></html><?die();?><?}?>
		<?$APPLICATION->IncludeComponent("aspro:theme.optimus", ".default", array("COMPONENT_TEMPLATE" => ".default"), false);?>
		<?COptimus::SetJSOptions();?>
		<div class="wrapper <?=(COptimus::getCurrentPageClass());?> basket_<?=strToLower($TEMPLATE_OPTIONS["BASKET"]["CURRENT_VALUE"]);?> <?=strToLower($TEMPLATE_OPTIONS["MENU_COLOR"]["CURRENT_VALUE"]);?> banner_auto">
			<div class="header_wrap <?=strtolower($TEMPLATE_OPTIONS["HEAD_COLOR"]["CURRENT_VALUE"])?>">
				<?if($TEMPLATE_OPTIONS["BASKET"]["CURRENT_VALUE"]=="NORMAL"){?>
					<div class="top-h-row">
						<div class="wrapper_inner">
							<div class="top_inner">
								<div class="content_menu">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
										array(
											"COMPONENT_TEMPLATE" => ".default",
											"PATH" => SITE_DIR."include/topest_page/menu.top_content_row.php",
											"AREA_FILE_SHOW" => "file",
											"AREA_FILE_SUFFIX" => "",
											"AREA_FILE_RECURSIVE" => "Y",
											"EDIT_TEMPLATE" => "standard.php"
										),
										false
									);?>
								</div>
								<div class="phones">
									<div class="phone_block">
										<span class="phone_wrap">
											<span class="icons fa fa-phone"></span>
											<span class="phone_text">
  											<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
													array(
														"COMPONENT_TEMPLATE" => ".default",
														"PATH" => SITE_DIR."include/phone.php",
														"AREA_FILE_SHOW" => "file",
														"AREA_FILE_SUFFIX" => "",
														"AREA_FILE_RECURSIVE" => "Y",
														"EDIT_TEMPLATE" => "standard.php"
													),
													false
												);?>
											</span>
										</span>
										<span class="order_wrap_btn">
											<span class="callback_btn"><?=GetMessage("CALLBACK")?></span>
										</span>
									</div>
								</div>
								<div class="h-user-block" id="personal_block">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
										array(
											"COMPONENT_TEMPLATE" => ".default",
											"PATH" => SITE_DIR."include/topest_page/auth.top.php",
											"AREA_FILE_SHOW" => "file",
											"AREA_FILE_SUFFIX" => "",
											"AREA_FILE_RECURSIVE" => "Y",
											"EDIT_TEMPLATE" => "standard.php"
										),
										false
									);?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				<?}?>
				<header id="header">
					<div class="wrapper_inner">
						<div class="top_br"></div>
						<table class="middle-h-row">
							<tr>
								<td class="logo_wrapp">
									<? /* <div class="logo nofill_<?=strtolower(\Bitrix\Main\Config\Option::get('aspro.optimus', 'NO_LOGO_BG', 'N'));?>"> */ ?>
                	<a class="logo" href="/">
                  	<img class="img-logo" src="/bitrix/templates/aspro_optimus/images/logo.svg" alt="">
                  	<span><b>СОЮЗ-КЕРАМИКА</b> - Официальный дистрибьютор</span>
                	</a>
								</td>
								<td class="text_wrapp">
                	<img class="img-top" src="/bitrix/templates/aspro_optimus/images/top.svg" alt="">
									<!-- <div class="slogan">
										<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
											array(
												"COMPONENT_TEMPLATE" => ".default",
												"PATH" => SITE_DIR."include/top_page/slogan.php",
												"AREA_FILE_SHOW" => "file",
												"AREA_FILE_SUFFIX" => "",
												"AREA_FILE_RECURSIVE" => "Y",
												"EDIT_TEMPLATE" => "standard.php"
											),
											false
										);?>
									</div> -->
								</td>
								<td  class="center_block">
									<div class="search">
                    <?if(IS_DILER):?>
                      <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                        array(
                          "COMPONENT_TEMPLATE" => ".default",
                          "PATH" => SITE_DIR."include/top_page/search.title.diler.php",
                          "AREA_FILE_SHOW" => "file",
                          "AREA_FILE_SUFFIX" => "",
                          "AREA_FILE_RECURSIVE" => "Y",
                          "EDIT_TEMPLATE" => "standard.php"
                        ),
                        false
                      );?>
                    <?else:?>
                      <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                        array(
                          "COMPONENT_TEMPLATE" => ".default",
                          "PATH" => SITE_DIR."include/top_page/search.title.catalog.php",
                          "AREA_FILE_SHOW" => "file",
                          "AREA_FILE_SUFFIX" => "",
                          "AREA_FILE_RECURSIVE" => "Y",
                          "EDIT_TEMPLATE" => "standard.php"
                        ),
                        false
                      );?>
                    <?endif?>
									</div>
								</td>
								<td class="work_wrapp">
                  <div class="mode-work">пн - сб с 10:00 до 20:00<br> вс с 10:00 до 18:00</div>
                </td>
								<td class="basket_wrapp">
									<?if($TEMPLATE_OPTIONS["BASKET"]["CURRENT_VALUE"] == "NORMAL"){?>
										<div class="wrapp_all_icons">
											<div class="header-compare-block icon_block iblock" id="compare_line" >
												<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
													array(
														"COMPONENT_TEMPLATE" => ".default",
														"PATH" => SITE_DIR."include/top_page/catalog.compare.list.compare_top.php",
														"AREA_FILE_SHOW" => "file",
														"AREA_FILE_SUFFIX" => "",
														"AREA_FILE_RECURSIVE" => "Y",
														"EDIT_TEMPLATE" => "standard.php"
													),
													false
												);?>
											</div>
											<div class="header-cart" id="basket_line">
												<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
													array(
														"COMPONENT_TEMPLATE" => ".default",
														"PATH" => SITE_DIR."include/top_page/comp_basket_top.php",
														"AREA_FILE_SHOW" => "file",
														"AREA_FILE_SUFFIX" => "",
														"AREA_FILE_RECURSIVE" => "Y",
														"EDIT_TEMPLATE" => "standard.php"
													),
													false
												);?>
											</div>
										</div>
									<?}else{?>
										<div class="header-cart fly" id="basket_line">
											<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
												array(
													"COMPONENT_TEMPLATE" => ".default",
													"PATH" => SITE_DIR."include/top_page/comp_basket_top.php",
													"AREA_FILE_SHOW" => "file",
													"AREA_FILE_SUFFIX" => "",
													"AREA_FILE_RECURSIVE" => "Y",
													"EDIT_TEMPLATE" => "standard.php"
												),
												false
											);?>
										</div>
										<div class="middle_phone">
											<div class="phones">
												<span class="phone_wrap">
													<span class="phone">
                            <span class="icons fa fa-phone"></span>
                            <span class="phone_text">
															<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
																array(
																	"COMPONENT_TEMPLATE" => ".default",
																	"PATH" => SITE_DIR."include/phone.php",
																	"AREA_FILE_SHOW" => "file",
																	"AREA_FILE_SUFFIX" => "",
																	"AREA_FILE_RECURSIVE" => "Y",
																	"EDIT_TEMPLATE" => "standard.php"
																),
																false
															);?>
														</span>
													</span>
													<span class="order_wrap_btn">
														<span class="callback_btn"><?=GetMessage("CALLBACK")?></span>
													</span>
												</span>
											</div>
										</div>
									<?}?>
									<div class="clearfix"></div>
								</td>
							</tr>
						</table>
					</div>
					<div class="catalog_menu menu_<?=strToLower($TEMPLATE_OPTIONS["MENU_COLOR"]["CURRENT_VALUE"]);?>">
						<div class="wrapper_inner">
							<div class="wrapper_middle_menu wrap_menu">
								<ul class="menu adaptive">
									<li class="menu_opener"><div class="text">
										<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
											array(
												"COMPONENT_TEMPLATE" => ".default",
												"PATH" => SITE_DIR."include/menu/menu.mobile.title.php",
												"AREA_FILE_SHOW" => "file",
												"AREA_FILE_SUFFIX" => "",
												"AREA_FILE_RECURSIVE" => "Y",
												"EDIT_TEMPLATE" => "standard.php"
											),
											false
										);?>
								</div></li>
								</ul>
								<div class="catalog_menu_ext">
									<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"front", 
	array(
		"COMPONENT_TEMPLATE" => "front",
		"PATH" => SITE_DIR."include/menu/menu.catalog.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
								</div>
								<div class="inc_menu">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
										array(
											"COMPONENT_TEMPLATE" => ".default",
											"PATH" => SITE_DIR."include/menu/menu.top_content_multilevel.php",
											"AREA_FILE_SHOW" => "file",
											"AREA_FILE_SUFFIX" => "",
											"AREA_FILE_RECURSIVE" => "Y",
											"EDIT_TEMPLATE" => "standard.php"
										),
										false
									);?>
								</div>
							</div>
						</div>
					</div>
				</header>
			</div>
			<div class="wraps" id="content">
				<div class="wrapper_inner <?=(COptimus::IsMainPage() ? "front" : "");?> <?=((COptimus::IsOrderPage() || COptimus::IsBasketPage()) ? "wide_page" : "");?>">
					<?if(!COptimus::IsOrderPage() && !COptimus::IsBasketPage()){?>
						<div class="left_block">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/menu.left_menu.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>

							<?$APPLICATION->ShowViewContent('left_menu');?>

							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/comp_banners_left.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/comp_news.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>
							<?/*$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/comp_news_articles.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);*/?>
              <?/*$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                array(
                  "COMPONENT_TEMPLATE" => ".default",
                  "PATH" => SITE_DIR."include/left_block/comp_subscribe.php",
                  "AREA_FILE_SHOW" => "file",
                  "AREA_FILE_SUFFIX" => "",
                  "AREA_FILE_RECURSIVE" => "Y",
                  "EDIT_TEMPLATE" => "standard.php"
                ),
                false
              );*/?>
              <?if(COptimus::IsMainPage()):?>
                <?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "template", Array(
                  "COMPONENT_TEMPLATE" => ".default",
                    "REGISTER_URL" => "",	// Страница регистрации
                    "FORGOT_PASSWORD_URL" => "",	// Страница забытого пароля
                    "PROFILE_URL" => "",	// Страница профиля
                    "SHOW_ERRORS" => "N",	// Показывать ошибки
                    "COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
                    "COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
                  ),
                  false
                );?>
              <?endif?>
						</div>
						<div class="right_block">
					<?}?>
						<div class="middle">
							<?if(!COptimus::IsMainPage()):?>
								<div class="container">
									<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "optimus", array(
										"START_FROM" => "0",
										"PATH" => "",
										"SITE_ID" => "-",
										"SHOW_SUBSECTIONS" => "N"
										),
										false
									);?>
									<h1 id="pagetitle"><?=$APPLICATION->ShowTitle(false);?></h1>
							<?endif;?>
<?if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest") $APPLICATION->RestartBuffer();?>
