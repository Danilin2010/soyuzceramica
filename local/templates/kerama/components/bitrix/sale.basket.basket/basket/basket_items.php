<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
			<div class="msg-box"></div>
			<div class="mainSide zakaz">
			<?if(count($arResult['ITEMS']['AnDelCanBuy'])>0){?>			
				<div class="mainZakaz block">
					<div class="stepMap">
						<ul>
							<li class="active">
								<a style="cursor:pointer;" data-step="0">Заказ</a>
							</li>
							<li class="disabled">
								<a style="cursor:pointer;" data-step="1">Доставка и подробности</a>
							</li>
							<li class="disabled">
								<a style="cursor:pointer;" data-step="2">Подтверждение</a>
							</li>
						</ul>
					</div>
					<div class="steps active" id="step0">
						<h2>Корзина</h2>
						<?getBusket($arResult);?>
						<div class="btnsControl"><a data-step="0" class="next">Оформить</a></div>
					</div>

					<div class="steps" id="step1" data-step="1">
						<h2>
							Оформление заказа
						</h2>
						
						<div class="shippingType">
							<ul>
								<li data-shippingType="3" class="active">
									<a href="javascript:void(0)">
										Доставка
									</a>
								</li>
								<li data-shippingType="2">
									<a href="javascript:void(0)">
										Самовывоз
									</a>
								</li>
							</ul>
						</div>

						<div data-shippingType="1" class="shippingBox shippingType1 active">
							<form action="" id="costumer_form" data-step="1">
							<fieldset>
							<div class="inputsBlock">
								<div class="box adress">
									<p>
										Адрес
									</p>
									<input type="text" name="costumer_address" required />
								</div>
								<div class="box tel">
									<p>Телефон</p>
									<input type="tel" pattern="\+[0-9]{1}\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}" name="phone" value="" placeholder="+_(___)___-__-__" required />
									<p class="inputPlaceholder">Мы перезвоним  вам в рабочее время для подтверждения заказа и согласования  времени доставки</p>
								</div>	
								<div class="box person">
									<p>Контактное лицо</p>
									<input type="text" name="costumer_name" required />
								</div>
								<div class="box">
									<p>E-mail</p>
									<input type="email" name="email" placeholder="example@mail.com" validate />
								</div>
								<div class="box comment">
									<p>Комментарий</p>
									<textarea name="comment"></textarea>
								</div>
							</div>
							<?//<div class="btnsControl" data-step="1"><a>Далее</a></div>?>
							<div class="btnsControl"><input type="submit" value="Далее" /></div>
							<fieldset/>
							</form>
						</div>
					</div>
					
					<div class="steps" id="step2">
						<h2>Подтверждение заказа</h2>
						<form id="do_order" data-step="2" action="/include/add.php">
							<?//getBusket($arResult);?>
							<div id="order_body_list">
							</div>
							<div id="order_body_costumer">
							</div>
							<div class="btnsControl"><input type="submit" value="Подтвердить" /></div>
						</form>
					</div>
					<?/*<div class="steps" id="step3">
						<h2>
							Заказ принят
						</h2>
						<p>
							Мы свяжемся с вами в рабочее время для подтверждения заказа и согласования времени доставки
						</p>
					</div>*/?>
				</div>
			<?}else{
				echo '<div>Корзина пуста</div>';
			}?>
		</div>