$(document).ready(function () {
	'use strict';

	/*check box*/
		function toggleBg (eventObj) {

			
			var checkBoxFalse = $($(eventObj).find('.checkBoxFalse')),
				checkBoxTrue = $($(eventObj).find('.checkBoxTrue')),
				trueFalse = false;

			if ($(checkBoxFalse).is(':visible')) {
				$(checkBoxFalse).hide();
				$(checkBoxTrue).show();

				trueFalse = true;
				changeCheckBox(eventObj, trueFalse);
			} else {
				$(checkBoxFalse).show();
				$(checkBoxTrue).hide();

				trueFalse = false;
				changeCheckBox(eventObj, trueFalse);
			}

		};

		function changeCheckBox (eventObj, trueFalse) {
			var checkBox = $($(eventObj).find('input'));

			if (trueFalse) {
				$(checkBox).prop('checked', true);
			} else {
				$(checkBox).prop('checked', false);
			}
							
		};

		$(document).delegate('.checkBoxBtn', 'click', function () {
			toggleBg($(this));
		});
	/*end check box*/

	/*mySelect*/
		function mySelectToggleDropBox (eventObj) {
			$(eventObj).toggle();
		}

		function mySelectMakeSelectedInDropBox (eventObj) {
			var dropBoxOptions = $(eventObj).closest('.dropBox').find('.option'),
				thisSelectedField = $(eventObj).closest('.mySelect').find('.selected'),
				cloneEventObj = $(eventObj).clone();

			for (var i = 0; i < dropBoxOptions.length; i++) {
				$(dropBoxOptions[i]).removeClass('active');
			};

			$(eventObj).addClass('active');

			$(thisSelectedField).html(cloneEventObj);

		}
		
		$(document).delegate('.link a', 'click', function (e) {
			
			var eventObj=$(this).attr('sese');
				window.eval("myPlacemark"+eventObj).balloon.open();
$('html, body').animate({ scrollTop: $('.mapA').offset().top }, 500);
		});
		

		
		
		
		
		
		$(document).delegate('.mySelect', 'click', function (e) {
			if (!$(this).hasClass('vDisabled')) {
				var dropBox = $(this).find('.dropBox');

				mySelectToggleDropBox(dropBox);
			}

			e.stopPropagation();
		});

		$(document).delegate('.mySelect .dropBox .option', 'click', function (e) {
			mySelectMakeSelectedInDropBox($(this));
		});

		$(document).delegate('body', 'click', function (e) {
			var dropBox = $('.mySelect .dropBox');

			$(dropBox).hide();
		});
	/*end mySelect*/

	/*loremIspum Btn click*/
		function loremBlock (eventObj) {
			var loremBlock = $(eventObj).closest('.loremBlock');

			$(loremBlock).removeClass('active');
		}

		$(document).delegate('.loremBlock .closeLoremBlock', 'click', function () {
			loremBlock($(this));
		});
	/*end loremIspum Btn click*/
});/********check boxes*/