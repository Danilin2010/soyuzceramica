ymaps.ready(init);
    var myMap,
        myPlacemark,
        myPlacemark2,
        myPlacemark3,
        myPlacemark4,
        myPlacemark5,
        myPlacemark6,
        myPlacemark7,
        myPlacemark8,
        myPlacemark9,
        myPlacemark10;

function init(){     
    myMap = new ymaps.Map("mapA", {
        center: [55.76, 37.64],
        zoom: 6
    });

    myMap.behaviors.disable('scrollZoom');

    myPlacemark = new ymaps.Placemark([55.684758, 37.738521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m1">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark2 = new ymaps.Placemark([55.684758, 38.738521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m2">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark3 = new ymaps.Placemark([55.984758, 38.738521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m3">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark4 = new ymaps.Placemark([56.984758, 38.738521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m4">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark5 = new ymaps.Placemark([55.384758, 38.738521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m5">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark6 = new ymaps.Placemark([55.384758, 38.338521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m6">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark7 = new ymaps.Placemark([55.384758, 38.038521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m7">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark8 = new ymaps.Placemark([55.384758, 37.738521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m8">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark9 = new ymaps.Placemark([55.384758, 37.438521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m9">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myPlacemark10 = new ymaps.Placemark([55.384758, 37.138521], {
        hintContent: 'Собственный значок метки',
        balloonContentBody: [
            '<div class="baloonAdress m10">',
            '<p class="tittle">Орехово</p>',
            '<p class="street">Шипиловска ул,д.35</p>',
            '<p class="yTell">+7 495 742-40-40</p>',
            '<p class="yTime">с 9:30 до 20:00</p>',
            '<p>&laquo;KERAMA MARAZZI&raquo;</p>',
            '</div>'
        ].join('')
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/bitrix/templates/kerama/images/markerRed.png',
        // Размеры метки.
        iconImageSize: [30, 30],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [0, 0],
        hideIconOnBalloonOpen: true
    });

    myMap.geoObjects.add(myPlacemark);
    myMap.geoObjects.add(myPlacemark2);
    myMap.geoObjects.add(myPlacemark3);
    myMap.geoObjects.add(myPlacemark4);
    myMap.geoObjects.add(myPlacemark5);
    myMap.geoObjects.add(myPlacemark6);
    myMap.geoObjects.add(myPlacemark7);
    myMap.geoObjects.add(myPlacemark8);
    myMap.geoObjects.add(myPlacemark9);
    myMap.geoObjects.add(myPlacemark10);
}