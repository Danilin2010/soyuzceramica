$(document).ready(function () {
	'use strict';

	var slider1 = $('.indexSlider').slick({
					autoplay: true,
				    autoplaySpeed: 2000,
				    dots: false,
				    arrows: false,
				    speed: 1000,
				    fade: true,
				    draggable: true,
				    asNavFor: '.navSlider'
				});

    $('.indexSlider').on('afterChange', function (event, slider1, currentSlide, nextSlide) {
        if(0 == slider1.slickCurrentSlide()){
            slider1.slickSetOption("autoplay", false, false)
        };
    });

    $('.indexSlider').on('afterChange', function () {
    	var index = ($(this).find('.slick-current').attr('data-slick-index')),
    		slides = $('.navSlider .slick-slide');

    		$('.navSlider .slick-slide').removeClass('slick-current');

    		for (var i = 0; i < slides.length; i++) {
    			var indexNav = $(slides[i]).attr('data-slick-index');

    			if (index == indexNav) {
    				$(slides[i]).addClass('slick-current');
    			}
    		}
    });

    var navSliderSlides = $('.navSlider .descriptionLink'),
    	number = navSliderSlides.length;

    $('.navSlider').slick({
		slidesToShow: number,
		slidesToScroll: 1,
		asNavFor: '.indexSlider',
		dots: false,
		speed: 1000,
		centerMode: false,
		vertical: true,
		focusOnSelect: true
	});

    // $('.navSlider .slick-slide').eq(0).addClass('slick-current');
    
})