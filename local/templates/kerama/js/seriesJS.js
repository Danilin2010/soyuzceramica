$(document).ready(function () {
	'use strict';

	/*var pops = $('.oneItemPopUp'),
		calls = [],
		ids = [];

	for (var i = 0; i < pops.length; i++) {
		var idPop = $(pops[i]).attr('id'),
			idEl = "#" + idPop,
			toZoom = "#" + idPop + " .myZoom";
		
		calls.push($(toZoom));
		ids.push(idEl);
	}

	for (var i = 0; i < calls.length; i++) {
		calls[i].imagezoomsl({
			zoomrange: [4, 10],
			zoomstart: 6,
			descarea: ids[i] + " .my-containerForZoom",
			magnifiereffectanimate: "fadeIn",
			zoomspeedanimate: 1,
			loopspeedanimate: 1,
			magnifierspeedanimate: 100,
			scrollspeedanimate: 1
		});
	}
*/
	function checkHeightInEmtyLi () {
		var fixedBlockLi = $('.fixedBlock li');

		for (var i = 0; i < fixedBlockLi.length; i++) {
			var height = $(fixedBlockLi[i]).height();

			if (height == 0) {
				$(fixedBlockLi[i]).hide();
			}
		}
	}

	checkHeightInEmtyLi();	
		
    /*item PopUp*/
    	/*toggle calculator btn click*/
    		function toggleCalculatorBlock (eventObj) {
    			/*var calculatorBlock = $(eventObj).closest('.oneItemPopUp').find('.toOrder .calculatorPopUp');

    			$(calculatorBlock).show();*/
				
				var calculator = $(eventObj).parent().find('.calculatorPopUp');
				//$('.calculatorPopUp.calculatorPopUp22').hide();
	    		$(calculator).addClass('active');
				$(calculator).show();
				//alert($(calculator).attr('class'));
    		}

    		$(document).delegate('.itemsPopUp .oneItemPopUp .toOrder .makeOrderBtn', 'click', function () {
				toggleCalculatorBlock($(this));
				
			});

			$(document).delegate('.itemsPopUp .oneItemPopUp .toOrder .calculatorPopUp .closeBtn', 'click', function () {
				//alert('ss');
				var calculator = $(this).parent();
				//toggleCalculatorBlock($(this));
				$(calculator).removeClass('active');
				$(calculator).hide();
			});
    	/*end toggle calculator btn click*/

    	/*toOrderBlock calculator*/
    		function convertToPieces (eventObj) {
    			var meters = $(eventObj).val(),
    				piecesInput = $(eventObj).closest('form').find('.piecesInput'),
    				mmm = $(eventObj).closest('form').parent().find('.orderPrice').attr('col_m'),
					hhh = $(eventObj).closest('form').parent().find('.orderPrice').attr('col_h')
					
					//var data=Math.ceil(((hhh/1)/(mmm/1))*(meters/1));
					$.get("/include/get_coll.php", {h: hhh,m: mmm,col: meters}).done(function(data){
						var obj = $.parseJSON(data);
						$(piecesInput).val(obj.col_h);
						eventObj.val(obj.col_m);
					});
    		}

    		function convertToMetters (eventObj) {
			
				var pieces = $(eventObj).val(),
    				mettersInput = $(eventObj).closest('form').find('.mettersInput'),
    				mmm = $(eventObj).closest('form').parent().find('.orderPrice').attr('col_m'),
					hhh = $(eventObj).closest('form').parent().find('.orderPrice').attr('col_h')
					
					
					$.get("/include/get_metr.php", {h: hhh,m: mmm,col: pieces}).done(function(data) {
						var obj = $.parseJSON(data);
						$(mettersInput).val(obj.col_m);
						eventObj.val(obj.col_h);
					});
			
    		}

    		$(document).delegate('.calculatorPopUp .mettersInput', 'change', function () {
    			if ($(this).val() != NaN) {
    				convertToPieces($(this));
    			}
			});

			$(document).delegate('.calculatorPopUp .piecesInput', 'change', function () {
    			if ($(this).val() != NaN) {
    				convertToMetters($(this));
    			}
			});
    	/*end toOrderBlock calculator*/

    	/*put to the basket*/
    		var itemsInTheBasketCounter = $('.someItems .numberOfItems').attr('data-itemsadded');

    		function visualChangeBtnsToAdded (eventObj) {
    			var inputs = $(eventObj).closest('.calculatorPopUp').find('form input');

    				for (var i = 0; i < inputs.length; i++) {
    					$(inputs[i]).attr('disabled', true);
    				}
alert('Товар добавлен в корзину');
    			$(eventObj).html('В корзине');
    			$(eventObj).addClass('noHover');
    			$(eventObj).removeClass('active');
    		}

    		function addingToTheBasket (eventObj) {
			
    			var inputsValue = $(eventObj).closest('.calculatorPopUp').find('.piecesInput').val(),
    				priceFor1Metter = $(eventObj).closest('.calculatorPopUp').find('.orderPrice').attr('data-qmetterPrice'),
    				basketEmtptySpan = $('.basketBox .noItems'),
    				basketAddedSpan = $('.basketBox .someItems'),
    				itemsInBascetSpan = $(basketAddedSpan).find('.numberOfItems'),
    				priceSpan = $(basketAddedSpan).find('.itemsPrice'),
    				valuePriceSpan = parseInt($(priceSpan).attr('data-summadded'));
					
				
					
    			if (inputsValue > 0 && inputsValue.length > 0 ) {
    				itemsInTheBasketCounter++;

    				
    				$(basketEmtptySpan).addClass('hide');
    				$(basketAddedSpan).removeClass('hide');


    				$(itemsInBascetSpan).html(itemsInTheBasketCounter);


				
						valuePriceSpan += inputsValue * priceFor1Metter;
					
					  $.ajax({
							url: '/ajax/addToCart.php',
							type: 'POST',
							data: {id:$(eventObj).closest('.calculatorPopUp').find('.orderPrice').attr('id-data'),coll:inputsValue},
							success: function(data){
								$(eventObj).text("В корзине");
							}
						});
					
					
 					$(priceSpan).attr('data-summadded', valuePriceSpan.toFixed(2));
    				$(priceSpan).html(valuePriceSpan.toFixed(2));

    				visualChangeBtnsToAdded(eventObj);
    			}
    		}

    		$('.iWhantBlock').on('click','.btn.active', function () {
    			addingToTheBasket($(this));
			});
			$('.itemsPopUp').on('click','.btn.active', function () {
    			addingToTheBasket($(this));
			});
    	/*end put to the basket*/

    	/*inTheSeries*/
    		function inTheSeries (eventObj) {
    			var inTheSeriesBlock = $(eventObj).closest('.aboutItem').find('.inTheSeries');

    			$(inTheSeriesBlock).toggleClass('active');
    		}

    		
    	/*end inTheSeries*/

    	

    	/*hide popUp*/
    		function hidePopUpBlocks (eventObj) {
    			var shadow = $('.shadow'),
    				fixedShadow = $('.fixedShadow'),
    				header = $('.header'),
    				mainSide = $('.mainSide'),
    				footer = $('.footer'),
    				filterBlock = $('.fixedBlock'),
    				body = $('body'),
    				zeroBox = $('.zeroBox'),

    				popUpsBlock = $('.itemsPopUp'),
    				elementById = $(eventObj).closest('.oneItemPopUp');

    			$(shadow).removeClass('on');
    			$(fixedShadow).removeClass('on');

    			$(header).removeClass('hide');
    			$(mainSide).removeClass('hide');
    			$(footer).removeClass('hide');
    			$(zeroBox).css({paddingTop: "20px"});

    			$(popUpsBlock).removeClass('active');
    			$(elementById).removeClass('active');

    			$(filterBlock).prependTo(body);
    		} 

    		$(document).delegate('.oneItemPopUp .closeBtn.closePopUp', 'click', function () {
    			//hidePopUpBlocks($(this));
			});
    	/*end hidePopUp*/

		function moveCalculator2 (eventObj) {
	    		var calculator = $(eventObj).parent().find('.calculatorPopUp');
				$('.calculatorPopUp.calculatorPopUp22').hide();
	    		$(calculator).show();
	    		
	    	}

	    	function closeMovedCalculator2 (eventObj) {
				$('.calculatorPopUp.calculatorPopUp22').hide();
	    	}
		
		
		
		
		
    	/*itemPrice click*/
	    	function moveCalculator (eventObj) {
	    		var id = $(eventObj).closest('.smallItem').find('.itemName a').attr('href'),
	    			calculator = $('.itemsPopUp ' + id).find('.aboutItem .toOrder .calculatorPopUp'),
	    			itemContent = $(eventObj).closest('.itemContent');

	    		$(calculator).appendTo(itemContent);
	    		console.log(id);
	    	}

	    	function closeMovedCalculator (eventObj) {
	    		var id = $(eventObj).closest('.smallItem').find('.itemName a').attr('href'),
	    			popupToPut = $('.itemsPopUp ' + id + ' .aboutItem .toOrder'),
	    			calculator = $(eventObj).closest('.itemContent').find('.calculatorPopUp');

	    		$(calculator).appendTo(popupToPut);
	    	}

	    	$(document).delegate('.item .smallItem .itemPrice', 'click', function () {
				moveCalculator2($(this));
			});
			$("html").click(function(){
				closeMovedCalculator2($('.calculatorPopUp div.closeBtn'));
			});
			$('.iWhantBlock').on('click','.calculatorPopUp .closeBtn', function () {
				closeMovedCalculator2($(this));
			});
			$(document).mouseup(function (e){ // событие клика по веб-документу
			//alert('ss');
		var div = $(".calculatorPopUp"); // тут указываем ID элемента
		if (!div.is(e.target) && div.has(e.target).length === 0 && div.css('display')=="block") { // и не по его дочерним элементам
		//alert(div.css('display'));
			div.hide(); // скрываем его
		}
		
	});
    	/*end item price click*/
    /*end item PopUp*/   
});