<?
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/reset.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/lib/slick/slick.css"/><!--slick-->
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/mainCSS.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/mediaCSS.css">
	<link rel="stylesheet" media="screen,projection" href="<?=SITE_TEMPLATE_PATH?>/css/ui.totop.css" />
		<script src="<?=SITE_TEMPLATE_PATH?>/lib/jquery-2.1.4.min.js"></script>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	
	<div class="zeroBox">
	<div id="itemsPopUp" class="itemsPopUp">
				<div class="oneItemPopUp block clearFix" id="1">
					<div class="closeBtn closePopUp">Закрыть</div>
					<div class="popappss"></div>
				</div>
			</div>
		<div class="header">
			<div class="clearFix">
				<div class="box">
					<p style="float:left;margin-bottom:0;"><a href="/">Союз Керамика</a> &mdash; <span class="breack"><b>крупнейший партнер</b></span></p>
					<div class="logo" style="float:left;">
						<a href="/">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="logo">
						</a>
					</div>
					<div class="is1mark">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/mark1.jpg" alt="logo">
					</div>
				</div>
				<div class="box">
					<p class="tel">
						+7 495 506-96-98
					</p>
					<p class="workTime">
						<b>пн - сб с 10:00 до 20:00 вс с 10:00 до 18:00</b>
					</p>
				</div>
					<?
					$quant=0;
					$summ=0;
					CModule::IncludeModule("catalog");
					CModule::IncludeModule('iblock');
					CModule::IncludeModule("sale");
					$cntBasketItems = CSaleBasket::GetList(array("NAME" => "ASC","ID" => "ASC"),array("FUSER_ID" => CSaleBasket::GetBasketUserID(),"LID" => SITE_ID,"ORDER_ID" => "NULL","CAN_BUY" => "Y",),false,false);
					foreach($cntBasketItems->arResult as $res){
						//print_r($res);
						$arPrice = CPrice::GetBasePrice($res['PRODUCT_ID']);
						$summ=$summ+$arPrice['PRICE']*$res['QUANTITY'];
						$quant++;
					}
					
					
					
					?>
				
				
				<div class="box basketBox">
					<p class="basket">
						<img src="http://test.soyuzceramica.ru/bitrix/templates/kerama/images/basket.png" class="mobile-basket-img" />
						<span class="noItems <?=(($quant==0)?'':'hide');?>">
							<b>В корзине пока пусто</b>
						</span>
						<a class="someItems <?=(($quant!=0)?'':'hide');?>" href="/basket/">
							<span data-itemsAdded="<?=(int)$quant?>" class="numberOfItems"><?=(int)$quant?></span> <?= pluralForm($quant, 'товар', 'товара', 'товаров')?> <br>
							на <span data-summadded="<?=$summ;?>" class="itemsPrice"><?=$summ;?></span> Р
						</a>
					</p>
				</div>
			</div>
			<div class="nav">
				<div class="nav-menu">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"menu-head",
						Array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_GET_VARS" => array(""),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "left",
							"USE_EXT" => "N"
						)
					);?>
				</div>
			</div>
		</div>
		<div class="mainSide">
			<div class="clearFix">
	<?
		if(GL!="false"){
			
			$ccc_class="kontakty";
			$sss2="mainKontaktyblock";
			if(tips=="Y"){$ccc_class="tips";}
			if(CAT=="true"){$ccc_class="katalog series";$sss2="mainKatalog";}
			
			echo '<div class="mainSide '.$ccc_class.'">
			<div class="'.$sss2.' block">';
				if(CAT!="true"){echo '<h2>'; $APPLICATION->ShowTitle(false);echo '</h2>';}
			
		}
	?>		
			
			
			