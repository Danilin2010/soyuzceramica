<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 01.07.2018
 * Time: 3:27
 */

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/init.php');

AddEventHandler("sale", "OnSaleComponentOrderOneStepPersonType", "selectSavedPersonType");
function selectSavedPersonType(&$arResult, &$arUserResult, $arParams)
{
    global $USER;
    if($USER->IsAuthorized())
    {

        if (IS_DILER) {
            $personType = 3;

            //очищаем текущее значение типа плательщика
            foreach($arResult['PERSON_TYPE'] as $key => $type){
                if($type['CHECKED'] == 'Y'){
                    $arResult['PERSON_TYPE'][$key]['CHECKED'] = '';
                }
            }
            //устанавливаем новое значение типа плательщика
            $arResult['PERSON_TYPE'][$personType]['CHECKED'] = 'Y';
            $arUserResult['PERSON_TYPE_ID'] = $personType;

        } else {
            $personType = 3;

            //очищаем текущее значение типа плательщика
            foreach($arResult['PERSON_TYPE'] as $key => $type){
                if($type['CHECKED'] == 'Y'){
                    $arResult['PERSON_TYPE'][$key]['CHECKED'] = '';
                }
            }
            //устанавливаем новое значение типа плательщика
            $arResult['PERSON_TYPE'][$personType]['CHECKED'] = 'Y';
            $arUserResult['PERSON_TYPE_ID'] = $personType;
        }

    }
}