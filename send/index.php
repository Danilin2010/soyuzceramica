<?
require $_SERVER['DOCUMENT_ROOT']."/send/class.php";

$HATZAK_DB = 'db/HATZAK.dbf';
$TABZAK_DB = 'db/TABZAK.dbf';
$CLIDATA_DB= 'db/CLIDATA.dbf';
$mode = dBase::MODE_READ_WRITE;




##################################################### ###############################################
##################################   Параметры которые приходят    ##################################
##################################################### ###############################################

$COD="102";// код подразделения, описан в п.1.1
$NUMM="022632"; //номер документа, п.1.4
$mail_to = "catolog@ya.ru"; //  Почта на которую шлем
$them="New Zakaz"; //  Тема письма
$messege=iconv('UTF-8','WINDOWS-1251',"Заявка на поставку товара от интернет-магазина"); //  Текст письма

$HATZAK_DB_DATA=array(
		'123', // Номер документа
		'20170315', // Дата документа
		'20170328', // Предполагаемая дата отгрузки
		iconv('UTF-8','CP866','TEST'), // Комментарий к документу
		'1'  // Вид оплаты, где 1 – наличная оплата, 2 – безналичная оплата в случае интернет-магазина всегда 1
		);

$TABZAK_DB_DATA=array(
					array(
						'555555', // Номер документа
						'r676893', // Артикул товара
						'1', // Количество товара
						'', // тон  не заполняется
						''  //калибр  не заполняется
					),
					array(
						'555533', // Номер документа
						'r645783', // Артикул товара
						'3', // Количество товара
						'', // тон  не заполняется
						''  // калибр  не заполняется
					),
					array(
						'555525', // Номер документа
						'r636843', // Артикул товара
						'6', // Количество товара
						'', //  тон  не заполняется
						''  // калибр  не заполняется
					)
);



$CLIDATA_DB_DATA=array(
		 iconv('UTF-8','CP866',  'Федор Федорович'), // Контактное лицо
		'+79098585858', //Телефон
		'+79098585853', // Телефон 2
		'test@ya.ru', //Электронный адрес 
		iconv('UTF-8','CP866','Г. Москва, черепичный переулок д.10 кв .120'),  // Адрес доставки
		iconv('UTF-8','CP866','Комментарий')  // Комментарий
		);

#####################################################################################################
########################                    end                  ####################################
#####################################################################################################




//$dBase = dBase::open($filename, $mode);
$HATZAK = array(
  array("cnumzak","C", 6), // Номер документа
  array("dtzak","D"),      // Дата документа
  array("dtotg","D"),     // Предполагаемая дата отгрузки
  array("cmemo","C", 254),  // Комментарий к документу
  array("vidopl","N", 1, 0)  // Вид оплаты, где 1 – наличная оплата, 2 – безналичная оплата в случае интернет-магазина всегда 1
);
$TABZAK = array(
  array("cnumzak","C", 6),     // Номер документа
  array("art", "C", 25),           // Артикул товара
  array("koll","N",14,4),         // Количество товара
  array("ton","C", 4),      //  тон  не заполняется
  array("kalibr", "C", 3)     //  калибр  не заполняется
);
$CLIDATA = array(
  array("contact","C", 100),  //Контактное лицо
  array("tele","C", 100),	//Телефон
  array("tele2","C", 100),	//Телефон 2
  array("mail","C", 100),	//Электронный адрес 
  array("adress","C", 254),	//Адрес доставки
  array("cmemo","C", 254)		//Комментарий
);


if(file_exists($HATZAK_DB)){unlink($HATZAK_DB);}
if(file_exists($TABZAK_DB)){unlink($TABZAK_DB);}
if(file_exists($CLIDATA_DB)){unlink($CLIDATA_DB);}



//Создание базззззз

$base_HATZAK = dBase::create($HATZAK_DB, $HATZAK);
$base_HATZAK[0] = $HATZAK_DB_DATA;


$base_TABZAK = dBase::create($TABZAK_DB, $TABZAK);
if(count($TABZAK_DB_DATA)>0){
	foreach($TABZAK_DB_DATA as $k=>$val){
		$base_TABZAK[$k]=$val;
	}
	
}



$base_CLIDATA = dBase::create($CLIDATA_DB, $CLIDATA);
$base_CLIDATA[0] = $CLIDATA_DB_DATA;



//Создание архива
$files_to_zip = array($HATZAK_DB,$TABZAK_DB,$CLIDATA_DB);

$name_zip='db/Z'.$COD.$NUMM.'.zip';

if(file_exists($name_zip)){unlink($name_zip);}

$zips=dBase::create_zip($files_to_zip,$name_zip);

//print_r($zips);
if($zips){
 echo dBase::send_mail($mail_to, $them, $messege, $name_zip);
}
/*

create_zip


$dBase = dBase::open($filename, $mode);
//$dBase = dBase::create($filename, $HATZAK);

foreach ($dBase as $record) {
    print_r($record);
}

//$dBase[1] = array('eeeeee','','','3333','1');*/
?>