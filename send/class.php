<?php
class dBase implements ArrayAccess, Iterator, Countable
{
    const MODE_READ = 0;
    const MODE_WRITE = 1;
    const MODE_READ_WRITE = 2;
    public static function open($filename, $mode = self::MODE_READ)
    {
        if (!file_exists($filename)) {
            throw new RuntimeException(sprintf('Filename %s not found', $filename));
        }
        if (!function_exists('dbase_open')) {
            throw new RuntimeException(sprintf('Extension dBase not support with your PHP interpreter'));
        }
        $dbaseId = @dbase_open($filename, $mode);
        if (false === $dbaseId) {
            throw new RuntimeException(sprintf('Failed to open database file %s', $filename));
        }
        return new self($dbaseId);
    }
    public static function create($filename, array $fieldDefinitions)
    {
        if (file_exists($filename)) {
			//echo "dd";
            throw new RuntimeException(sprintf('Filename %s not found', $filename));
        }
        if (!function_exists('dbase_create')) {
            throw new RuntimeException(sprintf('Extension dBase not support with your PHP interpreter'));
        }
        $dbaseId = @dbase_create($filename, $fieldDefinitions);
        if (false === $dbaseId) {
            throw new RuntimeException(sprintf('Failed to create database file %s', $filename));
        }
        return new self($dbaseId);
    }
	/* creates a compressed zip file */
	
public static function send_mail($mail_to, $thema, $html, $path){
	if ($path) {  
    $fp = fopen($path,"rb");   
    if (!$fp)   
    { print "Cannot open file";   
      exit();   
    }   
    $file = fread($fp, filesize($path));   
    fclose($fp);   
    }  
	$f_name=explode('/',$path);
    $name = $f_name[1]; // в этой переменной надо сформировать имя файла (без всякого пути)  
    $EOL = "\r\n"; // ограничитель строк, некоторые почтовые сервера требуют \n - подобрать опытным путём
    $boundary     = "--".md5(uniqid(time()));  // любая строка, которой не будет ниже в потоке данных.  
    $headers    = "MIME-Version: 1.0;$EOL";   
    $headers   .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";  
    $headers   .= "From: address@server.com";  
      
    $multipart  = "--$boundary$EOL";   
    $multipart .= "Content-Type: text/html; charset=windows-1251$EOL";   
    $multipart .= "Content-Transfer-Encoding: base64$EOL";   
    $multipart .= $EOL; // раздел между заголовками и телом html-части 
    $multipart .= chunk_split(base64_encode($html));   

    $multipart .=  "$EOL--$boundary$EOL";   
    $multipart .= "Content-Type: application/octet-stream; name=\"$name\"$EOL";   
    $multipart .= "Content-Transfer-Encoding: base64$EOL";   
    $multipart .= "Content-Disposition: attachment; filename=\"$name\"$EOL";   
    $multipart .= $EOL; // раздел между заголовками и телом прикрепленного файла 
    $multipart .= chunk_split(base64_encode($file));   

    $multipart .= "$EOL--$boundary--$EOL";   
      
        if(!mail($mail_to, $thema, $multipart, $headers))   
         {return False;           //если не письмо не отправлено
      }  
    else { //// если письмо отправлено
    return True;  
    }  
	
  }
	
	
	
	
	
 public static function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$zip->addFile($file,str_replace("db/","",$file));
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}
	
	
	
	
	
	
	
	
    private $_dbaseId;
    private $_recordNumber;
    private function __construct($dbaseId)
    {
        $this->_dbaseId = $dbaseId;
    }
    public function __destruct()
    {
        @dbase_close($this->_dbaseId);
    }
    /**
     * @inheritdoc
     */
    public function count()
    {
        return @dbase_numrecords($this->_dbaseId);
    }
    /**
     * @inheritdoc
     */
    public function current()
    {
        return @dbase_get_record_with_names($this->_dbaseId, $this->_recordNumber);
    }
    /**
     * @inheritdoc
     */
    public function next()
    {
        $this->_recordNumber++;
    }
    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->_recordNumber;
    }
    /**
     * @inheritdoc
     */
    public function valid()
    {
        return $this->_recordNumber <= count($this);
    }
    /**
     * @inheritdoc
     */
    public function rewind()
    {
        $this->_recordNumber = 1;
    }
    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        if (null !== $offset) {
            return (false !== @dbase_get_record_with_names($this->_dbaseId, $offset + 1));
        }
        return false;
    }
    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        if ($this->offsetExists($offset)) {
            return @dbase_get_record_with_names($this->_dbaseId, $offset + 1);
        } else {
            throw new OutOfBoundsException(sprintf('Invalid index %s', $offset));
        }
    }
    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        if ($this->offsetExists($offset)) {
            dbase_replace_record($this->_dbaseId, $value, $offset + 1);
        } else {
            dbase_add_record($this->_dbaseId, $value);
        }
    }
    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        if (isset($this[$offset])) {
            dbase_delete_record($this->_dbaseId, $offset+1);
            dbase_pack($this->_dbaseId);
        } else {
            throw new OutOfRangeException(sprintf('Invalid index %s', $offset));
        }
    }
}