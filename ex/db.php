<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");


exit('stop');


$dbLink = mysql_connect(OSTATKI_DB_HOST, OSTATKI_DB_USER, OSTATKI_DB_PASS) or die("Could not connect: " . mysql_error());
mysql_select_db(OSTATKI_DB_NAME) or die("Could not select database");


// Загружаем остатки
$result = mysql_query('CALL art_allitem("'. OSTATKI_DB_USER .'")') or die("Query failed : " . mysql_error());

$arData = Array();
while ($item = mysql_fetch_array($result, MYSQL_ASSOC)) 
{
  $arData[] = Array(
    'article' => $item['article'],
    'name'    => $item['namtov'],
    'sklad'   => $item['skl'],
    'transit' => $item['inway'],
    'factory' => $item['zavod'],
    'block'   => $item['blocktov'],
  );
}
//dbg($arData, 1);
/**
            [article] => 135
            [name] => 135 Синий карандаш
            [sklad] => 34.0000
            [transit] => 120.0000
            [factory] => 12400.0000
            [block] => 0
 */

// Загружаем данные остатков
foreach($arData as $arItem)
{
  if(empty($arItem['article']))
    continue;
  
  // Ищем артикул в БД
  $rs = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>IBLOCK_OSTATKI_ID, "NAME"=>$arItem['article']), false, Array("nPageSize"=>1), Array("ID", "NAME"));
  if($rs->SelectedRowsCount() > 0)
  {
    
    $arElement = $rs->Fetch();
    CIBlockElement::SetPropertyValuesEx($arElement['ID'], IBLOCK_OSTATKI_ID, Array(
      'PRODUCT_SKLAD_COUNT' => $arItem['sklad'],
      'PRODUCT_TRANSIT_COUNT' => $arItem['transit'],
      'PRODUCT_FACTORY_COUNT' => $arItem['factory'],
    ));
    
    // Обновление индекса
    \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(IBLOCK_OSTATKI_ID, $arElement['ID']);
    
  }
  else
  {
    
    $el = new CIBlockElement;
    $arLoadProductArray = Array(
      "ACTIVE"            => "Y",
      "NAME"              => $arItem['article'],
      "MODIFIED_BY"       => $USER->GetID(), // элемент изменен текущим пользователем
      "IBLOCK_ID"         => IBLOCK_OSTATKI_ID,
      "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
      "PROPERTY_VALUES"   => Array(
          275 => $arItem['sklad'],
          276 => $arItem['transit'],
          277 => $arItem['factory'],
      ),
    );
    $el->Add($arLoadProductArray);
    
  }
  
}